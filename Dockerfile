FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.10-slim

WORKDIR /usr/src/app

# Install specific requirements for the package.
ADD ./requirements.txt ./requirements-tests.txt ./
RUN pip install --upgrade pip && \
    pip install -r requirements.txt && \
    pip install -r requirements-tests.txt && \
    pip install tox

# Copy application source and install it. Use "-e" to avoid needlessly copying
# files into the site-packages directory.
ADD ./ ./
RUN pip install .

ENTRYPOINT ["essmsync"]
