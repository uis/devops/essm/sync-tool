# Test Booker operations
import unittest
import urllib
from faker import Faker
from . import TestCase
from .. import state
from .. import booker
from .. import termtime
from .. import exceptions


class BookerGetAllTests(TestCase):

    def _assert_non_empty_list(self, key, func):
        """
        Asserts that func operation loads mock non-empty list into booker.all_{key}

        """
        full_key = f'booker.all_{key}'
        self.assertIsNone(state.get(full_key))
        func()
        s = state.get(full_key)
        self.assertIsInstance(s, list)
        self.assertNotEqual(len(s), 0)

        # Return successfully retrieved state for further tests
        return s

    def _assert_exception_non_list(self, key, func, url=None):
        """
        Asserts that func operation raises an exception if response is not a list

        """
        if url is None:
            url = key
        full_key = f'booker.all_{key}'
        self.assertIsNone(state.get(full_key))
        # Override mock with dict instead of list
        self.set_request_mock('GET', 'booker/api', url, 'empty_dict.json')
        self.assertRaises(exceptions.BookerAPIError, func)

    def test_auth_with_token(self):
        """
        Booker auth function will use the given token and test against roomtypes
        endpoint

        """
        # `config.yaml` has access_token: "MOCK_TOKEN" and `booker-roomtypes.json`
        # has acceptable list of room types
        self.assertEqual(state.get('booker.api.access_token'), 'MOCK_TOKEN')
        booker.auth()
        # Only call to roomtypes made
        self.assertEqual(len(self.requests_mock.request_history), 1)
        self.assertEqual(self.requests_mock.request_history[0].url, 'http://booker/api/roomtypes')
        # Call used API key in header
        self.assertEqual(
            self.requests_mock.request_history[0].headers.get('Authorization'),
            'Bearer MOCK_TOKEN'
        )

    def test_auth_with_credentials(self):
        """
        Booker auth function will use the credentials to get a token then test
        against roomtypes endpoint

        """
        # Removing access_token
        state.set('booker.api.access_token', None)

        # Mock Token response
        def token_response(request, context):
            # POST body should contain email and password from `config.json`
            self.assertEqual(
                urllib.parse.parse_qs(request.text),
                {
                    'email': ['essm-user@booker'],
                    'password': ['BOOKER_PASSWORD'],
                }
            )
            return '{"access_token": "NEW_TOKEN"}'
        self.set_request_mock_func('POST', 'booker/api', 'Token', token_response)

        booker.auth()
        # POST to token and GET from roomtypes made
        self.assertEqual(len(self.requests_mock.request_history), 2)
        self.assertEqual(self.requests_mock.request_history[0].url, 'http://booker/api/Token')
        self.assertEqual(self.requests_mock.request_history[0].method, 'POST')
        self.assertEqual(self.requests_mock.request_history[1].url, 'http://booker/api/roomtypes')

        # New token remembered
        self.assertEqual(state.get('booker.api.access_token'), 'NEW_TOKEN')
        # And call to room types used it in header
        self.assertEqual(
            self.requests_mock.request_history[1].headers.get('Authorization'),
            'Bearer NEW_TOKEN'
        )

    def test_auth_with_bad_token_response(self):
        """
        Booker auth function will use the credentials to get a token and raise
        an exception if it doesn't get one

        """
        # Removing access_token
        state.set('booker.api.access_token', None)

        # Mock Token bad response
        def bad_token_response(request, context):
            # POST body should contain email and password from `config.json`
            self.assertEqual(
                urllib.parse.parse_qs(request.text),
                {
                    'email': ['essm-user@booker'],
                    'password': ['BOOKER_PASSWORD'],
                }
            )
            return '{"not_token": "BLAH"}'
        self.set_request_mock_func('POST', 'booker/api', 'Token', bad_token_response)
        with self.assertRaises(exceptions.BookerAPIError):
            booker.auth()

    def test_auth_with_roomtypes_failure(self):
        """
        Booker auth function will fail if test against roomtypes endpoint returns
        unexpected results

        """
        # Room Types response with non-list raises exception
        self.set_request_mock('GET', 'booker/api', 'roomtypes', 'empty_dict.json')
        with self.assertRaises(exceptions.BookerAPIError):
            booker.auth()

        # Room Types response with empty list raises exception
        self.set_request_mock('GET', 'booker/api', 'roomtypes', 'empty_list.json')
        with self.assertRaises(exceptions.BookerAPIError):
            booker.auth()

    def test_get_all_equipment(self):
        """
        Booker get_all_equipment operation loads equipment list with a single
        member with Id == 'TermTime'

        """
        s = self._assert_non_empty_list('equipment', booker.op_get_all_equipment)
        # Fixture has 5 equipment but only 1 should be TermTime
        self.assertEqual(len(s), 5)
        self.assertEqual(len([e for e in s if e.get('Id') == 'TermTime']), 1)

    def test_get_all_equipment_non_list(self):
        """
        Booker get_all_equipment operation raises an exception if not a list

        """
        self._assert_exception_non_list('equipment', booker.op_get_all_equipment)

    def test_get_all_depts(self):
        """
        Booker get_all_depts operation loads non-empty department list

        """
        s = self._assert_non_empty_list('depts', booker.op_get_all_depts)
        # Fixture has 3 departments
        self.assertEqual(len(s), 3)

    def test_get_all_depts_non_list(self):
        """
        Booker get_all_depts operation raises an exception if not a list

        """
        self._assert_exception_non_list('depts', booker.op_get_all_depts, 'departments')

    def test_get_all_sites(self):
        """
        Booker get_all_sites operation loads non-empty sites list

        """
        s = self._assert_non_empty_list('sites', booker.op_get_all_sites)
        # Fixture has 3 sites
        self.assertEqual(len(s), 3)

    def test_get_all_sites_non_list(self):
        """
        Booker get_all_sites operation raises an exception if not a list

        """
        self._assert_exception_non_list('sites', booker.op_get_all_sites)

    def test_get_all_buildings(self):
        """
        Booker get_all_buildings operation loads non-empty buildings list

        """
        s = self._assert_non_empty_list('buildings', booker.op_get_all_buildings)
        # Fixture has 4 buildings
        self.assertEqual(len(s), 4)

    def test_get_all_buildings_non_list(self):
        """
        Booker get_all_buildings operation raises an exception if not a list

        """
        self._assert_exception_non_list('buildings', booker.op_get_all_buildings)

    def test_get_all_rooms(self):
        """
        Booker get_all_rooms operation loads non-empty rooms list

        """
        s = self._assert_non_empty_list('rooms', booker.op_get_all_rooms)
        # Fixture has 6 rooms
        self.assertEqual(len(s), 6)

    def test_get_all_rooms_non_list(self):
        """
        Booker get_all_rooms operation  raises an exception if not a list

        """
        self._assert_exception_non_list('rooms', booker.op_get_all_rooms)

    def test_get_all_floors(self):
        """
        Booker get_all_floors operation compiles a list of floors from rooms

        """
        # get_all_floors uses get_all_rooms
        self.assertIsNone(state.get('booker.all_rooms'))
        self.assertIsNone(state.get('booker.all_floors'))
        booker.op_get_all_floors()
        s = state.get('booker.all_floors')
        self.assertIsInstance(s, list)
        # Fixture has 5 floors
        self.assertEqual(len(s), 5)

    def test_get_all_floor_empty_list(self):
        """
        Booker get_all_floors operation raises exception if list is empty

        """
        self.assertIsNone(state.get('booker.all_rooms'))
        self.assertIsNone(state.get('booker.all_floors'))
        # Override mock room response with empty list
        self.set_request_mock('GET', 'booker/api', 'rooms', 'empty_list.json')
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_all_floors)


class BookerGetFilteredTests(TestCase):

    def test_get_depts(self):
        """
        Booker get_depts operation loads department list and filters to
        only those related to rooms with TermTime equipment

        """
        self.assertIsNone(state.get('booker.depts'))
        booker.op_get_depts()
        s = state.get('booker.depts')
        self.assertIsInstance(s, list)

        # fixture has 3 departments but only 2 are used by TermTime rooms
        self.assertEqual(len(s), 2)

    def test_get_depts_empty(self):
        """
        Booker get_depts operation raises an exception if no departments are
        related to rooms with TermTime equipment

        """
        self.assertIsNone(state.get('booker.depts'))
        self.assertIsNone(state.get('booker.rooms'))
        # Load TermTime related rooms
        booker.op_get_rooms()
        self.assertIsInstance(state.get('booker.rooms'), list)
        # Change all rooms to have Departments list to be empty so that no departments are
        # found and an exception is raised
        state.set('booker.rooms', [
            {**r, **{'Departments': list()}} for r in state.get('booker.rooms')
        ])
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_depts)

    def test_get_depts_missing_warning(self):
        """
        Booker get_depts operation warns if any of the departments that TermTime
        rooms belong to are missing

        """
        self.assertIsNone(state.get('booker.all_depts'))
        # get all departments
        booker.op_get_all_depts()
        # remove second (of three) departments
        s = state.get('booker.all_depts')
        self.assertEqual(len(s), 3)
        s.pop(1)

        # Now get filtered departments
        with self.assertLogs(booker.LOG, "WARN") as log:
            booker.op_get_depts()
        # Only one department found
        s = state.get('booker.depts')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        # And warning of one missing
        self.assertIn("WARNING:essmsync.booker:Booker missing 1 departments", log.output)

    def test_get_depts_room_with_no_dept(self):
        """
        Booker get_depts operation warns if any of the Booker rooms matching the TermTime
        rooms are missing a department

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('booker.depts'))
        # remove the departments field from Teaching Room 1 (B001-01-0001) and
        # empty the departments field from Teaching Room 2 (B001-02-0001)
        ROOM_ID_DEL = 'B001-01-0001'
        ROOM_ID_EMPTY = 'B001-02-0001'
        for r in state.get('booker.rooms', booker.op_get_rooms):
            if r['Id'] == ROOM_ID_DEL:
                del(r['Departments'])
            if r['Id'] == ROOM_ID_EMPTY:
                r['Departments'] = list()

        # Now get filtered departments
        with self.assertLogs(booker.LOG, "WARN") as log:
            booker.op_get_depts()
        # warnings of two missing
        self.assertTrue(set(log.output).issuperset({
            f"WARNING:essmsync.booker:Room '{ROOM_ID_DEL}' has no department",
            f"WARNING:essmsync.booker:Room '{ROOM_ID_EMPTY}' has no department",
        }))

    def test_get_depts_room_with_multiple_depts(self):
        """
        Booker get_depts operation warns if any of the Booker rooms matching the TermTime
        rooms have more than one department

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('booker.depts'))
        # add another department to Teaching Room 1 (B001-01-0001)
        ROOM_ID = 'B001-01-0001'
        for r in state.get('booker.rooms', booker.op_get_rooms):
            if r['Id'] == ROOM_ID:
                r['Departments'].append(666)

        # Now get filtered departments
        with self.assertLogs(booker.LOG, "WARN") as log:
            booker.op_get_depts()
        # warnings of one room with two departments
        self.assertIn(f"WARNING:essmsync.booker:Room '{ROOM_ID}' has 2 departments", log.output)

    def test_get_sites(self):
        """
        Booker get_sites operation loads sites list and filters to only those
        related to rooms with TermTime equipment

        """
        self.assertIsNone(state.get('booker.sites'))
        booker.op_get_sites()
        s = state.get('booker.sites')
        self.assertIsInstance(s, list)

        # fixture has 3 sites but only 2 are used by TermTime rooms
        self.assertEqual(len(s), 2)

    def test_get_sites_empty(self):
        """
        Booker get_sites operation raises an exception if no sites are related to
        rooms with TermTime equipment

        """
        self.assertIsNone(state.get('booker.sites'))
        self.assertIsNone(state.get('booker.buildings'))
        # Load TermTime related buildings
        booker.op_get_buildings()
        self.assertIsInstance(state.get('booker.buildings'), list)
        # Change all buildings have SiteId of -1 so that no sites are found and an
        # exception is raised
        state.set('booker.buildings', [
            {**b, **{'SiteId': -1}} for b in state.get('booker.buildings')
        ])
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_sites)

    def test_get_buildings(self):
        """
        Booker get_buildings operation loads buildings list and filters to only
        those related to rooms with TermTime equipment

        """
        self.assertIsNone(state.get('booker.buildings'))
        booker.op_get_buildings()
        s = state.get('booker.buildings')
        self.assertIsInstance(s, list)

        # fixture has 4 buildings but only 3 are used by TermTime rooms
        self.assertEqual(len(s), 3)

    def test_get_buildings_empty(self):
        """
        Booker get_buildings operation raises an exception if no buildings are
        related to rooms with TermTime equipment

        """
        self.assertIsNone(state.get('booker.buildings'))
        self.assertIsNone(state.get('booker.rooms'))
        # Load TermTime related rooms
        booker.op_get_rooms()
        self.assertIsInstance(state.get('booker.rooms'), list)
        # If all rooms have BuildingId of -1 so that no buildings are found and
        # an exception is raised
        state.set('booker.rooms', [
            {**r, **{'BuildingId': -1}} for r in state.get('booker.rooms')
        ])
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_buildings)

    def test_get_rooms(self):
        """
        Booker get_rooms operation loads rooms list and filters to only those
        with TermTime equipment

        """
        self.assertIsNone(state.get('booker.rooms'))
        booker.op_get_rooms()
        s = state.get('booker.rooms')
        self.assertIsInstance(s, list)

        # fixture has 6 rooms but only 4 have TermTime equipment
        self.assertEqual(len(s), 4)

    def test_get_rooms_empty(self):
        """
        Booker get_rooms operation raises an exception if no rooms have the
        TermTime equipment

        """
        self.assertIsNone(state.get('booker.rooms'))
        # Override mock with response where no room has the TermTime equipment
        self.set_request_mock('GET', 'booker/api', 'rooms', 'booker-rooms-none-tt.json')
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_rooms)

    def test_get_rooms_missing_termtime_equipment(self):
        """
        Booker get_rooms operation raises an exception if no equipment with the
        TermTime Id exists

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('booker.termtime_equipment'))
        # Set booker.termtime_id to an Id that does not exist exist in equipment list
        state.set('booker.termtime_id', 'NonExistantId')
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_rooms)

    def test_get_floors(self):
        """
        Booker get_floors operation compiles a list of floors from a filtered
        list of rooms

        """
        self.assertIsNone(state.get('booker.floors'))
        booker.op_get_floors()
        s = state.get('booker.floors')
        self.assertIsInstance(s, list)

        # fixture has 5 floors but only 4 are related to TermTime rooms
        self.assertEqual(len(s), 4)

    def test_get_floors_empty(self):
        """
        Booker get_floors operation raises an exception if no floors are
        related to rooms with TermTime equipment

        """
        self.assertIsNone(state.get('booker.floors'))
        self.assertIsNone(state.get('booker.rooms'))
        # Load TermTime related rooms
        booker.op_get_rooms()
        self.assertIsInstance(state.get('booker.rooms'), list)
        # Change all rooms to have BuildingIds of -1 so that no floors can be
        # compiled from rooms and an exception is raised
        state.set('booker.rooms', [
            {**r, **{'BuildingId': -1}} for r in state.get('booker.rooms')
        ])
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_floors)


class BookerGetBookingsTests(TestCase):

    def test_room_bookings_without_bookedby_id(self):
        """
        Booker get_room_bookings operation raises an exception if at least one
        booking doesn't have a `BookedBy.Id`

        """
        # Override mock with response with a booking missing 'BookedBy'
        def missing_bookedby(request, context):
            with open(f'{self.fixtures_dir}/booker-bookings-missing-bookedby.json') as f:
                text = f.read()
            return text
        self.set_request_mock_func(
            'POST', 'booker/api', 'booker/calendars/events', missing_bookedby)

        self.assertIsNone(state.get('booker.roombookings'))
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_room_bookings)

        # Override mock with response with a booking missing 'BookedBy.Id'
        def missing_bookedby_id(request, context):
            with open(f'{self.fixtures_dir}/booker-bookings-missing-bookedby-id.json') as f:
                text = f.read()
            return text
        self.set_request_mock_func(
            'POST', 'booker/api', 'booker/calendars/events', missing_bookedby_id)

        self.assertIsNone(state.get('booker.roombookings'))
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_room_bookings)

    def test_room_bookings_with_no_description(self):
        """
        Booker get_room_bookings operation will convert bookings without a Description to having
        an empty string.

        """
        # Override mock with response with a booking missing a Description field
        def missing_description(request, context):
            with open(f'{self.fixtures_dir}/booker-bookings-missing-description.json') as f:
                text = f.read()
            return text
        self.set_request_mock_func(
            'POST', 'booker/api', 'booker/calendars/events', missing_description)

        self.assertIsNone(state.get('booker.roombookings'))

        # Get bookings
        bookings = state.get('booker.roombookings', booker.op_get_room_bookings)

        self.assertEqual(bookings[0].get('Description'), '')

    def test_room_bookings_with_none_description(self):
        """
        Booker get_room_bookings operation will convert bookings with a Description of None
        to having an empty string.

        """
        # Override mock with response with a booking with a Description of None
        def none_description(request, context):
            with open(f'{self.fixtures_dir}/booker-bookings-none-description.json') as f:
                text = f.read()
            return text
        self.set_request_mock_func(
            'POST', 'booker/api', 'booker/calendars/events', none_description)

        self.assertIsNone(state.get('booker.roombookings'))

        bookings = state.get('booker.roombookings', booker.op_get_room_bookings)

        self.assertEqual(bookings[0].get('Description'), '')

    def test_get_room_bookings(self):
        """
        Booker get_room_bookings operation retrieves bookings within `termtime.timeframe`
        (default 'All Year') that don't have an activity:instance reference in their
        Description

        """
        self.assertIsNone(state.get('booker.roombookings'))
        # Using defaults
        booker.op_get_room_bookings()
        s = state.get('booker.roombookings')
        self.assertIsInstance(s, list)
        # API used All Year timeframe
        self.assertEqual(self.bookings_post.get('start'), "2020-10-05")
        self.assertEqual(self.bookings_post.get('end'), "2021-10-03")

        # fixture has 3 bookings without references
        self.assertEqual(len(s), 3)
        for b in s:
            self.assertNotRegex(b['Description'], r'\[Activity\-\d+:\d+\]')

    def test_get_room_bookings_timeframe(self):
        """
        Booker get_room_bookings operation retrieves bookings only within specified
        termtime timeframe

        """
        self.assertIsNone(state.get('booker.roombookings'))
        # Restrict timeframe to 'Easter'
        state.set('termtime.timeframe', 'Easter')
        booker.op_get_room_bookings()
        s = state.get('booker.roombookings')
        self.assertIsInstance(s, list)
        # API used Easter timeframe
        self.assertEqual(self.bookings_post.get('start'), "2021-04-26")
        self.assertEqual(self.bookings_post.get('end'), "2021-06-20")

        # fixture has 1 booking not made by 'essm-sync' user in Easter
        self.assertEqual(len(s), 1)

    def test_get_room_bookings_dates(self):
        """
        Booker get_room_bookings operation retrieves bookings within start and end
        dates if given

        """
        start = "2020-12-02"
        end = "2021-02-02"
        self.assertIsNone(state.get('booker.roombookings'))
        # Restrict range by specifying start and end dates
        booker.op_get_room_bookings(start, end)
        s = state.get('booker.roombookings')
        self.assertIsInstance(s, list)
        # API used specified timeframe
        self.assertEqual(self.bookings_post.get('start'), start)
        self.assertEqual(self.bookings_post.get('end'), end)

        # fixture has 2 bookings not made by 'essm-sync' user with range specified
        self.assertEqual(len(s), 2)

    def test_get_room_bookings_exclude_past(self):
        """
        Booker get_room_bookings operation retrieves bookings within start and end
        dates if given, but also not in past

        """
        start = "2020-12-02"
        end = "2021-02-02"
        # same day but after start of Michaelmas room booking
        state.set('now', '2020-12-03T11:00:00Z')
        self.assertIsNone(state.get('booker.roombookings'))
        # Restrict range by specifying start and end dates
        booker.op_get_room_bookings(start, end)
        s = state.get('booker.roombookings')
        self.assertIsInstance(s, list)
        # API used specified timeframe but brought start date forward
        self.assertEqual(self.bookings_post.get('start'), "2020-12-03")
        self.assertEqual(self.bookings_post.get('end'), end)

        # fixture has 2 bookings not made by 'essm-sync' user with range specified
        # but Michaelmas booking starts before 'now' so only 1 returned
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0].get('OptimeIndex'), 1002)

    def test_get_room_bookings_ignore_booked_by(self):
        """
        Booker get_room_bookings operation retrieves bookings removing those that
        match the `booker.ignore_bookings` booked_by filters.

        """
        self.assertIsNone(state.get('booker.roombookings'))
        # Ignore bookings booked by 'real-user'
        state.set('booker.ignore_bookings', [
            {
                'by': 'booked_by',
                'values': ['real-user']
            },
        ])
        booker.op_get_room_bookings()
        s = state.get('booker.roombookings')
        self.assertIsInstance(s, list)
        # All bookings ignored
        self.assertEqual(len(s), 0)

    def test_get_room_bookings_ignore_room(self):
        """
        Booker get_room_bookings operation retrieves bookings removing those that
        match the `booker.ignore_bookings` room filters.

        """
        self.assertIsNone(state.get('booker.roombookings'))
        # Ignore bookings in room 'B001-02-0001'
        IGNORE_ROOM = 'B001-02-0001'
        state.set('booker.ignore_bookings', [
            {
                'by': 'room',
                'values': [IGNORE_ROOM]
            },
        ])
        booker.op_get_room_bookings()
        s = state.get('booker.roombookings')
        self.assertIsInstance(s, list)
        # One of the three room bookings removed and neither of the remaining are
        # in ignored room
        self.assertEqual(len(s), 2)
        for b in s:
            self.assertNotEqual(b.get('Room', {}).get('Id', ''), IGNORE_ROOM)

    def test_events_without_bookedby_id(self):
        """
        Booker get_events operation raises an exception if at least one booking
        doesn't have a `BookedBy.Id`

        """
        # Override mock with response with a booking missing 'BookedBy'
        def missing_bookedby(request, context):
            with open(f'{self.fixtures_dir}/booker-bookings-missing-bookedby.json') as f:
                text = f.read()
            return text
        self.set_request_mock_func(
            'POST', 'booker/api', 'booker/calendars/events', missing_bookedby)

        self.assertIsNone(state.get('booker.events'))
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_events)

        # Override mock with response with a booking missing 'BookedBy.Id'
        def missing_bookedby_id(request, context):
            with open(f'{self.fixtures_dir}/booker-bookings-missing-bookedby-id.json') as f:
                text = f.read()
            return text
        self.set_request_mock_func(
            'POST', 'booker/api', 'booker/calendars/events', missing_bookedby_id)

        self.assertIsNone(state.get('booker.events'))
        self.assertRaises(exceptions.BookerAPIError, booker.op_get_events)

    def test_get_events(self):
        """
        Booker get_events operation retrieves bookings within `termtime.timeframe`
        (default 'All Year') that have an activity:instance reference in their
        Description

        """
        self.assertIsNone(state.get('booker.events'))
        # Using defaults
        booker.op_get_events()
        s = state.get('booker.events')
        self.assertIsInstance(s, list)
        # API used All Year timeframe
        self.assertEqual(self.bookings_post.get('start'), "2020-10-05")
        self.assertEqual(self.bookings_post.get('end'), "2021-10-03")

        # fixture has 3 bookings with references
        self.assertEqual(len(s), 3)
        for b in s:
            self.assertRegex(b['Description'], r'\[Activity\-\d+:\d+\]')

    def test_get_events_timeframe(self):
        """
        Booker get_events operation retrieves bookings only within specified
        termtime timeframe

        """
        self.assertIsNone(state.get('booker.events'))
        # Restrict timeframe to 'Easter'
        state.set('termtime.timeframe', 'Easter')
        booker.op_get_events()
        s = state.get('booker.events')
        self.assertIsInstance(s, list)
        # API used Easter timeframe
        self.assertEqual(self.bookings_post.get('start'), "2021-04-26")
        self.assertEqual(self.bookings_post.get('end'), "2021-06-20")

        # fixture has 1 booking made by 'essm-sync' user in Easter
        self.assertEqual(len(s), 1)

    def test_get_events_dates(self):
        """
        Booker get_events operation retrieves bookings within start and end
        dates if given

        """
        start = "2020-12-02"
        end = "2021-02-02"
        self.assertIsNone(state.get('booker.events'))
        # Restrict range by specifying start and end dates
        booker.op_get_events(start, end)
        s = state.get('booker.events')
        self.assertIsInstance(s, list)
        # API used specified timeframe
        self.assertEqual(self.bookings_post.get('start'), start)
        self.assertEqual(self.bookings_post.get('end'), end)

        # fixture has 2 bookings made by 'essm-sync' user with range specified
        self.assertEqual(len(s), 2)

    def test_get_events_exclude_past(self):
        """
        Booker get_events operation retrieves bookings within start and end
        dates if given, but also not in past

        """
        start = "2020-12-02"
        end = "2021-02-02"
        # same day but after start of Michaelmas event
        state.set('now', '2020-12-03T11:00:00Z')
        self.assertIsNone(state.get('booker.events'))
        # Restrict range by specifying start and end dates
        booker.op_get_events(start, end)
        s = state.get('booker.events')
        self.assertIsInstance(s, list)
        # API used specified timeframe but brought start date forward
        self.assertEqual(self.bookings_post.get('start'), "2020-12-03")
        self.assertEqual(self.bookings_post.get('end'), end)

        # fixture has 2 bookings made by 'essm-sync' user with range specified
        # but Michaelmas booking starts before 'now' so only 1 returned
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0].get('OptimeIndex'), 1012)

    def test_get_events_ignore_booked_by(self):
        """
        Booker get_events operation retrieves bookings removing those that
        match the `booker.ignore_bookings` booked_by filters.

        """
        self.assertIsNone(state.get('booker.events'))
        # Ignore bookings booked by 'crsid1'
        IGNORE_BOOKED_BY = 'crsid1'
        state.set('booker.ignore_bookings', [
            {
                'by': 'booked_by',
                'values': [IGNORE_BOOKED_BY]
            },
        ])
        booker.op_get_events()
        s = state.get('booker.events')
        self.assertIsInstance(s, list)
        # Only one event left (booked by 'crsid2')
        self.assertEqual(len(s), 1)
        for b in s:
            self.assertNotEqual(b.get('BookedBy', {}).get('Id', ''), IGNORE_BOOKED_BY)

    def test_get_events_ignore_room(self):
        """
        Booker get_events operation retrieves bookings removing those that
        match the `booker.ignore_bookings` room filters.

        """
        self.assertIsNone(state.get('booker.events'))
        # Ignore bookings in room 'B002-00-0001'
        IGNORE_ROOM = 'B002-00-0001'
        state.set('booker.ignore_bookings', [
            {
                'by': 'room',
                'values': [IGNORE_ROOM]
            },
        ])
        booker.op_get_events()
        s = state.get('booker.events')
        self.assertIsInstance(s, list)
        # One of the three room bookings removed and neither of the remaining are
        # in ignored room
        self.assertEqual(len(s), 2)
        for b in s:
            self.assertNotEqual(b.get('Room', {}).get('Id', ''), IGNORE_ROOM)

    def test_get_all_staff(self):
        """
        Booker get_all_staff operation loads entire staff list

        """
        self.assertIsNone(state.get('booker.all_staff'))
        booker.op_get_all_staff()
        s = state.get('booker.all_staff')
        self.assertIsInstance(s, list)

        # fixture has 5 staff members
        self.assertEqual(len(s), 5)

        # Additional fields are removed but not all are necessarily provided
        filtered_keys = {'Id', 'OptimeIndex', 'email', 'forename', 'surname'}
        for staff in s:
            other_keys = set(staff.keys()) - filtered_keys
            self.assertEqual(len(other_keys), 0)

    def test_get_staff_by_id(self):
        """
        Booker get_staff_by_id operation filters all staff to those with crsid-like ids
        and stores them in `booker.staff_by_id` as dict keyed on id

        """
        self.assertIsNone(state.get('booker.staff_by_id'))
        booker.op_get_staff_by_id()
        s = state.get('booker.staff_by_id')
        self.assertIsInstance(s, dict)

        # Only crsid-like staff remain and id is lowercased and stripped
        self.assertEqual(set(s.keys()), {'crsid1', 'crsid2'})


class BookerUpdateTests(TestCase):

    def setUp(self):
        super().setUp()

        # Catch request when POSTing and PUTting to termtime API
        self.requests = {
            'POST': [],
            'PUT': [],
        }

        def booker_callback(request, context):
            split_path = [urllib.parse.unquote(s) for s in request.path.split('/')]
            request_info = {
                'data': request.json(),
                'path': split_path,
                # generally the resource's 'OptimeIndex' is the last part of url path
                'oi': split_path[-1],
            }
            self.requests[request.method].append(request_info)
            # dummy 'successful' response
            return '{}'

        self.set_request_mock_func('POST', 'booker/api', 'booker/booking', booker_callback)
        self.set_request_mock_func('PUT', 'booker/api', 'booker/booking/.+', booker_callback, True)

        # Not in read only mode but default
        state.set('read_only', False)

    def assertDictIncludes(self, data, expected):
        """
        Asserts that dict `data` contains all the key/value pairs of dict
        `expected`, though it may contain more.

        """
        self.assertIsInstance(data, dict)
        self.assertIsInstance(expected, dict)
        for k, v in expected.items():
            self.assertIn(k, data)
            self.assertEqual(data[k], v)

    def test_update_events_all_matched(self):
        """
        update_events operation with default fixtures will have nothing to
        create or update as all bookings will be matched

        """
        self.assertIsNone(state.get('comparison.events'))

        booker.op_update_events()

        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_events_create(self):
        """
        update_events operation will send request to create an event if a new
        one is added

        """
        self.assertIsNone(state.get('termtime.events'))
        self.assertIsNone(state.get('comparison.events'))

        # Preload the TermTime events
        termtime.op_get_events()

        # Add a new event
        NEW_EVENT = {
            'roomcode': 'B001-01-0001',
            'roomname': 'Teaching Room 1',
            'buildingname': 'Building One',
            'id': 'Activity-99999',
            'name': 'New Test Booking',
            'start': '2021-02-22 10:00:00',
            'end': '2021-02-22 12:00:00',
            'type': 'Lecture',
            'instance': 1,
            'size': 99,
        }
        state.get('termtime.events').append(NEW_EVENT)

        booker.op_update_events()

        # Addition was needed
        self.assertEqual(len(state.get('comparison.events.create')), 1)

        # Single POST request made
        self.assertEqual(len(self.requests['POST']), 1)
        self.assertEqual(len(self.requests['PUT']), 0)

        EXPECTED_TO_INCLUDE = {
            'Title': 'New Test Booking',
            'RoomId': 1,      # OptimeIndex of room B001-01-0001
            'BuildingId': 1,  # OptimeIndex of "Building One"
            'StartTime': '2021-02-22T10:00:00Z',
            'EndTime': '2021-02-22T12:00:00Z',
            'ExpectedAttendees': 99,
            'Type': 'Teaching',
            'Status': 'Approved',
            'BatchId': -1,
        }
        # The single POST matches new booking.
        self.assertDictIncludes(self.requests['POST'][0]['data'], EXPECTED_TO_INCLUDE)

    def test_update_events_update(self):
        """
        update_events operation will send request to update an event if one is
        modified

        """
        self.assertIsNone(state.get('termtime.events'))
        self.assertIsNone(state.get('comparison.events'))

        # Preload the TermTime events
        termtime.op_get_events()

        # Update Lent event's name, room and time
        ACTIVITY_ID = 'Activity-10000'
        INSTANCE = 2
        UPDATES = {
            'name': 'Rescheduled event',
            'roomcode': 'B002-00-0001',
            'start': '2021-02-01 15:00:00',
            'end': '2021-02-01 16:00:00',
        }

        updated = 0
        for d in state.get('termtime.events'):
            # Find the event to update
            if d.get('id') == ACTIVITY_ID and d.get('instance') == INSTANCE:
                updated += 1
                for k, v in UPDATES.items():
                    # Check the update changes the current value and change it
                    self.assertNotEqual(d.get(k), v)
                    d[k] = v
        self.assertEqual(updated, 1)

        booker.op_update_events()

        # Single PUT request made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 1)

        # Last part of url contains Lent event's OptimeIndex
        put = self.requests['PUT'][0]
        self.assertEqual(put['oi'], '1012')

        # and data contains expected changes
        EXPECTED_TO_INCLUDE = {
            'Title': 'Rescheduled event',
            'RoomId': 3,  # OptimeIndex of room 'B002-00-0001'
            'StartTime': '2021-02-01T15:00:00Z',
            'EndTime': '2021-02-01T16:00:00Z',
        }
        self.assertDictIncludes(put['data'], EXPECTED_TO_INCLUDE)

    def test_update_events_update_just_requestor(self):
        """
        update_events operation will send request to update an event's BookedBy
        if activity's crsid custom user field is modified

        """
        self.assertIsNone(state.get('termtime.events'))
        self.assertIsNone(state.get('comparison.events'))

        # Preload the TermTime activities
        acts_by_code = state.get('termtime.activities_by_code', termtime.op_get_activities_by_code)

        # Update Activity-10000's requestor in 'user5' key
        ID = 'Activity-10000'
        ACT_NUM = ID.split('-')[1]
        NEW_CRSID = 'crsid2'
        STAFF_OI = 4  # OptimeIndex of staff member 'crsid2' in Booker
        BOOKER_OIS = {'1011', '1012'}  # OptimeIndex of bookings matching instances of activity

        acts_by_code[ACT_NUM]['user5'] = NEW_CRSID

        booker.op_update_events()

        # PUT request made for each instance of Activity-10000 in Booker
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), len(BOOKER_OIS))
        ois_updated = {put['oi'] for put in self.requests['PUT']}
        self.assertEqual(ois_updated, BOOKER_OIS)

        # and BookedBy contains expected changes for both updates
        EXPECTED_BOOKED_BY = {
            'Id': NEW_CRSID,
            'UserOptimeIndex': STAFF_OI,
            'UserType': 'Staff',
        }
        for put in self.requests['PUT']:
            self.assertIsNotNone(put['data'].get('BookedBy'))
            self.assertEqual(put['data']['BookedBy'], EXPECTED_BOOKED_BY)

    def test_update_events_read_only(self):
        """
        update_events operation will *not* send requests to create or update
        events if in read only mode

        """
        state.set('read_only', True)

        self.assertIsNone(state.get('termtime.events'))
        self.assertIsNone(state.get('comparison.events'))

        # Preload the TermTime events
        termtime.op_get_events()

        # Add a new event
        s = state.get('termtime.events')
        s.append({
            'roomcode': 'B001-01-0001',
            'roomname': 'Teaching Room 1',
            'buildingname': 'Building One',
            'id': 'Activity-99999',
            'name': 'New Test Booking',
            'start': '2021-02-22 10:00:00',
            'end': '2021-02-22 12:00:00',
            'type': 'Lecture',
            'instance': 1,
            'size': 99,
        })
        # Update one
        for d in s:
            if d.get('id') == 'Activity-10000' and d.get('instance') == 1:
                self.assertEqual(d['name'], 'Booking made by essm-sync (Michaelmas)')
                d['name'] = 'Updated Event Title'

        booker.op_update_events()

        # Addition and update was needed
        self.assertEqual(len(state.get('comparison.events.create')), 1)
        self.assertEqual(len(state.get('comparison.events.update')), 1)

        # No requests made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_cancel_events(self):
        """
        cancel_events operation will send request to cancel an event if one is
        removed

        """
        self.assertIsNone(state.get('termtime.events'))
        self.assertIsNone(state.get('comparison.events'))

        # Preload the TermTime events
        termtime.op_get_events()

        # Remove Easter event
        ACTIVITY_ID = 'Activity-20000'
        INSTANCE = 1
        OPTIMEINDEX = 1013

        previous_length = len(state.get('termtime.events'))
        state.set('termtime.events', [
            d for d in state.get('termtime.events')
            if d.get('id') != ACTIVITY_ID or d.get('instance') != INSTANCE
        ])
        # One removed
        self.assertEqual(previous_length - 1, len(state.get('termtime.events')))

        booker.op_cancel_events()

        # Single PUT request made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 1)

        # Last part of url contains Lent event's OptimeIndex (as string)
        put = self.requests['PUT'][0]
        self.assertEqual(put['oi'], str(OPTIMEINDEX))

        # and data contains expected changes
        EXPECTED_TO_INCLUDE = {
            'OptimeIndex': OPTIMEINDEX,
            'Title': 'Booking made by essm-sync (Easter)',
            'Status': 'Cancelled',
        }
        self.assertDictIncludes(put['data'], EXPECTED_TO_INCLUDE)

    def test_cancel_events_read_only(self):
        """
        cancel_events operation will *not* send requests to cancel events if in
        read only mode

        """
        state.set('read_only', True)

        self.assertIsNone(state.get('termtime.events'))
        self.assertIsNone(state.get('comparison.events'))

        # Preload the TermTime events
        termtime.op_get_events()

        # Remove Easter event
        ACTIVITY_ID = 'Activity-20000'
        INSTANCE = 1

        previous_length = len(state.get('termtime.events'))
        state.set('termtime.events', [
            d for d in state.get('termtime.events')
            if d.get('id') != ACTIVITY_ID or d.get('instance') != INSTANCE
        ])
        # One removed
        self.assertEqual(previous_length - 1, len(state.get('termtime.events')))

        booker.op_cancel_events()

        # Need for deletion was detected by comparison
        self.assertEqual(len(state.get('comparison.events.delete')), 1)

        # No requests made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_cancel_events_limit(self):
        """
        cancel_events operation will send request to cancel an event up to limit
        in `booker.cancel_limit`

        """
        self.assertIsNone(state.get('termtime.events'))
        self.assertIsNone(state.get('comparison.events'))

        # Limit cancellation to 2
        state.set('booker.cancel_limit', 2)

        # Have no termtime events
        state.set('termtime.events', [])

        booker.op_cancel_events()

        # Need for deletion of all three was detected by comparison
        self.assertEqual(len(state.get('comparison.events.delete')), 3)

        # Only 2 PUT requests made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 2)

        # Both PUT requests were to cancel events
        for put in self.requests['PUT']:
            self.assertEqual(put['data']['Status'], 'Cancelled')


class ShouldIgnoreBookingsFactoryTest(unittest.TestCase):
    """ Tests for `booker.should_ignore_bookings_factory` """

    def setUp(self):
        super().setUp()
        self.faker = Faker()

        # Create random set of users
        self.users = [self.faker.bothify('???##') for i in range(9)]
        # Create random set of rooms
        self.rooms = [self.faker.bothify('?###-##-###') for i in range(17)]
        # List of booking types
        self.types = ['Meeting', 'Teaching', 'Exam', 'Cleaning']
        # Create random set of bookings for filtering against
        while True:
            self.bookings = [self.create_fake_booking() for i in range(1000)]
            # guarantee all users, rooms and types have been used
            if ({b['BookedBy']['Id'] for b in self.bookings} == set(self.users) and
                    {b['Room']['Id'] for b in self.bookings} == set(self.rooms) and
                    {b['Type'] for b in self.bookings} == set(self.types)):
                break
        # Construct set of possible buildings from first part of rooms
        self.buildings = {r[:4] for r in self.rooms}

    def create_fake_booking(self):
        room_id = self.faker.random_element(self.rooms)
        return {
            'BookedBy': {
                'Id': self.faker.random_element(self.users),
            },
            'Room': {
                'Id': room_id,
                'Building': {
                    'Id': room_id[:4],  # First part of room considered building
                }
            },
            'Type': self.faker.random_element(self.types),
        }

    def test_factory_returns_callable(self):
        """
        Passing no filters to factory returns a callable that always returns False
        or raises an exception if a non-list is provided. Passing a valid list of
        filters returns a callable.

        """
        # None returns always False function
        func = booker.should_ignore_bookings_factory(None)
        self.assertTrue(callable(func))
        self.assertFalse(func({}))
        # Empty list returns always False function too
        func = booker.should_ignore_bookings_factory([])
        self.assertTrue(callable(func))
        self.assertFalse(func({}))
        # Non-list raises ConfigurationInvalid exception
        with self.assertRaises(exceptions.ConfigurationInvalid):
            booker.should_ignore_bookings_factory({})
        # Non-empty list (with valid content) returns a callable
        self.assertTrue(callable(booker.should_ignore_bookings_factory([
            {'by': 'type', 'values': ['Meeting']},
        ])))

    def test_filter_list_validation(self):
        """
        Passing invalid filter list content causes ConfigurationInvalid exceptions
        to be raised.

        """
        # No 'by' in filter
        with self.assertRaises(exceptions.ConfigurationInvalid):
            booker.should_ignore_bookings_factory([{'values': ['test']}])
        # No 'values' in filter
        with self.assertRaises(exceptions.ConfigurationInvalid):
            booker.should_ignore_bookings_factory([{'by': 'type'}])
        # Invalid 'by' (doesn't match a possible check to be made) in filter
        with self.assertRaises(exceptions.ConfigurationInvalid):
            booker.should_ignore_bookings_factory([{'by': 'badcheck', 'values': ['test']}])
        # Non-list for 'values' in filter
        with self.assertRaises(exceptions.ConfigurationInvalid):
            booker.should_ignore_bookings_factory([{'by': 'type', 'values': 'not_a_list'}])

    def test_filter_booked_by(self):
        """
        Passing a filter using 'booked_by' produces a function that only returns True for
        bookings with matching BookedBy.Id

        """
        MATCHED_USER = self.faker.random_element(self.users)
        matcher = booker.should_ignore_bookings_factory([
            {'by': 'booked_by', 'values': [MATCHED_USER]}
        ])
        filtered_bookings = [b for b in self.bookings if matcher(b)]
        for b in filtered_bookings:
            self.assertEqual(b['BookedBy']['Id'], MATCHED_USER)

        # Multiple values
        MATCHED_USERS = self.faker.random_elements(
            self.users, unique=True, length=self.faker.random_int(2, len(self.users))
        )
        matcher = booker.should_ignore_bookings_factory([
            {'by': 'booked_by', 'values': MATCHED_USERS}
        ])
        filtered_bookings = [b for b in self.bookings if matcher(b)]
        for b in filtered_bookings:
            self.assertIn(b['BookedBy']['Id'], MATCHED_USERS)

    def test_filter_room(self):
        """
        Passing a filter using 'room' produces a function that only returns True for
        bookings with matching Room.Id

        """
        MATCHED_ROOM = self.faker.random_element(self.rooms)
        matcher = booker.should_ignore_bookings_factory([
            {'by': 'room', 'values': [MATCHED_ROOM]}
        ])
        filtered_bookings = [b for b in self.bookings if matcher(b)]
        for b in filtered_bookings:
            self.assertEqual(b['Room']['Id'], MATCHED_ROOM)

        # Multiple values
        MATCHED_ROOMS = self.faker.random_elements(
            self.rooms, unique=True, length=self.faker.random_int(2, len(self.rooms))
        )
        matcher = booker.should_ignore_bookings_factory([
            {'by': 'room', 'values': MATCHED_ROOMS}
        ])
        filtered_bookings = [b for b in self.bookings if matcher(b)]
        for b in filtered_bookings:
            self.assertIn(b['Room']['Id'], MATCHED_ROOMS)

    def test_filter_building(self):
        """
        Passing a filter using 'building' produces a function that only returns True for
        bookings with matching Room.Building.Id

        """
        MATCHED_BUILDING = self.faker.random_element(self.rooms)[:4]
        matcher = booker.should_ignore_bookings_factory([
            {'by': 'building', 'values': [MATCHED_BUILDING]}
        ])
        filtered_bookings = [b for b in self.bookings if matcher(b)]
        for b in filtered_bookings:
            self.assertEqual(b['Room']['Building']['Id'], MATCHED_BUILDING)

        # Multiple values
        MATCHED_BUILDINGS = self.faker.random_elements(
            self.buildings, unique=True, length=self.faker.random_int(2, len(self.buildings))
        )
        matcher = booker.should_ignore_bookings_factory([
            {'by': 'building', 'values': MATCHED_BUILDINGS}
        ])
        filtered_bookings = [b for b in self.bookings if matcher(b)]
        for b in filtered_bookings:
            self.assertIn(b['Room']['Building']['Id'], MATCHED_BUILDINGS)

    def test_filter_type(self):
        """
        Passing a filter using 'type' produces a function that only returns True for
        bookings with matching Type

        """
        MATCHED_TYPE = self.faker.random_element(self.types)
        matcher = booker.should_ignore_bookings_factory([
            {'by': 'type', 'values': [MATCHED_TYPE]}
        ])
        filtered_bookings = [b for b in self.bookings if matcher(b)]
        for b in filtered_bookings:
            self.assertEqual(b['Type'], MATCHED_TYPE)

        # Multiple values
        MATCHED_TYPES = self.faker.random_elements(
            self.types, unique=True, length=self.faker.random_int(2, len(self.types))
        )
        matcher = booker.should_ignore_bookings_factory([
            {'by': 'type', 'values': MATCHED_TYPES}
        ])
        filtered_bookings = [b for b in self.bookings if matcher(b)]
        for b in filtered_bookings:
            self.assertIn(b['Type'], MATCHED_TYPES)

    def test_filter_multiple_bys(self):
        """
        Multiple filter conditions (`by`s) can be used. Function returns True if any
        conditions match, therefore inversely failures to match will not match any
        condition.

        """
        # Filter by two of every category
        MATCHED_USERS = self.faker.random_elements(self.users, unique=True, length=2)
        MATCHED_ROOMS = self.faker.random_elements(self.rooms, unique=True, length=2)
        MATCHED_BUILDINGS = self.faker.random_elements(self.buildings, unique=True, length=2)
        MATCHED_TYPES = self.faker.random_elements(self.types, unique=True, length=2)

        filter_list = [
            {'by': 'booked_by', 'values': MATCHED_USERS},
            {'by': 'room', 'values': MATCHED_ROOMS},
            {'by': 'building', 'values': MATCHED_BUILDINGS},
            {'by': 'type', 'values': MATCHED_TYPES},
        ]
        matcher = booker.should_ignore_bookings_factory(filter_list)

        # get bookings *NOT* matching any of the conditions
        ignored_bookings = [b for b in self.bookings if not matcher(b)]

        for b in ignored_bookings:
            # No conditions should match
            self.assertNotIn(b['BookedBy']['Id'], MATCHED_USERS)
            self.assertNotIn(b['Room']['Id'], MATCHED_ROOMS)
            self.assertNotIn(b['Room']['Building']['Id'], MATCHED_BUILDINGS)
            self.assertNotIn(b['Type'], MATCHED_TYPES)
