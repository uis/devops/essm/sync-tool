# Test utils.py functions
import unittest
import datetime
from dateutil import tz
import requests_mock
import json
from freezegun import freeze_time
from .. import utils
from .. import exceptions
from .. import state


class DateConversionTestCase(unittest.TestCase):

    def setUp(self):
        super().setUp()

        # Make sure state is clear
        state.state = {}

    def test_booker_parse_datetime(self):
        """
        Given a ISO 8601 "zulu" string, returns a datetime.datetime with UTC timezone

        """
        # Date outside DST
        self.assertEqual(
            utils.booker_parse_datetime('2001-02-03T04:05:06Z'),
            datetime.datetime(2001, 2, 3, 4, 5, 6, tzinfo=tz.UTC)
        )

        # Date inside DST
        self.assertEqual(
            utils.booker_parse_datetime('2007-08-09T10:11:12Z'),
            datetime.datetime(2007, 8, 9, 10, 11, 12, tzinfo=tz.UTC)
        )

        # Invalid format raises exception
        with self.assertRaisesRegex(exceptions.ConversionError, 'Invalid Booker datetime'):
            utils.booker_parse_datetime('invalid-format')
        # Unparsable date raises exception
        with self.assertRaisesRegex(exceptions.ConversionError, 'Unable to parse Booker datetime'):
            utils.booker_parse_datetime('2013-14-15T16:17:18Z')

    def test_booker_format_datetime(self):
        """
        Given a datetime.datetime object (with timezone), returns an ISO 8601 "zulu" string

        """
        # Date outside DST with UTC timezone
        self.assertEqual(
            utils.booker_format_datetime(
                datetime.datetime(2001, 2, 3, 4, 5, 6, tzinfo=tz.UTC)
            ),
            '2001-02-03T04:05:06Z'
        )
        # Date outside DST with GB timezone
        self.assertEqual(
            utils.booker_format_datetime(
                datetime.datetime(2001, 2, 3, 4, 5, 6, tzinfo=tz.gettz('GB'))
            ),
            '2001-02-03T04:05:06Z'
        )

        # Date inside DST with UTC tinezone
        self.assertEqual(
            utils.booker_format_datetime(
                datetime.datetime(2007, 8, 9, 10, 11, 12, tzinfo=tz.UTC)
            ),
            '2007-08-09T10:11:12Z'
        )
        # Date inside DST with GB timezone (hour decreased for UTC result)
        self.assertEqual(
            utils.booker_format_datetime(
                datetime.datetime(2007, 8, 9, 10, 11, 12, tzinfo=tz.gettz('GB'))
            ),
            '2007-08-09T09:11:12Z'
        )

        # Datetime object without timezone raises exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'Booker datetime missing timezone'):
            utils.booker_format_datetime(datetime.datetime(2007, 8, 9, 10, 11, 12))

    def test_termtime_parse_datetime(self):
        """
        Given a datetime string in local UK time of format "yyyy-mm-dd hh:mm:ss",
        returns a datetime.datetime with GB timezone

        """
        # Date outside DST
        self.assertEqual(
            utils.termtime_parse_datetime('2001-02-03 04:05:06'),
            datetime.datetime(2001, 2, 3, 4, 5, 6, tzinfo=tz.gettz('GB'))
        )

        # Date inside DST
        self.assertEqual(
            utils.termtime_parse_datetime('2007-08-09 10:11:12'),
            datetime.datetime(2007, 8, 9, 10, 11, 12, tzinfo=tz.gettz('GB'))
        )

        # Invalid format raises exception
        with self.assertRaisesRegex(exceptions.ConversionError, 'Invalid TermTime datetime'):
            utils.termtime_parse_datetime('invalid-format')
        # Unparsable date raises exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'Unable to parse TermTime datetime'):
            utils.termtime_parse_datetime('2013-14-15 16:17:18')

    def test_termtime_format_datetime(self):
        """
        Given a datetime.datetime (with timezone), returns a string in local UK time
        of format "yyyy-mm-dd hh:mm:ss"

        """
        # Date outside DST with UTC timezone
        self.assertEqual(
            utils.termtime_format_datetime(
                datetime.datetime(2001, 2, 3, 4, 5, 6, tzinfo=tz.UTC)
            ),
            '2001-02-03 04:05:06'
        )
        # Date outside DST with GB timezone
        self.assertEqual(
            utils.termtime_format_datetime(
                datetime.datetime(2001, 2, 3, 4, 5, 6, tzinfo=tz.gettz('GB'))
            ),
            '2001-02-03 04:05:06'
        )

        # Date inside DST with UTC tinezone (hour increased for DST result)
        self.assertEqual(
            utils.termtime_format_datetime(
                datetime.datetime(2007, 8, 9, 10, 11, 12, tzinfo=tz.UTC)
            ),
            '2007-08-09 11:11:12'
        )
        # Date inside DST with GB timezone
        self.assertEqual(
            utils.termtime_format_datetime(
                datetime.datetime(2007, 8, 9, 10, 11, 12, tzinfo=tz.gettz('GB'))
            ),
            '2007-08-09 10:11:12'
        )

        # Datetime object without timezone raises exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'TermTime datetime missing timezone'):
            utils.termtime_format_datetime(datetime.datetime(2007, 8, 9, 10, 11, 12))

    def test_termtime_booking_parse_datetime(self):
        """
        Given a dict of containing time (hh:mm), du (hh:mm), day (1-7, 1=Mon),
        pat (ISO week, yyyy-Www) in local UK time, returns a tuple of start and end
        datetime.datetime with GB timezones

        Alternatively, the 'pat' key may be 'weeks' (but with only a single week),
        and the 'day' key may be 'days' with a single 'Mon' to 'Sun' value

        """
        # Date outside DST
        self.assertEqual(
            utils.termtime_booking_parse_datetime(
                {'time': '03:04', 'du': '02:30', 'day': 4, 'pat': '2020-W01'}
            ),
            (
                datetime.datetime(2020, 1, 2, 3, 4, tzinfo=tz.gettz('GB')),
                datetime.datetime(2020, 1, 2, 5, 34, tzinfo=tz.gettz('GB'))
            )
        )
        # Date inside DST
        self.assertEqual(
            utils.termtime_booking_parse_datetime(
                {'time': '10:11', 'du': '01:00', 'day': 1, 'pat': '2020-W25'}
            ),
            (
                datetime.datetime(2020, 6, 15, 10, 11, tzinfo=tz.gettz('GB')),
                datetime.datetime(2020, 6, 15, 11, 11, tzinfo=tz.gettz('GB'))
            )
        )

        # Using 'weeks' instead of 'pat' and 'days' instead of 'day'
        self.assertEqual(
            utils.termtime_booking_parse_datetime(
                {'time': '10:11', 'du': '01:00', 'days': 'Mon', 'weeks': '2020-W25'}
            ),
            (
                datetime.datetime(2020, 6, 15, 10, 11, tzinfo=tz.gettz('GB')),
                datetime.datetime(2020, 6, 15, 11, 11, tzinfo=tz.gettz('GB'))
            )
        )

        # Missing fields/keys raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    r'TermTime booking missing field\(s\)'):
            utils.termtime_booking_parse_datetime({})
        # Invalid ISO week pattern raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'Invalid TermTime ISO week'):
            utils.termtime_booking_parse_datetime(
                {'time': '01:02', 'du': '03:04', 'day': 5, 'pat': 'BAD'}
            )
        # Invalid time format raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'Invalid TermTime time'):
            utils.termtime_booking_parse_datetime(
                 {'time': 'BAD', 'du': '03:04', 'day': 5, 'pat': '2020-W21'}
            )
        # Invalid duration format raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'Invalid TermTime duration'):
            utils.termtime_booking_parse_datetime(
                 {'time': '01:02', 'du': 'BAD', 'day': 5, 'pat': '2020-W21'}
            )
        # Unparsable time raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'Unable to parse TermTime booking start time'):
            utils.termtime_booking_parse_datetime(
                 {'time': '26:00', 'du': '03:04', 'day': 5, 'pat': '2020-W21'}
            )

        # Providing multiple days raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'Unsupported multiple TermTime booking days'):
            utils.termtime_booking_parse_datetime(
                 {'time': '01:02', 'du': '03:04', 'days': "Mon,Tue", 'pat': '2020-W21'}
            )
        # Providing multiple weeks raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'Unsupported multiple TermTime booking weeks'):
            utils.termtime_booking_parse_datetime(
                 {'time': '01:02', 'du': '03:04', 'day': 5, 'weeks': '2020-W21,2020-W22'}
            )
        # Providing invalid days value raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'Invalid TermTime booking days'):
            utils.termtime_booking_parse_datetime(
                 {'time': '01:02', 'du': '03:04', 'days': "BAD", 'pat': '2020-W21'}
            )

    def test_termtime_booking_format_datetime(self):
        """
        Given a start and end datetime.datetime objects (with timezone) on the same day,
        returns a dict containing time (hh:mm), du (hh:mm), day (1-7, 1=Mon),
        pat (ISO week, yyyy-Www) in local UK time

        """

        # Dates outside DST with UTC timezones
        self.assertEqual(
            utils.termtime_booking_format_datetime(
                datetime.datetime(2020, 1, 2, 3, 4, 5, tzinfo=tz.UTC),
                datetime.datetime(2020, 1, 2, 5, 34, 56, tzinfo=tz.UTC)
            ),
            {'time': '03:04', 'du': '02:30', 'day': 4, 'pat': '2020-W01'}
        )
        # Dates outside DST with GB timezones
        self.assertEqual(
            utils.termtime_booking_format_datetime(
                datetime.datetime(2020, 1, 2, 3, 4, 5, tzinfo=tz.gettz('GB')),
                datetime.datetime(2020, 1, 2, 5, 34, 56, tzinfo=tz.gettz('GB'))
            ),
            {'time': '03:04', 'du': '02:30', 'day': 4, 'pat': '2020-W01'}
        )

        # Dates inside DST with UTC timezones (time increased an hour for DST result)
        self.assertEqual(
            utils.termtime_booking_format_datetime(
                datetime.datetime(2020, 8, 9, 10, 11, 12, tzinfo=tz.UTC),
                datetime.datetime(2020, 8, 9, 10, 56, 56, tzinfo=tz.UTC)
            ),
            {'time': '11:11', 'du': '00:45', 'day': 7, 'pat': '2020-W32'}
        )
        # Dates inside DST with GB timezones
        self.assertEqual(
            utils.termtime_booking_format_datetime(
                datetime.datetime(2020, 8, 9, 10, 11, 12, tzinfo=tz.gettz('GB')),
                datetime.datetime(2020, 8, 9, 10, 56, 56, tzinfo=tz.gettz('GB'))
            ),
            {'time': '10:11', 'du': '00:45', 'day': 7, 'pat': '2020-W32'}
        )

        # Dates not on the same day raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'TermTime dates not on same day'):
            utils.termtime_booking_format_datetime(
                datetime.datetime(2020, 8, 9, 10, 11, 12, tzinfo=tz.UTC),
                datetime.datetime(2020, 8, 10, 10, 56, 56, tzinfo=tz.UTC)
            )
        # Start datetime object without timezone raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'TermTime start datetime missing timezone'):
            utils.termtime_booking_format_datetime(
                datetime.datetime(2020, 8, 9, 10, 11, 12),
                datetime.datetime(2020, 8, 10, 10, 56, 56, tzinfo=tz.UTC)
            )
        # End datetime object without timezone raises an exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'TermTime end datetime missing timezone'):
            utils.termtime_booking_format_datetime(
                datetime.datetime(2020, 8, 9, 10, 11, 12, tzinfo=tz.UTC),
                datetime.datetime(2020, 8, 10, 10, 56, 56)
            )

    def test_localtime_hhmm(self):
        """
        Given a datetime.datetime object (with timezone), returns a string of format "hh:mm"

        """
        # Date outside DST with UTC timezone
        self.assertEqual(
            utils.localtime_hhmm(datetime.datetime(2001, 2, 3, 4, 5, 6, tzinfo=tz.UTC)),
            '04:05'
        )
        # Date outside DST with GB timezone
        self.assertEqual(
            utils.localtime_hhmm(datetime.datetime(2001, 2, 3, 4, 5, 6, tzinfo=tz.gettz('GB'))),
            '04:05'
        )

        # Date inside DST with UTC timezone (hour increased for DST result)
        self.assertEqual(
            utils.localtime_hhmm(datetime.datetime(2007, 8, 9, 10, 11, 12, tzinfo=tz.UTC)),
            '11:11'
        )
        # Date inside DST with GB timezone
        self.assertEqual(
            utils.localtime_hhmm(datetime.datetime(2007, 8, 9, 10, 11, 12, tzinfo=tz.gettz('GB'))),
            '10:11'
        )

        # Datetime object without timezone raises exception
        with self.assertRaisesRegex(exceptions.ConversionError,
                                    'Localtime missing timezone'):
            utils.localtime_hhmm(datetime.datetime(2007, 8, 9, 10, 11, 12))

    @freeze_time('2021-01-21 12:21:12')
    def test_now(self):
        """
        Returns datetime.now() unless `now` state set (and contains valid
        ISO 8901 "Zulu" string)

        """
        # Without `now` state, returns now (safe due to freeze_time)
        self.assertIsNone(state.get('now'))
        self.assertEqual(utils.now(), datetime.datetime.now(tz=tz.UTC))

        # Returns `now` (as datetime if provided)
        OVERRIDE_NOW = '2021-01-30T10:00:00Z'
        state.set('now', OVERRIDE_NOW)
        self.assertEqual(utils.now().strftime('%Y-%m-%dT%H:%M:%S') + 'Z', OVERRIDE_NOW)

        # Invalid `now` format raises exception
        state.set('now', 'invalid-format')
        with self.assertRaisesRegex(exceptions.ConversionError, 'Invalid "now" datetime'):
            utils.now()

        # Unparsable `now` date raises exception
        state.set('now', '2013-14-15T16:17:18Z')
        with self.assertRaisesRegex(exceptions.ConversionError, 'Unable to parse "now" datetime'):
            utils.now()


class ExampleAPIError(RuntimeError):
    pass


class APITestCase(unittest.TestCase):

    def setUp(self):
        super().setUp()

        self.api_functions = [
            ('GET', utils.api_get),
            ('POST', utils.api_post),
            ('PUT', utils.api_put),
            ('DELETE', utils.api_delete),
        ]

        self.requests_mock = requests_mock.Mocker(case_sensitive=True)

        # Mock requests to test/echo endpoint for all methods to use echo_response handler
        for method, _ in self.api_functions:
            self.requests_mock.register_uri(
                method, 'http://test/echo', json=APITestCase.echo_response
            )

        self.requests_mock.start()

    def tearDown(self):
        super().tearDown()
        self.requests_mock.stop()

    @staticmethod
    def echo_response(request, context):
        return {
            'success': True,
            'method': request.method,
            'query': {
                k: v if len(v) > 1 else v[0]  # flatten single element lists
                for k, v in request.qs.items()
            },
            'body': json.loads(request.text) if request.text else ''
        }

    def test_success(self):
        """
        API calls return json responses on success

        """
        QUERY_STRING = {'p1': 'test'}
        BODY_JSON = {'b1': 'test'}
        for method, func in self.api_functions:
            result, _ = func('http://test/echo', params=QUERY_STRING, json=BODY_JSON)
            self.assertTrue(result.get('success'))
            self.assertEqual(result.get('method'), method)
            self.assertEqual(result.get('query'), QUERY_STRING)
            self.assertEqual(result.get('body'), BODY_JSON)

    def test_retries_exception(self):
        """
        Retries calls to failing API calls a given number of times before finally giving up
        with given exception

        """
        # mock failing endpoint
        self.requests_mock.register_uri('GET', 'http://test/fail', status_code=404)

        for retries in range(1, 4):
            # Reset mock call count
            self.requests_mock.reset_mock()
            # Call will raise specified exception
            with self.assertRaises(ExampleAPIError):
                utils.api_get('http://test/fail',
                              retries=retries, sleep=1,
                              exception=ExampleAPIError)
            # mock request call count matches number of retries plus one for original call
            self.assertEqual(self.requests_mock.call_count, retries+1)

    def test_retries_success(self):
        """
        Retries calls to an initially failing API before succeeding

        """
        # mock initially failing endpoint (fails twice then echo response)
        RETRIES = 2
        responses = RETRIES * [{'status_code': 500}] + [{'json': APITestCase.echo_response}]
        self.requests_mock.register_uri('GET', 'http://test/initfail', responses)

        result, _ = utils.api_get('http://test/initfail', retries=RETRIES, sleep=1)
        # successfully gets a result after all responses attempted
        self.assertTrue(result.get('success'))
        self.assertEqual(self.requests_mock.call_count, len(responses))
