# Test comparison operations
from . import TestCase
from .. import state
from .. import comparison
from .. import booker
from .. import termtime


class ComparisonTests(TestCase):

    def _assert_list_contains(self, list, key, value):
        """
        asserts that a list of dicts contains (at least one) member with key,
        value pair

        """
        filtered = [m for m in list if m.get(key) == value]
        self.assertGreater(len(filtered), 0)

    def _assert_list_doesnt_contain(self, list, key, value):
        """
        asserts that a list of dicts contains no members with key, value pair

        """
        filtered = [m for m in list if m.get(key) == value]
        self.assertEqual(len(filtered), 0)

    def _assert_comparison_invalid(self, compare_func, *,
                                   key, termtime_data, booker_data, expected):
        """
        asserts that calling the `compare_func` with given data in termtime and
        booker state as defined by `key` causes an entry to be added to
        `comparison.{key}.invalid` that matches `expected`

        """
        # Make sure comparison result is clear
        state.set(f'comparison.{key}', None)

        state.set(f'termtime.{key}', termtime_data)
        state.set(f'booker.{key}', booker_data)
        compare_func()

        s = state.get(f'comparison.{key}.invalid')
        # Result is a list with a single entry
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        invalid_entry = s[0]
        # Values of keys in expected match entry
        for k, v in expected.items():
            self.assertEqual(v, invalid_entry[k])


class CompareDeptsTest(ComparisonTests):

    def test_compare_depts_all_match(self):
        """
        Comparison compare_depts operation with default fixtures matches all
        departments

        """
        self.assertIsNone(state.get('booker.depts'))
        self.assertIsNone(state.get('termtime.depts'))

        comparison.op_compare_depts()

        # empty invalid, create and update results
        for key in ['invalid', 'create', 'update']:
            s = state.get(f'comparison.depts.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results includes all TermTime related departments
        s = state.get('comparison.depts.match')
        self.assertIsInstance(s, list)
        # fixtures have 2 departments that related to TermTime rooms
        self.assertEqual(len(s), 2)
        # their descriptions ('n') being "Department A" and "Department B"
        for desc in ['Department A', 'Department B']:
            self._assert_list_contains(s, 'n', desc)

    def test_compare_depts_update_needed(self):
        """
        Comparison compare_depts operation with modified fixtures matches all
        departments except one that needs updating

        """
        self.assertIsNone(state.get('booker.depts'))
        self.assertIsNone(state.get('termtime.depts'))

        # Preload the Booker departments
        booker.op_get_depts()

        # Update "Department A" description
        ID = '1001'
        OLD_DESC = 'Department A'
        NEW_DESC = 'New Department Name'
        s = state.get('booker.depts')
        self._assert_list_contains(s, 'Id', ID)
        self._assert_list_contains(s, 'Description', OLD_DESC)
        for d in s:
            if d.get('Id') == ID:
                d['Description'] = NEW_DESC
        self._assert_list_contains(state.get('booker.depts'), 'Description', NEW_DESC)

        # Compare departments
        comparison.op_compare_depts()

        # empty invalid and create results
        for key in ['invalid', 'create']:
            s = state.get(f'comparison.depts.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 1 department
        s = state.get('comparison.depts.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        # update results contains just the updated to department
        s = state.get('comparison.depts.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', ID)
        self._assert_list_contains(s, 'n', NEW_DESC)

    def test_compare_depts_create_needed(self):
        """
        Comparison compare_depts operation with modified fixtures matches all
        departments and a new one needs creating

        """
        self.assertIsNone(state.get('booker.depts'))
        self.assertIsNone(state.get('termtime.depts'))

        # Preload the Booker departments
        booker.op_get_depts()

        # Add a department "Department C"
        NEW_ID = '2001'
        NEW_DESC = 'New Department'
        s = state.get('booker.depts')
        s.append({
            'OptimeIndex': 999,
            'Id': NEW_ID,
            'Description': NEW_DESC,
        })

        self._assert_list_contains(state.get('booker.depts'), 'Id', NEW_ID)

        # Compare departments
        comparison.op_compare_depts()

        # empty invalid or update results
        for key in ['invalid', 'update']:
            s = state.get(f'comparison.depts.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results contains the 2 unchanged departments
        s = state.get('comparison.depts.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # create results contains just the new department
        s = state.get('comparison.depts.create')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', NEW_ID)
        self._assert_list_contains(s, 'n', NEW_DESC)


class CompareSitesTests(ComparisonTests):

    def test_compare_sites_all_match(self):
        """
        Comparison compare_sites operation with default fixtures matches all
        sites

        """
        self.assertIsNone(state.get('booker.sites'))
        self.assertIsNone(state.get('termtime.sites'))

        comparison.op_compare_sites()

        # empty invalid, create and update results
        for key in ['invalid', 'create', 'update']:
            s = state.get(f'comparison.sites.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results includes all TermTime related sites
        s = state.get('comparison.sites.match')
        self.assertIsInstance(s, list)
        # fixtures have 2 sites that related to TermTime rooms
        self.assertEqual(len(s), 2)
        # their descriptions ('n') being "Main Site" and "Second Site"
        for desc in ['Main Site', 'Second Site']:
            self._assert_list_contains(s, 'n', desc)

    def test_compare_sites_update_needed(self):
        """
        Comparison compare_sites operation with modified fixtures matches all
        sites except one that needs updating

        """
        self.assertIsNone(state.get('booker.sites'))
        self.assertIsNone(state.get('termtime.sites'))

        # Preload the Booker sites
        booker.op_get_sites()

        # Update "Second Site" description
        ID = '21000002'
        OLD_DESC = 'Second Site'
        NEW_DESC = 'New Site Name'
        s = state.get('booker.sites')
        self._assert_list_contains(s, 'Id', ID)
        self._assert_list_contains(s, 'Description', OLD_DESC)
        for d in s:
            if d.get('Id') == ID:
                self.assertEqual(d['Description'], OLD_DESC)
                d['Description'] = NEW_DESC
        self._assert_list_contains(state.get('booker.sites'), 'Description', NEW_DESC)

        # Compare sites
        comparison.op_compare_sites()

        # empty invalid and create results
        for key in ['invalid', 'create']:
            s = state.get(f'comparison.sites.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 1 site
        s = state.get('comparison.sites.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        # update results contains just the updated to site
        s = state.get('comparison.sites.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', ID)
        self._assert_list_contains(s, 'n', NEW_DESC)

    def test_compare_sites_create_needed(self):
        """
        Comparison compare_sites operation with modified fixtures matches all
        sites and a new one needs creating

        """
        self.assertIsNone(state.get('booker.sites'))
        self.assertIsNone(state.get('termtime.sites'))

        # Preload the Booker sites
        booker.op_get_sites()

        # Add a site "Remote Site"
        NEW_ID = '21000100'
        NEW_DESC = 'Remote Site'
        s = state.get('booker.sites')
        s.append({
            'OptimeIndex': 999,
            'Id': NEW_ID,
            'Description': NEW_DESC
        })

        self._assert_list_contains(state.get('booker.sites'), 'Id', NEW_ID)

        # Compare sites
        comparison.op_compare_sites()

        # empty invalid or update results
        for key in ['invalid', 'update']:
            s = state.get(f'comparison.sites.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results contains the 2 unchanged sites
        s = state.get('comparison.sites.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # create results contains just the new site
        s = state.get('comparison.sites.create')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', NEW_ID)
        self._assert_list_contains(s, 'n', NEW_DESC)

    def test_compare_sites_bad_api_response(self):
        """
        Comparison compare_sites operation logs warnings and creates invalid
        entries when given bad API responses

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Preload the Booker sites
        booker.op_get_sites()

        for d in state.get('booker.sites'):
            # Invalidate "Main Site" (no Description)
            if d.get('Id') == '21000001':
                del(d['Description'])

        # Compare sites causes warning for site
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_sites()
        self.assertIn(
            "WARNING:essmsync.comparison:Booker entry missing Description - '21000001'",
            log.output
        )

        # site put in invalid results
        s = state.get('comparison.sites.invalid')
        self.assertIsInstance(s, list)
        ids = {r['Id'] for r in s}
        self.assertEqual(ids, {'21000001'})


class CompareBuildingsTests(ComparisonTests):

    def test_compare_buildings_all_match(self):
        """
        Comparison compare_buildings operation with default fixtures matches all
        buildings

        """
        self.assertIsNone(state.get('booker.buildings'))
        self.assertIsNone(state.get('termtime.buildings'))

        comparison.op_compare_buildings()

        # empty invalid, create and update results
        for key in ['invalid', 'create', 'update']:
            s = state.get(f'comparison.buildings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results includes all TermTime related buildings
        s = state.get('comparison.buildings.match')
        self.assertIsInstance(s, list)
        # fixtures have 3 buildings that related to TermTime rooms
        self.assertEqual(len(s), 3)
        # their Ids/codes being "B001", "B002" and "B201"
        for c in ['B001', 'B002', 'B201']:
            self._assert_list_contains(s, 'code', c)

    def test_compare_buildings_update_desc(self):
        """
        Comparison compare_buildings operation with modified fixtures matches all
        buildings except the one with updated Description

        """
        self.assertIsNone(state.get('booker.buildings'))
        self.assertIsNone(state.get('termtime.buildings'))

        # Preload the Booker buildings
        booker.op_get_buildings()

        # Update "Building Two" description
        ID = 'B002'
        OLD_DESC = 'Building Two'
        NEW_DESC = 'Newly Named Building'
        s = state.get('booker.buildings')
        self._assert_list_contains(s, 'Id', ID)
        self._assert_list_contains(s, 'Description', OLD_DESC)
        for d in s:
            if d.get('Id') == ID:
                self.assertEqual(d['Description'], OLD_DESC)
                d['Description'] = NEW_DESC
        self._assert_list_contains(state.get('booker.buildings'), 'Description', NEW_DESC)

        # Compare buildings
        comparison.op_compare_buildings()

        # empty invalid and create results
        for key in ['invalid', 'create']:
            s = state.get(f'comparison.buildings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 2 buildings
        s = state.get('comparison.buildings.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # update results contains just the updated building
        s = state.get('comparison.buildings.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', ID)
        self._assert_list_contains(s, 'n', NEW_DESC)

    def test_compare_buildings_update_site(self):
        """
        Comparison compare_buildings operation with modified fixtures matches all
        buildings except the one with updated Site

        """
        self.assertIsNone(state.get('booker.buildings'))
        self.assertIsNone(state.get('termtime.buildings'))

        # Preload the Booker buildings
        booker.op_get_buildings()

        # Update "Building Two" site (1 - Main Site to 2 - Second Site)
        ID = 'B002'
        OLD_SITE_OI = 1
        NEW_SITE_OI = 2
        NEW_SITE_CODE = '21000002'
        NEW_SITE_DESC = 'Second Site'
        s = state.get('booker.buildings')
        self._assert_list_contains(s, 'Id', ID)
        for d in s:
            if d.get('Id') == ID:
                self.assertEqual(d['SiteId'], OLD_SITE_OI)
                d['SiteId'] = NEW_SITE_OI
                d['Site'] = {
                    'OptimeIndex': NEW_SITE_OI,
                    'Id': NEW_SITE_CODE,
                    'Description': NEW_SITE_DESC,
                }

        # Compare buildings
        comparison.op_compare_buildings()

        # empty invalid and create results
        for key in ['invalid', 'create']:
            s = state.get(f'comparison.buildings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 2 buildings
        s = state.get('comparison.buildings.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # update results contains just the updated building
        s = state.get('comparison.buildings.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', ID)
        # TermTime update API uses 'camp' (campus code)
        self._assert_list_contains(s, 'camp', NEW_SITE_CODE)

    def test_compare_buildings_create_needed(self):
        """
        Comparison compare_buildings operation with modified fixtures matches all
        buildings and a new one needs creating

        """
        self.assertIsNone(state.get('booker.buildings'))
        self.assertIsNone(state.get('termtime.buildings'))

        # Preload the Booker buildings
        booker.op_get_buildings()

        # Add a building "Shiny New Building"
        NEW_ID = 'S100'
        NEW_DESC = 'Shiny New Building'
        SITE_OI = 2
        SITE_ID = '21000002'
        SITE_DESC = 'Second Site'
        s = state.get('booker.buildings')
        s.append({
            'OptimeIndex': 999,
            'Id': NEW_ID,
            'Description': NEW_DESC,
            'SiteId': SITE_OI,
            'Site': {
                'OptimeIndex': SITE_OI,
                'Id': SITE_ID,
                'Description': SITE_DESC,
            }
        })

        self._assert_list_contains(state.get('booker.buildings'), 'Id', NEW_ID)

        # Compare buildings
        comparison.op_compare_buildings()

        # empty invalid or update results
        for key in ['invalid', 'update']:
            s = state.get(f'comparison.buildings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results contains the 3 unchanged buildings
        s = state.get('comparison.buildings.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 3)
        # create results contains just the new building
        s = state.get('comparison.buildings.create')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', NEW_ID)
        self._assert_list_contains(s, 'n', NEW_DESC)
        # TermTime update API uses 'camp' (campus code)
        self._assert_list_contains(s, 'camp', SITE_ID)

    def test_compare_buildings_bad_api_response(self):
        """
        Comparison compare_buildings operation logs warnings and creates invalid
        entries when given bad API responses

        """
        self.assertIsNone(state.get('booker.buildings'))
        self.assertIsNone(state.get('termtime.buildings'))

        # Preload the Booker buildings and sites
        booker.op_get_buildings()
        booker.op_get_sites()

        # Invalidate "Building One" (no SiteId) and "Building Two" (invalid SiteId)
        for d in state.get('booker.buildings'):
            if d.get('Id') == 'B001':
                del(d['SiteId'])
            if d.get('Id') == 'B002':
                d['SiteId'] = 666

        # Compare buildings causes warnings for both
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_buildings()
        self.assertTrue(set(log.output).issuperset({
            "WARNING:essmsync.comparison:Booker building missing SiteId - 'B001'",
            "WARNING:essmsync.comparison:Booker building has missing SiteId '666' - 'B002'"
        }))

        # both buildings put in invalid results
        s = state.get('comparison.buildings.invalid')
        self.assertIsInstance(s, list)
        ids = {r['Id'] for r in s}
        self.assertEqual(ids, {'B001', 'B002'})


class CompareRoomsTests(ComparisonTests):

    def test_compare_rooms_all_match(self):
        """
        Comparison compare_rooms operation with default fixtures matches all
        rooms

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        comparison.op_compare_rooms()

        # empty invalid, create and update results
        for key in ['invalid', 'create', 'update']:
            s = state.get(f'comparison.rooms.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results includes all TermTime related rooms
        s = state.get('comparison.rooms.match')
        self.assertIsInstance(s, list)
        # fixtures have 4 rooms that related to TermTime rooms
        self.assertEqual(len(s), 4)

    def test_compare_rooms_update_desc(self):
        """
        Comparison compare_rooms operation with modified fixtures matches all
        rooms except the one with updated Description

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Preload the Booker rooms
        booker.op_get_rooms()

        # Update "Seminar Room A" description
        ID = 'B002-00-0001'
        OLD_DESC = 'Seminar Room A'
        NEW_DESC = 'Newly Named Room'
        s = state.get('booker.rooms')
        self._assert_list_contains(s, 'Id', ID)
        self._assert_list_contains(s, 'Description', OLD_DESC)
        for d in s:
            if d.get('Id') == ID:
                self.assertEqual(d['Description'], OLD_DESC)
                d['Description'] = NEW_DESC
        self._assert_list_contains(state.get('booker.rooms'), 'Description', NEW_DESC)

        # Compare rooms
        comparison.op_compare_rooms()

        # empty invalid and create results
        for key in ['invalid', 'create']:
            s = state.get(f'comparison.rooms.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 3 rooms
        s = state.get('comparison.rooms.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 3)
        # update results contains just the updated room
        s = state.get('comparison.rooms.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', ID)
        self._assert_list_contains(s, 'n', NEW_DESC)

    def test_compare_rooms_update_dept(self):
        """
        Comparison compare_rooms operation with modified fixtures matches all
        rooms except the one with updated department

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Preload the Booker rooms
        booker.op_get_rooms()

        # Update "Seminar Room A" department from
        # (2, 1002, Department B) to (1, 1001, Department A)
        ID = 'B002-00-0001'
        OLD_DEPT_OI = 2
        NEW_DEPT_OI = 1
        NEW_DESC_ID = '1001'
        s = state.get('booker.rooms')
        self._assert_list_contains(s, 'Id', ID)
        for d in s:
            if d.get('Id') == ID:
                self.assertEqual(d['Departments'][0], OLD_DEPT_OI)
                d['Departments'] = [NEW_DEPT_OI]

        # Compare rooms
        comparison.op_compare_rooms()

        # empty invalid and create results
        for key in ['invalid', 'create']:
            s = state.get(f'comparison.rooms.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 3 rooms
        s = state.get('comparison.rooms.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 3)
        # update results contains just the updated room
        s = state.get('comparison.rooms.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', ID)
        # TermTime update API uses dn for department
        self._assert_list_contains(s, 'dn', NEW_DESC_ID)

    def test_compare_rooms_update_building(self):
        """
        Comparison compare_rooms operation with modified fixtures matches all
        rooms except the one with updated building

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Preload the Booker rooms and buildings
        booker.op_get_rooms()
        booker.op_get_buildings()

        # Update "Seminar Room A" building from
        # (2, B002, Building Two) to (1, B001, Building One)
        ID = 'B002-00-0001'
        OLD_BUILDING_OI = 2
        NEW_BUILDING_OI = 1
        NEW_BUILDING_NAME = 'Building One'

        # First find 'Building One' to change to
        s = state.get('booker.buildings')
        NEW_BUILDING = None
        self._assert_list_contains(s, 'OptimeIndex', NEW_BUILDING_OI)
        for b in s:
            if b.get('OptimeIndex') == NEW_BUILDING_OI:
                self.assertEqual(b.get('Description'), NEW_BUILDING_NAME)
                NEW_BUILDING = b
        self.assertIsNotNone(NEW_BUILDING)

        # Now update room
        s = state.get('booker.rooms')
        self._assert_list_contains(s, 'Id', ID)
        for d in s:
            if d.get('Id') == ID:
                self.assertEqual(d['BuildingId'], OLD_BUILDING_OI)
                d['BuildingId'] = NEW_BUILDING_OI
                d['Building'] = NEW_BUILDING

        # Compare rooms
        comparison.op_compare_rooms()

        # empty invalid and create results
        for key in ['invalid', 'create']:
            s = state.get(f'comparison.rooms.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 3 rooms
        s = state.get('comparison.rooms.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 3)
        # update results contains just the updated room
        s = state.get('comparison.rooms.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', ID)
        # TermTime update API uses building name ('b')
        self._assert_list_contains(s, 'b', NEW_BUILDING_NAME)

    def test_compare_rooms_update_equipment(self):
        """
        Comparison compare_rooms operation with modified fixtures matches all
        rooms except the one with updated Equipment

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Preload the Booker rooms
        booker.op_get_rooms()

        # Update "Seminar Room A" equipment (remove Managed PC and add Data Projector)
        ID = 'B002-00-0001'
        OLD_EQUIPMENT = [1, 3, 5]
        NEW_EQUIPMENT = [1, 4, 5]
        EXPECTED_CAPS = 'Data Projector,Wheelchair Access'
        s = state.get('booker.rooms')
        self._assert_list_contains(s, 'Id', ID)
        for d in s:
            if d.get('Id') == ID:
                self.assertEqual(d['Equipment'], OLD_EQUIPMENT)
                d['Equipment'] = NEW_EQUIPMENT

        # Compare rooms
        comparison.op_compare_rooms()

        # empty invalid and create results
        for key in ['invalid', 'create']:
            s = state.get(f'comparison.rooms.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 3 rooms
        s = state.get('comparison.rooms.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 3)
        # update results contains just the updated room
        s = state.get('comparison.rooms.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        updated_room = s[0]
        self.assertEqual(updated_room['code'], ID)
        self.assertEqual(updated_room['capabilities'], EXPECTED_CAPS)

    def test_compare_rooms_create_needed(self):
        """
        Comparison compare_rooms operation with modified fixtures matches all
        rooms and a new one needs creating

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Override booker rooms list with new room (Teaching Room 3)
        self.set_request_mock('GET', 'booker/api', 'rooms', 'booker-rooms-new-room.json')

        # Preload the Booker rooms
        booker.op_get_rooms()

        NEW_ID = 'B001-03-0001'
        self._assert_list_contains(state.get('booker.rooms'), 'Id', NEW_ID)

        # Compare rooms
        comparison.op_compare_rooms()

        # empty invalid or update results
        for key in ['invalid', 'update']:
            s = state.get(f'comparison.rooms.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results contains the 4 unchanged rooms
        s = state.get('comparison.rooms.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 4)
        # create results contains just the new room
        s = state.get('comparison.rooms.create')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self._assert_list_contains(s, 'code', NEW_ID)

    def test_compare_rooms_bad_building_api_response(self):
        """
        Comparison compare_rooms operation logs warnings and creates invalid
        entries when given bad API responses for buildings

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Preload the Booker rooms
        booker.op_get_rooms()

        for d in state.get('booker.rooms'):
            # Invalidate "Teaching Room 1" (no Building)
            if d.get('Id') == 'B001-01-0001':
                del(d['Building'])
            # Invalidate "Teaching Room 2" (no Building.Id)
            if d.get('Id') == 'B001-02-0001':
                del(d['Building']['Id'])
            # Invalidate "Seminar Room A" (no Building.Description)
            if d.get('Id') == 'B002-00-0001':
                del(d['Building']['Description'])

        # Compare rooms causes warnings for each
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_rooms()
        self.assertTrue(set(log.output).issuperset({
            "WARNING:essmsync.comparison:Booker room missing/invalid Building - 'B001-01-0001'",
            "WARNING:essmsync.comparison:Booker room missing Building.Id - 'B001-02-0001'",
            "WARNING:essmsync.comparison:Booker room missing Building.Description - 'B002-00-0001'"
        }))

        # rooms put in invalid results
        s = state.get('comparison.rooms.invalid')
        self.assertIsInstance(s, list)
        ids = {r['Id'] for r in s}
        self.assertEqual(ids, {'B001-01-0001', 'B001-02-0001', 'B002-00-0001'})

    def test_compare_rooms_bad_site_api_response(self):
        """
        Comparison compare_rooms operation logs warnings and creates invalid
        entries when given bad API responses for sites

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Preload the Booker rooms and sites
        booker.op_get_rooms()
        booker.op_get_sites()

        for d in state.get('booker.rooms'):
            # Invalidate "Teaching Room 1" (no Building.SiteId)
            if d.get('Id') == 'B001-01-0001':
                del(d['Building']['SiteId'])
            # Invalidate "Teaching Room 2" (invalid Building.SiteId)
            if d.get('Id') == 'B001-02-0001':
                d['Building']['SiteId'] = 666

        # Compare rooms causes warnings for both
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_rooms()
        self.assertTrue(set(log.output).issuperset({
            "WARNING:essmsync.comparison:Booker room missing Building.SiteId - 'B001-01-0001'",
            "WARNING:essmsync.comparison:Booker room has missing SiteId '666' - 'B001-02-0001'"
        }))

        # rooms put in invalid results
        s = state.get('comparison.rooms.invalid')
        self.assertIsInstance(s, list)
        ids = {r['Id'] for r in s}
        self.assertEqual(ids, {'B001-01-0001', 'B001-02-0001'})

    def test_compare_rooms_bad_dept_api_response(self):
        """
        Comparison compare_rooms operation logs warnings and creates invalid
        entries when given bad API responses for departments

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Preload the Booker rooms
        booker.op_get_rooms()

        for d in state.get('booker.rooms'):
            # Invalidate "Teaching Room 1" (no Building.Departments)
            if d.get('Id') == 'B001-01-0001':
                del(d['Departments'])
            # Invalidate "Teaching Room 2" (invalid Building.Departments[0])
            if d.get('Id') == 'B001-02-0001':
                d['Departments'] = [666]

        # Compare rooms causes warnings for both
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_rooms()
        print(log.output)
        self.assertTrue(set(log.output).issuperset({
            "WARNING:essmsync.comparison:Booker room missing Department - 'B001-01-0001'",
            "WARNING:essmsync.comparison:Booker room has missing Department '666' - 'B001-02-0001'"
        }))

        # rooms put in invalid results
        s = state.get('comparison.rooms.invalid')
        self.assertIsInstance(s, list)
        ids = {r['Id'] for r in s}
        self.assertEqual(ids, {'B001-01-0001', 'B001-02-0001'})

    def test_compare_rooms_bad_floor_api_response(self):
        """
        Comparison compare_rooms operation logs warnings and creates invalid
        entries when given bad API responses for floors

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Preload the Booker rooms
        booker.op_get_rooms()

        for d in state.get('booker.rooms'):
            # Invalidate "Teaching Room 1" (no Floor)
            if d.get('Id') == 'B001-01-0001':
                del(d['Floor'])

        # Compare rooms causes warning for room
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_rooms()
        self.assertIn(
            "WARNING:essmsync.comparison:Booker room missing Floor - 'B001-01-0001'",
            log.output
        )

        # room put in invalid results
        s = state.get('comparison.rooms.invalid')
        self.assertIsInstance(s, list)
        ids = {r['Id'] for r in s}
        self.assertEqual(ids, {'B001-01-0001'})

    def test_compare_rooms_bad_equipment_api_response(self):
        """
        Comparison compare_rooms operation logs warnings and creates invalid
        entries when given bad API responses for equipment

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('termtime.rooms'))

        # Preload the Booker rooms
        booker.op_get_rooms()

        for d in state.get('booker.rooms'):
            # Invalidate "Teaching Room 1" (no Equipment)
            if d.get('Id') == 'B001-01-0001':
                del(d['Equipment'])

        # Preload the TermTime capabilities
        termtime.op_get_capabilities()

        # Add two extra capabilities that are not in Booker
        state.get('termtime.capabilities').extend(["Time Machine", "Fusion Reactor"])

        # Compare rooms causes warning for room and missing equipment
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_rooms()
        self.assertTrue(set(log.output).issuperset({
            "WARNING:essmsync.comparison:Booker room missing Equipment - 'B001-01-0001'",
            "WARNING:essmsync.comparison:Booker missing equipment for TermTime capabilities - "
            "'Fusion Reactor, Time Machine'"
        }))

        # room put in invalid results
        s = state.get('comparison.rooms.invalid')
        self.assertIsInstance(s, list)
        ids = {r['Id'] for r in s}
        self.assertEqual(ids, {'B001-01-0001'})


class CompareFloorsTests(ComparisonTests):

    def test_compare_floors_all_match(self):
        """
        Comparison compare_floors operation with default fixtures matches all
        floors

        """
        self.assertIsNone(state.get('booker.floors'))
        self.assertIsNone(state.get('termtime.floors'))

        comparison.op_compare_floors()

        # empty invalid, create and update results
        for key in ['invalid', 'create', 'update']:
            s = state.get(f'comparison.floors.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results includes all TermTime related floors
        s = state.get('comparison.floors.match')
        self.assertIsInstance(s, list)
        # fixtures have 4 floors that related to TermTime floors
        self.assertEqual(len(s), 4)

    def test_compare_floors_update_desc(self):
        """
        Comparison compare_floors operation with modified fixtures matches all
        floors except the one with updated Description that needs creating.
        As Booker has not Ids for floors, matching for simply updating isn't
        possible to new a floor gets created instead.

        """
        self.assertIsNone(state.get('booker.floors'))
        self.assertIsNone(state.get('termtime.floors'))

        # Preload the Booker rooms
        booker.op_get_rooms()

        # Rename "Seminar Room B" floor from "Third Floor" to "Penthouse"
        ID = 'B201-03-0011'
        OLD_DESC = 'Third Floor'
        NEW_DESC = 'Penthouse'
        s = state.get('booker.rooms')
        self._assert_list_contains(s, 'Id', ID)
        for d in s:
            if d.get('Id') == ID:
                self.assertEqual(d['Floor'], OLD_DESC)
                d['Floor'] = NEW_DESC
        self._assert_list_contains(state.get('booker.rooms'), 'Floor', NEW_DESC)

        # Compare floors
        comparison.op_compare_floors()

        # empty invalid and update results
        for key in ['invalid', 'update']:
            s = state.get(f'comparison.floors.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 3 floors
        s = state.get('comparison.floors.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 3)
        # create results contains just the 'updated' floor
        s = state.get('comparison.floors.create')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        # TermTime create API uses 'f' for floor name
        self._assert_list_contains(s, 'f', NEW_DESC)

    def test_compare_floors_create_needed(self):
        """
        Comparison compare_floors operation with modified fixtures matches all
        floors and a new one needs creating if a new room is added on a new floor

        """
        self.assertIsNone(state.get('booker.floors'))
        self.assertIsNone(state.get('termtime.floors'))

        # Override booker rooms list with new room (Teaching Room 3) which is on
        # a new floor - Third Floor of Building One
        self.set_request_mock('GET', 'booker/api', 'rooms', 'booker-rooms-new-room.json')

        # Preload the Booker rooms
        booker.op_get_rooms()

        NEW_ID = 'B001-03-0001'
        NEW_FLOOR_NAME = 'Third Floor'
        NEW_FLOOR_BUILDING_ID = 'B001'
        NEW_FLOOR_SITE_ID = '21000001'
        s = state.get('booker.rooms')
        self._assert_list_contains(s, 'Id', NEW_ID)

        # Compare floors
        comparison.op_compare_floors()

        # empty invalid or update results
        for key in ['invalid', 'update']:
            s = state.get(f'comparison.floors.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results contains the 4 unchanged floors
        s = state.get('comparison.floors.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 4)
        # create results contains just the new floor
        s = state.get('comparison.floors.create')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        # TermTime create API uses floor name ('f'), building id ('b') and
        # site id ('c')
        self._assert_list_contains(s, 'f', NEW_FLOOR_NAME)
        self._assert_list_contains(s, 'b', NEW_FLOOR_BUILDING_ID)
        self._assert_list_contains(s, 'c', NEW_FLOOR_SITE_ID)


class CompareRoomBookingsTests(ComparisonTests):

    def test_compare_bookings_all_match(self):
        """
        Comparison compare_room_bookings operation with default fixtures matches all
        room bookings

        """
        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('termtime.roombookings'))

        comparison.op_compare_room_bookings()

        # empty invalid, create, update and delete results
        for key in ['invalid', 'create', 'update', 'delete']:
            s = state.get(f'comparison.roombookings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results includes all TermTime related buildings
        s = state.get('comparison.roombookings.match')
        self.assertIsInstance(s, list)
        # fixtures have 3 room bookings within the default 'All Year' timeframe
        self.assertEqual(len(s), 3)
        # their Ids/codes being "BKR1001", "BKR1002" and "BKR1003"
        self.assertEqual({b['code'] for b in s}, {"BKR1001", "BKR1002", "BKR1003"})

    def test_compare_bookings_update_title(self):
        """
        Comparison compare_room_bookings operation with modified fixtures matches all
        room bookings except the one with updated Title

        """
        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('termtime.roombookings'))

        # Preload the Booker room bookings
        booker.op_get_room_bookings()

        # Update Lent booking with new Title
        OPTIMEINDEX = 1002
        OLD_TITLE = 'Booking made by a real person (Lent)'
        NEW_TITLE = 'Modified Title for Lent booking'
        s = state.get('booker.roombookings')
        self._assert_list_contains(s, 'OptimeIndex', OPTIMEINDEX)
        self._assert_list_contains(s, 'Title', OLD_TITLE)
        for d in s:
            if d.get('OptimeIndex') == OPTIMEINDEX:
                self.assertEqual(d['Title'], OLD_TITLE)
                d['Title'] = NEW_TITLE
        self._assert_list_contains(state.get('booker.roombookings'), 'Title', NEW_TITLE)

        # Compare bookings
        comparison.op_compare_room_bookings()

        # empty invalid, create and delete results
        for key in ['invalid', 'create', 'delete']:
            s = state.get(f'comparison.roombookings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 2 room bookings
        s = state.get('comparison.roombookings.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # update results contains just the updated booking
        s = state.get('comparison.roombookings.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0]['code'], f"BKR{OPTIMEINDEX}")
        self.assertEqual(s[0]['des'], NEW_TITLE)

    def test_compare_bookings_update_room(self):
        """
        Comparison compare_room_bookings operation with modified fixtures matches all
        room bookings except the one with updated room

        """
        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('termtime.roombookings'))

        # Preload the Booker room bookings
        booker.op_get_room_bookings()

        # Update booking in B001-01-0001 with new room
        OPTIMEINDEX = 1001
        OLD_ROOM = 'B001-01-0001'
        NEW_ROOM = 'B201-03-0011'
        s = state.get('booker.roombookings')
        self._assert_list_contains(s, 'OptimeIndex', OPTIMEINDEX)
        self._assert_list_contains(s, 'Room', {'Id': OLD_ROOM})
        self._assert_list_doesnt_contain(s, 'Room', {'Id': NEW_ROOM})
        for d in s:
            if d.get('OptimeIndex') == OPTIMEINDEX:
                self.assertEqual(d['Room']['Id'], OLD_ROOM)
                d['Room']['Id'] = NEW_ROOM
        self._assert_list_contains(state.get('booker.roombookings'), 'Room', {'Id': NEW_ROOM})

        # Compare bookings
        comparison.op_compare_room_bookings()

        # empty invalid, create and delete results
        for key in ['invalid', 'create', 'delete']:
            s = state.get(f'comparison.roombookings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 2 room bookings
        s = state.get('comparison.roombookings.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # update results contains just the updated booking
        s = state.get('comparison.roombookings.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0]['code'], f"BKR{OPTIMEINDEX}")
        self.assertEqual(s[0]['rooms'], NEW_ROOM)

    def test_compare_bookings_update_times(self):
        """
        Comparison compare_room_bookings operation with modified fixtures matches all
        room bookings except the one with updated times

        """
        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('termtime.roombookings'))

        # Preload the Booker room bookings
        booker.op_get_room_bookings()

        # Update booking start and end times
        OPTIMEINDEX = 1003
        OLD_START = '2021-05-20T13:00:00Z'
        OLD_END = '2021-05-20T14:00:00Z'
        NEW_START = '2021-05-20T15:00:00Z'
        NEW_END = '2021-05-20T16:30:00Z'
        s = state.get('booker.roombookings')
        self._assert_list_contains(s, 'OptimeIndex', OPTIMEINDEX)
        for d in s:
            if d.get('OptimeIndex') == OPTIMEINDEX:
                self.assertEqual(d['StartTime'], OLD_START)
                self.assertEqual(d['EndTime'], OLD_END)
                d['StartTime'] = NEW_START
                d['EndTime'] = NEW_END
        self._assert_list_contains(state.get('booker.roombookings'), 'StartTime', NEW_START)
        self._assert_list_contains(state.get('booker.roombookings'), 'EndTime', NEW_END)

        # Compare bookings
        comparison.op_compare_room_bookings()

        # empty invalid, create and delete results
        for key in ['invalid', 'create', 'delete']:
            s = state.get(f'comparison.roombookings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 2 room bookings
        s = state.get('comparison.roombookings.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # update results contains just the updated booking
        s = state.get('comparison.roombookings.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0]['code'], f"BKR{OPTIMEINDEX}")
        # Expected new time and duration
        self.assertEqual(s[0]['time'], '16:00')  # includes conversion to BST
        self.assertEqual(s[0]['du'], '01:30')

    def test_compare_bookings_update_dates(self):
        """
        Comparison compare_room_bookings operation with modified fixtures matches all
        room bookings except the one with updated dates

        """
        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('termtime.roombookings'))

        # Preload the Booker room bookings
        booker.op_get_room_bookings()

        # Update booking start and end dates
        # (postponed from 2020-W49 day 4 to 2021-W02 day 3)
        OPTIMEINDEX = 1001
        OLD_START = '2020-12-03T10:15:00Z'
        OLD_END = '2020-12-03T12:30:00Z'
        NEW_START = '2021-01-13T10:15:00Z'
        NEW_END = '2021-01-13T12:30:00Z'
        s = state.get('booker.roombookings')
        self._assert_list_contains(s, 'OptimeIndex', OPTIMEINDEX)
        for d in s:
            if d.get('OptimeIndex') == OPTIMEINDEX:
                self.assertEqual(d['StartTime'], OLD_START)
                self.assertEqual(d['EndTime'], OLD_END)
                d['StartTime'] = NEW_START
                d['EndTime'] = NEW_END
        self._assert_list_contains(state.get('booker.roombookings'), 'StartTime', NEW_START)
        self._assert_list_contains(state.get('booker.roombookings'), 'EndTime', NEW_END)

        # Compare bookings
        comparison.op_compare_room_bookings()

        # empty invalid, create and delete results
        for key in ['invalid', 'create', 'delete']:
            s = state.get(f'comparison.roombookings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 2 room bookings
        s = state.get('comparison.roombookings.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # update results contains just the updated booking
        s = state.get('comparison.roombookings.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0]['code'], f"BKR{OPTIMEINDEX}")
        # Expected new ISO weeks pattern and day
        self.assertEqual(s[0]['pat'], '2021-W02')
        self.assertEqual(s[0]['day'], 3)

    def test_compare_bookings_create_needed(self):
        """
        Comparison compare_room_bookings operation with modified fixtures matches all
        room bookings and a new one needs creating

        """
        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('termtime.roombookings'))

        # Preload the Booker room bookings
        booker.op_get_room_bookings()

        # Add a room booking
        NEW_OPTIMEINDEX = 999
        NEW_ROOM_ID = 'B001-01-0001'
        NEW_TITLE = 'Newly created booking'
        NEW_START = '2021-01-03T14:00:00Z'
        NEW_END = '2021-01-03T17:00:00Z'
        s = state.get('booker.roombookings')
        s.append({
            "OptimeIndex": NEW_OPTIMEINDEX,
            "BatchId": -1,
            "BookedBy": {
                "Id": "real-user"
            },
            "StartTime": NEW_START,
            "EndTime": NEW_END,
            "Room": {
                "Id": NEW_ROOM_ID
            },
            "Status": "Approved",
            "Title": NEW_TITLE
        })

        self._assert_list_contains(
            state.get('booker.roombookings'), 'OptimeIndex', NEW_OPTIMEINDEX)

        # Compare bookings
        comparison.op_compare_room_bookings()

        # empty invalid, update and delete results
        for key in ['invalid', 'update', 'delete']:
            s = state.get(f'comparison.roombookings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results contains the 3 unchanged room bookings
        s = state.get('comparison.roombookings.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 3)
        # create results contains just the new room booking
        s = state.get('comparison.roombookings.create')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        b = s[0]
        self.assertEqual(b['code'], f"BKR{NEW_OPTIMEINDEX}")
        self.assertEqual(b['des'], NEW_TITLE)
        self.assertEqual(b['rooms'], NEW_ROOM_ID)
        self.assertEqual(b['time'], '14:00')
        self.assertEqual(b['du'], '03:00')
        self.assertEqual(b['pat'], '2020-W53')
        self.assertEqual(b['day'], 7)

    def test_compare_bookings_deletion_needed(self):
        """
        Comparison compare_room_bookings operation with modified fixtures matches all
        room bookings except the one needs deleting

        """
        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('termtime.roombookings'))

        # Preload the Booker room bookings
        booker.op_get_room_bookings()

        # Delete a room booking
        OPTIMEINDEX = 1003
        s = state.get('booker.roombookings')
        self._assert_list_contains(s, 'OptimeIndex', OPTIMEINDEX)
        s = [b for b in s if b.get('OptimeIndex') != OPTIMEINDEX]
        state.set('booker.roombookings', s)

        self._assert_list_doesnt_contain(
            state.get('booker.roombookings'), 'OptimeIndex', OPTIMEINDEX)

        # Compare bookings
        comparison.op_compare_room_bookings()

        # empty invalid, update and create results
        for key in ['invalid', 'update', 'create']:
            s = state.get(f'comparison.roombookings.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results contains only the 2 remaining room bookings
        s = state.get('comparison.roombookings.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # delete results contains just the one room booking to be deleted
        s = state.get('comparison.roombookings.delete')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0]['code'], f"BKR{OPTIMEINDEX}")


class CompareEventsTests(ComparisonTests):

    def test_compare_events_all_match(self):
        """
        Comparison compare_events operation with default fixtures matches all
        events

        """
        self.assertIsNone(state.get('booker.events'))
        self.assertIsNone(state.get('termtime.events'))

        comparison.op_compare_events()

        # empty invalid, create, update and delete results
        for key in ['invalid', 'create', 'update', 'delete']:
            s = state.get(f'comparison.events.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results includes all TermTime related buildings
        s = state.get('comparison.events.match')
        self.assertIsInstance(s, list)
        # fixtures have 3 events within the default 'All Year' timeframe
        self.assertEqual(len(s), 3)
        # matching these activity ids and instances
        self.assertEqual(
            {e['code'] for e in s},
            {"Activity-10000:1", "Activity-10000:2", "Activity-20000:1"}
        )

    def test_compare_events_update_title(self):
        """
        Comparison compare_events operation with modified fixtures matches all
        events except the one with updated Title

        """
        self.assertIsNone(state.get('booker.events'))
        self.assertIsNone(state.get('termtime.events'))

        # Preload the TermTime events
        termtime.op_get_events()

        # Update Lent event with new TermTime name (Title in Booker)
        OLD_TITLE = 'Booking made by essm-sync (Lent)'
        NEW_TITLE = 'Modified Title for Lent event'
        s = state.get('termtime.events')
        self._assert_list_contains(s, 'name', OLD_TITLE)
        for d in s:
            if d.get('name') == OLD_TITLE:
                d['name'] = NEW_TITLE
        self._assert_list_contains(state.get('termtime.events'), 'name', NEW_TITLE)

        # Compare events
        comparison.op_compare_events()

        # empty invalid, create and delete results
        for key in ['invalid', 'create', 'delete']:
            s = state.get(f'comparison.events.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 2 events
        s = state.get('comparison.events.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # update results contains just the updated event
        s = state.get('comparison.events.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0]['Title'], NEW_TITLE)

    def test_compare_events_update_room(self):
        """
        Comparison compare_events operation with modified fixtures matches all
        events except the one with updated room

        """
        self.assertIsNone(state.get('booker.events'))
        self.assertIsNone(state.get('termtime.events'))

        # Preload the TermTime events
        termtime.op_get_events()

        # Update event Activity-20000:1 with new room
        ID = 'Activity-20000'
        INSTANCE = 1
        OLD_ROOM = 'B002-00-0001'  # Seminar Room A of Building Two
        NEW_ROOM = 'B001-02-0001'  # Teaching Room 2 of Building One
        NEW_ROOM_OI = 2
        NEW_BUILDING_OI = 1
        s = state.get('termtime.events')
        changed_count = 0
        for d in s:
            if d.get('id') == ID and d.get('instance') == INSTANCE:
                self.assertEqual(d.get('roomcode'), OLD_ROOM)
                d['roomcode'] = NEW_ROOM
                changed_count += 1
        self.assertEqual(changed_count, 1)

        # Compare events
        comparison.op_compare_events()

        # empty invalid, create and delete results
        for key in ['invalid', 'create', 'delete']:
            s = state.get(f'comparison.events.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 2 events
        s = state.get('comparison.events.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # update results contains just the updated event
        s = state.get('comparison.events.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0]['RoomId'], NEW_ROOM_OI)
        self.assertEqual(s[0]['BuildingId'], NEW_BUILDING_OI)
        # Descriptions aren't changed, as long as they contain the activity id and instance
        self.assertEqual(s[0]['Description'], f'Description containing [{ID}:{INSTANCE}] in it')

    def test_compare_events_update_booked_by(self):
        """
        Comparison compare_events operation with modified fixtures matches all
        events except the one with updated room

        """
        self.assertIsNone(state.get('booker.events'))
        self.assertIsNone(state.get('termtime.events'))
        self.assertIsNone(state.get('termtime.activities'))

        # Preload the TermTime activities
        termtime.op_get_activities_by_code()

        # Update activity Activity-20000:1 with new crsid
        ID = 'Activity-20000'
        CODE = ID.split('-')[1]  # Just the number piece
        OLD_CRSID = 'crsid2'
        NEW_CRSID = 'crsid1'
        NEW_USEROI = 3   # Booker Staff OptimeIndex of 'crsid1' member

        s = state.get('termtime.activities_by_code')
        self.assertIsNotNone(s.get(CODE))
        self.assertEqual(s[CODE]['user5'], OLD_CRSID)
        s[CODE]['user5'] = NEW_CRSID

        # Compare events
        comparison.op_compare_events()

        # empty invalid, create and delete results
        for key in ['invalid', 'create', 'delete']:
            s = state.get(f'comparison.events.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results now only 2 events
        s = state.get('comparison.events.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # update results contains the updated event for Activity-20000
        s = state.get('comparison.events.update')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0]['code'], f'{ID}:1')
        self.assertEqual(s[0]['BookedBy'], {
            'Id': NEW_CRSID,
            'UserType': 'Staff',
            'UserOptimeIndex': NEW_USEROI,
        })

    def test_compare_events_create_needed(self):
        """
        Comparison compare_events operation with modified fixtures matches all
        events and a new one needs creating

        """
        self.assertIsNone(state.get('booker.events'))
        self.assertIsNone(state.get('termtime.events'))

        # Preload the TermTime events
        termtime.op_get_events()

        NEW_TERMTIME_EVENT = {
            'roomcode': 'B201-03-0011',
            'roomname': 'Seminar Room B',
            'buildingname': 'Secondary Building',
            'id': 'Activity-99999',
            'name': 'Newly Created Event',
            'start': '2021-04-05 10:00:00',
            'end': '2021-04-05 11:30:00',
            'type': 'Lecture',
            'instance': 1,
            'size': 10,
        }
        EXPECTED_BOOKER_EVENT = {
            'Status': 'Approved',
            'BatchId': -1,
            'BatchPosition': -1,
            'BatchSize': -1,
            'Services_Layout': -1,
            'Title': 'Newly Created Event',
            'RoomId': 4,      # OptimeIndex for Seminar Room B
            'Room': {
                'Id': 'B201-03-0011',
            },
            'BuildingId': 3,  # OptimeIndex for Secondary Building
            'StartTime': '2021-04-05T09:00:00Z',  # Hour shift for DST to UTC
            'EndTime': '2021-04-05T10:30:00Z',
            'Type': 'Teaching',
            'ExpectedAttendees': 10,
            'Description': 'Booked by TermTime [Activity-99999:1]',
            'code': 'Activity-99999:1',
        }
        # Add an event
        state.get('termtime.events').append(NEW_TERMTIME_EVENT)

        # Compare events
        comparison.op_compare_events()

        # empty invalid, update and delete results
        for key in ['invalid', 'update', 'delete']:
            s = state.get(f'comparison.events.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results contains the 3 unchanged events
        s = state.get('comparison.events.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 3)
        # create results contains just the new event
        s = state.get('comparison.events.create')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0], EXPECTED_BOOKER_EVENT)

    def test_compare_events_deletion_needed(self):
        """
        Comparison compare_events operation with modified fixtures matches all
        events except the one needs deleting

        """
        self.assertIsNone(state.get('booker.events'))
        self.assertIsNone(state.get('termtime.events'))

        # Preload the TermTime events
        termtime.op_get_events()

        # Delete an event
        ID = 'Activity-10000'
        INSTANCE = 2
        s = state.get('termtime.events')
        initial_number_of_events = len(s)
        s = [e for e in s if e.get('id') != ID or e.get('instance') != INSTANCE]
        state.set('termtime.events', s)

        # Only one event removed
        self.assertEqual(
            initial_number_of_events - 1,
            len(state.get('termtime.events'))
        )

        # Compare events
        comparison.op_compare_events()

        # empty invalid, update and create results
        for key in ['invalid', 'update', 'create']:
            s = state.get(f'comparison.events.{key}')
            self.assertIsInstance(s, list)
            self.assertEqual(len(s), 0)
        # match results contains only the 2 remaining events
        s = state.get('comparison.events.match')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 2)
        # delete results contains just the one event to be deleted
        s = state.get('comparison.events.delete')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 1)
        self.assertEqual(s[0]['Description'], f'Booked by TermTime [{ID}:{INSTANCE}]')

    def test_compare_events_invalid_data(self):
        """
        Missing critical fields from entries being compared adds them to invalid
        result

        """
        # Preload Booker and TermTime events
        booker.op_get_events()
        booker_data = state.get('booker.events')
        termtime.op_get_events()
        termtime_data = state.get('termtime.events')

        idx = 0
        for removed_key in ['id', 'instance', 'name', 'roomcode']:
            # Remove field from a single entry in valid data
            bad_data = [
                evt if i != idx else {k: v for k, v in evt.items() if k != removed_key}
                for i, evt in enumerate(termtime_data)
            ]
            self.assertIsNone(bad_data[idx].get(removed_key))
            # Assert comparison using bad_data results in the expected single invalid entry
            self._assert_comparison_invalid(
                comparison.op_compare_events, key='events',
                termtime_data=bad_data, booker_data=booker_data,
                expected=bad_data[idx]
            )
            idx = (idx + 1) % len(termtime_data)

        # A TermTime event with an Activity Id not in the format 'Activity-{number}` gets
        # added to the invalid result of comparison
        for bad_id in ['BadId-10000', 'BadId10000', 'BadId-100-100']:
            # Set last event to have each bad id
            bad_data = (
                termtime_data[:-1] +
                [{
                    k: v if k != 'id' else bad_id
                    for k, v in termtime_data[-1].items()
                }]
            )
            # Assert comparison using bad_data results in the expected single invalid entry
            self._assert_comparison_invalid(
                comparison.op_compare_events, key='events',
                termtime_data=bad_data, booker_data=booker_data,
                expected=bad_data[-1]
            )

        # Set Description of first booker event to match the second
        ID = 'Activity-10000'
        INSTANCE = 2
        dup_desc = booker_data[1]['Description']
        self.assertEqual(dup_desc, f'Booked by TermTime [{ID}:{INSTANCE}]')
        bad_data = (
            [{
                k: v if k != 'Description' else dup_desc
                for k, v in booker_data[0].items()
            }]
            + booker_data[1:]
        )

        # A TermTime event that matches more than one booker event gets added to
        # the invalid result of comparison
        self._assert_comparison_invalid(
            comparison.op_compare_events, key='events',
            termtime_data=termtime_data, booker_data=bad_data,
            expected={'id': ID, 'instance': INSTANCE}
        )
        # Matched events don't end up in the deleted result
        self.assertEqual(len(state.get('comparison.events.delete')), 0)

        # Set last TermTime event to be in a room that doesn't exist in Booker
        bad_data = (
            termtime_data[:-1] +
            [{
                k: v if k != 'roomcode' else 'badroomcode'
                for k, v in termtime_data[-1].items()
            }]
        )
        state.set('termtime.events', bad_data)
        state.set('booker.events', booker_data)

        # Make sure comparison result is clear
        state.set('comparison.events', None)
        with self.assertRaisesRegex(comparison.ComparisonError,
                                    "TermTime event's room missing in Booker"):
            comparison.op_compare_events()

    def test_compare_events_invalid_activities(self):
        """
        Missing activities or activities without crsids or crsids unknown to Booker
        cause events BookedBy to not be compared.

        """
        # Preload TermTime activities
        acts_by_code = state.get('termtime.activities_by_code', termtime.op_get_activities_by_code)

        # Initially, TermTime 'Activity-20000' is requested by 'crsid2' and event
        # 'Activity-20000:1' matches Booker booking with OptimeIndex 1013.
        ID = 'Activity-20000'
        ACT_NUM = ID.split('-')[1]
        # Confirm comparison causes no update
        comparison.op_compare_events()
        updates = state.get('comparison.events.update')
        self.assertEqual(len(updates), 0)

        ###
        # Change crsid for Activity-20000 to one unknown to Booker
        NEW_CRSID = 'new1'
        acts_by_code[ACT_NUM]['user5'] = NEW_CRSID

        # Compare events causes warning for missing staff member
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_events()
        self.assertIn(
            f"WARNING:essmsync.comparison:{ID} crsid '{NEW_CRSID}' not found in Booker staff",
            log.output
        )
        # Also doesn't try to update bookings with failed to match crsid
        updates = state.get('comparison.events.update')
        self.assertEqual(len(updates), 0)

        ###
        # Clear crsid for Activity-20000
        acts_by_code[ACT_NUM]['user5'] = None

        # Compare events causes warning for missing crsid
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_events()
        self.assertIn(
            f"WARNING:essmsync.comparison:{ID} missing crsid field (user5)",
            log.output
        )
        # Also doesn't try to update bookings with unknown crsid
        updates = state.get('comparison.events.update')
        self.assertEqual(len(updates), 0)

        ###
        # Completely remove user5 key from Activity-20000
        del acts_by_code[ACT_NUM]['user5']

        # Compare events causes same warning for missing crsid
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_events()
        self.assertIn(
            f"WARNING:essmsync.comparison:{ID} missing crsid field (user5)",
            log.output
        )
        # Also doesn't try to update bookings with unknown crsid
        updates = state.get('comparison.events.update')
        self.assertEqual(len(updates), 0)

        ###
        # Completely Activity-20000 from list of activities
        del acts_by_code[ACT_NUM]

        # Compare events causes same warning for missing activity in list
        with self.assertLogs(comparison.LOG, "WARN") as log:
            comparison.op_compare_events()
        self.assertIn(
            f"WARNING:essmsync.comparison:{ID} missing from TermTime Activities list",
            log.output
        )
        # Also doesn't try to update bookings with unknown crsid
        updates = state.get('comparison.events.update')
        self.assertEqual(len(updates), 0)
