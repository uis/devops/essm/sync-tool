import json
import unittest
import os
import re
import urllib
import requests_mock
from .. import state


class TestCase(unittest.TestCase):
    requests_to_mock = [
        ('GET', 'booker/api', 'equipment', 'booker-equipment.json'),
        ('GET', 'booker/api', 'departments', 'booker-depts.json'),
        ('GET', 'booker/api', 'sites', 'booker-sites.json'),
        ('GET', 'booker/api', 'buildings', 'booker-buildings.json'),
        ('GET', 'booker/api', 'rooms', 'booker-rooms.json'),
        ('GET', 'booker/api', 'roomtypes', 'booker-roomtypes.json'),
        ('GET', 'booker/api', 'staff', 'booker-staff.json'),

        ('GET', 'termtime', 'ping', 'termtime-ping.json'),
        ('GET', 'termtime', 'timeframes', 'termtime-timeframes.json'),
        ('GET', 'termtime', 'departments', 'termtime-depts.json'),
        ('GET', 'termtime', 'campuses', 'termtime-campuses.json'),
        ('GET', 'termtime', 'buildings', 'termtime-buildings.json'),
        ('GET', 'termtime', 'floors', 'termtime-floors.json'),
        ('GET', 'termtime', 'rooms', 'termtime-rooms.json'),
        ('GET', 'termtime', 'capabilities', 'termtime-capabilities.json'),
        ('GET', 'termtime', 'roombookings', 'termtime-roombookings.json'),
        ('GET', 'termtime', 'schedule/rooms?start=2020-10-05&days=363',
            'termtime-events-all-year.json'),
        ('GET', 'termtime', 'schedule/rooms?start=2021-04-26&days=55',
            'termtime-events-easter.json'),
        ('GET', 'termtime', 'activities', 'termtime-activities.json'),
    ]
    configs_to_load = [
        'config.yaml',
    ]

    def setUp(self):
        super().setUp()

        # where fixtures are all loaded from
        self.fixtures_dir = os.path.dirname(__file__) + '/fixtures'

        # setup requests mocker (prevent mocker converting query to lowercase)
        self.requests_mock = requests_mock.Mocker(case_sensitive=True)

        # Add mock requests
        self.set_request_mocks()
        self.requests_mock.start()

        # Make sure state is clear
        state.state = {}

        # Load mock configuration
        state.load_configs([f'{self.fixtures_dir}/{c}' for c in self.configs_to_load])

        # Default 'now' to be before first day of All Year timeframe
        state.set('now', '2020-10-05T09:00:00Z')

    def tearDown(self):
        super().tearDown()
        self.requests_mock.stop()

    def set_request_mocks(self):
        for method, service, path, file in self.requests_to_mock:
            self.set_request_mock(method, service, path, file)

        # Catch request when POSTing to bookings API
        self.bookings_post = None

        def bookings_callback(request, context):
            self.assertEqual(
                request.headers.get('Content-Type'),
                'application/x-www-form-urlencoded')
            # urllib.parse.parse_qs puts single valued params in a list, so flatten all but roomIds
            self.bookings_post = {
                k: v if k == 'roomIds' or len(v) > 1 else v[0]
                for k, v in urllib.parse.parse_qs(request.text).items()
            }
            self.assertTrue(set(self.bookings_post.keys()).issuperset(['start', 'end', 'roomIds']))
            start = self.bookings_post.get('start')
            end = self.bookings_post.get('end')
            file = f'{self.fixtures_dir}/booker-bookings-{start}-{end}.json'
            with open(file) as f:
                text = f.read()
            # As getting bookings from Booker has been hacked to get limited room(s) with each
            # call, we need to filter the response to only include those rooms (rather than
            # recreating all the booking fixtures). So convert from json, filter then back to json
            room_ois = self.bookings_post.get('roomIds')
            self.assertIsInstance(room_ois, list)
            response = json.loads(text)
            # response is a list of dicts with a single 'booking' key.
            self.assertIsInstance(response, list)
            bookings = [
                r for r in response
                if str(r['booking']['RoomId']) in room_ois
            ]
            return json.dumps(bookings)

        self.set_request_mock_func(
            'POST', 'booker/api', 'booker/calendars/events', bookings_callback)

    def set_request_mock(self, method, service, path, file):
        with open(f'{self.fixtures_dir}/{file}') as f:
            text = f.read()
        self.requests_mock.register_uri(method, f'http://{service}/{path}', text=text)

    def set_request_mock_func(self, method, service, path, func, is_regex=False):
        if is_regex:
            match = re.compile(f'http://{service}/{path}')
        else:
            match = f'http://{service}/{path}'
        self.requests_mock.register_uri(method, match, text=func)
