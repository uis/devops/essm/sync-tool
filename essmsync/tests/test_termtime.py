# Test termtime operations
from datetime import datetime, timezone
import json
import urllib

from . import TestCase
from .. import state
from .. import termtime
from .. import booker
from .. import exceptions
from .. import utils


class TermTimeGetTests(TestCase):

    def _assert_non_empty_list(self, key, func):
        """
        Asserts that func operation loads mock non-empty list into termtime.{key},
        or throws an exception if not a list

        """
        full_key = f'termtime.{key}'
        state.set(full_key, None)
        self.assertIsNone(state.get(full_key))
        func()
        s = state.get(full_key)
        self.assertIsInstance(s, list)
        self.assertNotEqual(len(s), 0)

        # Return successfully retrieved state for further tests
        return s

    def _assert_exception_non_list(self, key, func, url=None):
        """
        Asserts that func operation raises an exception if response is not a list

        """
        if url is None:
            url = key
        full_key = f'termtime.{key}'
        self.assertIsNone(state.get(full_key))
        # Override mock with dict instead of list
        self.set_request_mock('GET', 'termtime', url, 'empty_dict.json')
        self.assertRaises(exceptions.TermTimeAPIError, func)

    def test_auth(self):
        """
        TermTime auth function should call ping endpoint checking user and database
        match with API key in header

        """
        # `termtime-ping.json` matches so no exception raised
        termtime.auth()
        self.assertEqual(len(self.requests_mock.request_history), 1)
        # Call used API key in header
        self.assertEqual(
            self.requests_mock.request_history[0].headers.get('Authorization'),
            'APIKEY'
        )

        # Ping response with 'ping' of 1 raises exception
        self.set_request_mock('GET', 'termtime', 'ping', 'empty_dict.json')
        with self.assertRaises(exceptions.TermTimeAPIError):
            termtime.auth()

        # Ping response with wrong user raises exception
        self.set_request_mock('GET', 'termtime', 'ping', 'termtime-ping-bad-user.json')
        with self.assertRaises(exceptions.TermTimeAPIError):
            termtime.auth()

        # Ping response with wrong database raises exception
        self.set_request_mock('GET', 'termtime', 'ping', 'termtime-ping-bad-db.json')
        with self.assertRaises(exceptions.TermTimeAPIError):
            termtime.auth()

    def test_get_timeframes(self):
        """
        TermTime get_timeframes operation loads timeframes list with a single
        member with c == 'All Year'

        """
        s = self._assert_non_empty_list('timeframes', termtime.op_get_timeframes)
        # Fixture has 4 timeframes but only 1 should be All Year
        self.assertEqual(len(s), 4)
        self.assertEqual(len([tf for tf in s if tf.get('c') == 'All Year']), 1)

    def test_get_timeframes_non_list(self):
        """
        TermTime get_timeframes operation raises an exception if not a list

        """
        self._assert_exception_non_list('timeframes', termtime.op_get_timeframes)

    def test_get_depts(self):
        """
        TermTime get_depts operation loads non-empty department list

        """
        s = self._assert_non_empty_list('depts', termtime.op_get_depts)
        # Fixture has 3 departments
        self.assertEqual(len(s), 3)

    def test_get_depts_non_list(self):
        """
        TermTime get_depts operation raises an exception if not a list

        """
        self._assert_exception_non_list('depts', termtime.op_get_depts, 'departments')

    def test_get_campsuses(self):
        """
        termtime get_campuses operation loads non-empty campuses list

        """
        s = self._assert_non_empty_list('campuses', termtime.op_get_campuses)
        # Fixture has 3 campuses
        self.assertEqual(len(s), 3)

    def test_get_campsuses_non_list(self):
        """
        termtime get_campuses operation raises an exception if not a list

        """
        self._assert_exception_non_list('campuses', termtime.op_get_campuses)

    def test_get_buildings(self):
        """
        termtime get_buildings operation loads non-empty buildings list

        """
        s = self._assert_non_empty_list('buildings', termtime.op_get_buildings)
        # Fixture has 4 buildings
        self.assertEqual(len(s), 4)

    def test_get_buildings_non_list(self):
        """
        termtime get_buildings operation raises an exception if not a list

        """
        self._assert_exception_non_list('buildings', termtime.op_get_buildings)

    def test_get_rooms(self):
        """
        termtime get_rooms operation loads non-empty rooms list and capabilities
        gets sorted

        """
        s = self._assert_non_empty_list('rooms', termtime.op_get_rooms)
        # Fixture has 6 rooms
        self.assertEqual(len(s), 6)

        # Capabilities are sorted
        for r in s:
            caps = r['capabilities'].split(',')
            expected = ','.join(sorted(caps))
            self.assertEqual(r['capabilities'], expected)

    def test_get_rooms_non_list(self):
        """
        termtime get_rooms operation raises an exception if not a list

        """
        self._assert_exception_non_list('rooms', termtime.op_get_rooms)

    def test_get_floors(self):
        """
        termtime get_floors operation loads non-empty floors list

        """
        s = self._assert_non_empty_list('floors', termtime.op_get_floors)
        # Fixture has 5 floors
        self.assertEqual(len(s), 5)

    def test_get_floors_non_list(self):
        """
        termtime get_floors operation raises an exception if not a list

        """
        self._assert_exception_non_list('floors', termtime.op_get_floors)

    def test_get_all_room_bookings(self):
        """
        termtime get_all_room_bookings operation loads non-empty room bookings list

        """
        bookings = self._assert_non_empty_list(
            'all_roombookings', termtime.op_get_all_room_bookings)
        # Fixture has 6 room bookings
        self.assertEqual(len(bookings), 6)
        # Check that missing leading zero in weeks pattern is fixed
        for b in bookings:
            self.assertRegex(b.get('weeks'), '^[0-9]{4}-W[0-9]{2}$')

    def test_get_all_room_bookings_non_list(self):
        """
        termtime get_all_room_bookings operation raises an exception if not a list

        """
        self._assert_exception_non_list(
            'all_roombookings', termtime.op_get_all_room_bookings, 'roombookings')

    def test_get_room_bookings_default_timeframe(self):
        """
        termtime get_room_bookings operation loads non-empty room bookings list filtered
        to only those within the 'All Year' timeframe by default

        """
        bookings = self._assert_non_empty_list('roombookings', termtime.op_get_room_bookings)
        # Fixture has only 3 (of the 6) room bookings within the All Year period
        # and of type 'bkr'
        self.assertEqual(len(bookings), 3)
        self.assertEqual({b['code'] for b in bookings}, {'BKR1001', 'BKR1002', 'BKR1003'})

    def test_get_room_bookings_specific_timeframe(self):
        """
        termtime get_room_bookings operation loads non-empty room bookings list filtered
        to only those within the timeframe specified by `termtime.timeframe`

        """
        state.set('termtime.timeframe', 'Easter')
        bookings = self._assert_non_empty_list('roombookings', termtime.op_get_room_bookings)
        # Fixture has only 1 room booking within the Easter period (and of type 'bkr')
        self.assertEqual(len(bookings), 1)
        self.assertEqual(bookings[0]['code'], 'BKR1003')

    def test_get_room_bookings_exclude_past(self):
        """
        termtime get_room_bookings operation loads non-empty room bookings list filtered
        to only those within the 'All Year' timeframe but after 'now'

        """
        # same day but after start of Michaelmas room booking
        state.set('now', '2020-12-03T11:00:00Z')
        bookings = self._assert_non_empty_list('roombookings', termtime.op_get_room_bookings)
        # Fixture has only 2 (of the 6) room bookings within the All Year period,
        # of type 'bkr' and starting after 11:00 on 2020-12-03
        self.assertEqual(len(bookings), 2)
        self.assertEqual({b['code'] for b in bookings}, {'BKR1002', 'BKR1003'})

    def test_get_room_bookings_specific_type(self):
        """
        termtime get_room_bookings operation loads non-empty room bookings list filtered
        to only those of the type specified by `termtime.room_booking_type`

        """
        state.set('termtime.room_booking_type', 'test')
        bookings = self._assert_non_empty_list('roombookings', termtime.op_get_room_bookings)
        # Fixture has only 1 (of the 6) room bookings of type 'test' (within All Year)
        self.assertEqual(len(bookings), 1)
        self.assertEqual(bookings[0]['code'], 'BKR9000')

    def test_get_events(self):
        """
        termtime get_events operation loads non-empty events list using All Year timeframe
        by default

        """
        events = self._assert_non_empty_list('events', termtime.op_get_events)

        # Fixture (for All Year) has 3 events
        # (1 whole room ignored and 1 room booking in valid room ignored)
        self.assertEqual(len(events), 3)
        self.assertEqual(0, len([e for e in events if e.get('type') == 'Booking']))

    def test_get_events_non_list(self):
        """
        termtime get_events operation raises an exception if not a list

        """
        self._assert_exception_non_list('events', termtime.op_get_events,
                                        url='schedule/rooms?start=2020-10-05&days=363')

    def test_get_events_no_events_message(self):
        """
        termtime get_events operation results in empty events list if "No events"
        response given

        """
        # Mock No Events response
        self.set_request_mock_func(
            'GET', 'termtime', 'schedule/rooms?start=2020-10-05&days=363',
            '{"code":0,"message":"No events"}'
        )

        termtime.op_get_events()

        # Empty list of events produced
        s = state.get('termtime.events')
        self.assertIsInstance(s, list)
        self.assertEqual(len(s), 0)

    def test_get_events_specific_timeframe(self):
        """
        termtime get_events operation loads non-empty room bookings list based
        on the timeframe specified by `termtime.timeframe`

        """
        state.set('termtime.timeframe', 'Easter')
        events = self._assert_non_empty_list('events', termtime.op_get_events)
        # Fixture has only 1 event within the Easter period
        self.assertEqual(len(events), 1)
        self.assertEqual(events[0]['roomcode'], 'B002-00-0001')
        self.assertEqual(events[0]['id'], 'Activity-20000')
        self.assertEqual(events[0]['instance'], 1)

    def test_get_events_exclude_past(self):
        """
        termtime get_events operation loads non-empty events list using All Year timeframe
        but after 'now'

        """
        # Mock will get All Year feature but will still filter out past events
        self.set_request_mock(
            'GET', 'termtime', 'schedule/rooms?start=2020-12-03&days=304',
            'termtime-events-all-year.json'
        )
        # same day but after start of Michaelmas event
        state.set('now', '2020-12-03T11:00:00Z')
        events = self._assert_non_empty_list('events', termtime.op_get_events)

        # Fixture (for All Year) has 2 events after Michaelmas event
        self.assertEqual(len(events), 2)
        self.assertEqual(
            {(e['id'], e['instance']) for e in events},
            {('Activity-10000', 2), ('Activity-20000', 1)}
        )

    def test_get_capabilities(self):
        """
        termtime get_capabilities operation loads non-empty flattened
        capabilities list

        """
        s = self._assert_non_empty_list(
            'capabilities', termtime.op_get_capabilities)
        # Fixture has 4 capabilities that get flattened to list of strings
        self.assertEqual(len(s), 4)
        self.assertIsInstance(s[0], str)

    def test_get_capabilities_non_list(self):
        """
        termtime get_capabilities operation raises an exception if not a list

        """
        self._assert_exception_non_list(
            'capabilities', termtime.op_get_capabilities)

    def test_get_activities(self):
        """
        TermTime get_activities operation puts all scheduled activities in
        `termtime.activities` with filtered set of fields

        """
        self.assertIsNone(state.get('termtime.activities'))
        termtime.op_get_activities()
        s = state.get('termtime.activities')
        self.assertIsInstance(s, list)

        # Only scheduled activities kept
        codes = {act.get('code') for act in s}
        self.assertEqual(codes, {'10000', '20000', '40000', '90000'})

        # Activity dict keys are filtered
        filtered_keys = {'code', 'departmentCode', 'user5'}
        for act in s:
            self.assertEqual(set(act.keys()), filtered_keys)

    def test_get_activities_by_code(self):
        """
        TermTime get_activities_by_code operation puts a dict of scheduled
        activities keyed on `code` in `termtime.activities_by_code`

        """
        self.assertIsNone(state.get('termtime.activities_by_code'))
        termtime.op_get_activities_by_code()
        s = state.get('termtime.activities_by_code')
        self.assertIsInstance(s, dict)

        # Dict will have keys of 'code's of only scheduled activities
        self.assertEqual(set(s.keys()), {'10000', '20000', '40000', '90000'})


class TermTimeUtilsTests(TestCase):

    def test_get_timeframe_dates(self):
        """
        TermTime get_timeframe_dates function returns the start and end dates
        of the timeframe specified by `termtime.timeframe` ('All Year') by
        default. Optionally, returning the values as datetimes

        """
        # With no termtime.timeframe, the dates for All Year are returned
        self.assertIsNone(state.get('termtime.timeframe'))
        start, end = termtime.get_timeframe_dates()
        start_dt, end_dt = termtime.get_timeframe_dates(True)
        self.assertEqual(start, "2020-10-05")
        self.assertEqual(end, "2021-10-03")
        self.assertEqual(start_dt, datetime(2020, 10, 5, tzinfo=timezone.utc))
        self.assertEqual(end_dt, datetime(2021, 10, 3, tzinfo=timezone.utc))

        # Results change with a different `termtime.timeframe`
        state.set('termtime.timeframe', 'Lent')
        start, end = termtime.get_timeframe_dates()
        start_dt, end_dt = termtime.get_timeframe_dates(True)
        self.assertEqual(start, "2021-01-18")
        self.assertEqual(end, "2021-03-21")
        self.assertEqual(start_dt, datetime(2021, 1, 18, tzinfo=timezone.utc))
        self.assertEqual(end_dt, datetime(2021, 3, 21, tzinfo=timezone.utc))

        # Exception raised if `termtime.timeframe` isn't in `termtime.timeframes`
        state.set('termtime.timeframe', 'Bad')
        with self.assertRaises(exceptions.TermTimeAPIError):
            termtime.get_timeframe_dates()


class TermTimeUpdateTests(TestCase):

    def setUp(self):
        super().setUp()

        # Catch request when POSTing and PUTting to termtime API
        self.requests = {
            'POST': [],
            'PUT': [],
            'DELETE': [],
        }

        def tt_callback(request, context):
            split_path = [urllib.parse.unquote(s) for s in request.path.split('/')]
            request_info = {
                # request.qs is created using urllib.parse.parse_qs which puts single
                # valued query params in a list, so flatten those
                'query': {
                    k: v if len(v) > 1 else v[0]
                    for k, v in request.qs.items()
                },
                'path': split_path,
                # generally the resources 'code' is the last part of url path
                'code': split_path[-1],
            }
            self.requests[request.method].append(request_info)
            # simulated response (similar to TermTime API responses)
            return json.dumps({
                "code": 0,
                "message": "Something was updated or created successfully"
            })

        for r in ('room', 'department', 'campus', 'building', 'floor', 'roombooking'):
            self.set_request_mock_func('POST', 'termtime', f'{r}/.+', tt_callback, True)
            self.set_request_mock_func('PUT', 'termtime', f'{r}/.+', tt_callback, True)

        # Only room bookings get deleted
        self.set_request_mock_func('DELETE', 'termtime', 'roombooking/.+', tt_callback, True)

        # Not in read only mode but default
        state.set('read_only', False)

    def test_update_rooms_all_matched(self):
        """
        update_rooms operation with default fixtures will have nothing to create
        or update as all rooms will be matched

        """
        self.assertIsNone(state.get('comparison.rooms'))

        termtime.op_update_rooms()

        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_rooms_create(self):
        """
        update_rooms operation will send request to create a room if a new one
        is added

        """
        self.assertIsNone(state.get('comparison.rooms'))

        # Override booker rooms list with new room (Teaching Room 3)
        self.set_request_mock('GET', 'booker/api', 'rooms', 'booker-rooms-new-room.json')

        termtime.op_update_rooms()

        # Single POST request made
        self.assertEqual(len(self.requests['POST']), 1)
        self.assertEqual(len(self.requests['PUT']), 0)

        # The single POST matches new room (Teaching Room 3)
        posted = self.requests['POST'][0]
        self.assertEqual(posted['code'], 'B001-03-0001')
        self.assertEqual(posted['query'], {
            'n': 'Teaching Room 3',
            'cam': 'Main Site',
            'b': 'Building One',
            'f': 'Third Floor',
            'dc': '1001',
            'capabilities': 'Blackboard',
        })

    def test_update_rooms_update(self):
        """
        update_rooms operation will send request to update a room if one is
        modified

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('comparison.rooms'))

        # Preload the Booker rooms
        booker.op_get_rooms()

        # Update "Teaching Room 2" description
        ID = 'B001-02-0001'
        OLD_DESC = 'Teaching Room 2'
        NEW_DESC = 'Learning Area 1'
        for d in state.get('booker.rooms'):
            if d.get('Id') == ID:
                self.assertEqual(d['Description'], OLD_DESC)
                d['Description'] = NEW_DESC

        termtime.op_update_rooms()

        # Single PUT request made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 1)

        # The single PUT matches change of room (Teaching Room 2)
        put = self.requests['PUT'][0]
        self.assertEqual(put['code'], ID)
        self.assertEqual(put['query'], {
            'n': NEW_DESC,
            'cam': 'Main Site',
            'b': 'Building One',
            'f': 'Second Floor',
            'dn': '1001',
            'capabilities': 'Centrally Managed PC,Data Projector',
        })

    def test_update_rooms_read_only(self):
        """
        update_rooms operation will *not* send requests to create or update rooms
        if in read only mode

        """
        state.set('read_only', True)

        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('comparison.rooms'))

        # Cause a creation to be needed
        self.set_request_mock('GET', 'booker/api', 'rooms', 'booker-rooms-new-room.json')
        # Preload the Booker rooms
        booker.op_get_rooms()
        # Add an update too
        ID = 'B001-02-0001'
        OLD_DESC = 'Teaching Room 2'
        NEW_DESC = 'Learning Area 1'
        for d in state.get('booker.rooms'):
            if d.get('Id') == ID:
                self.assertEqual(d['Description'], OLD_DESC)
                d['Description'] = NEW_DESC

        termtime.op_update_rooms()

        # Addition and update was needed
        self.assertEqual(len(state.get('comparison.rooms.create')), 1)
        self.assertEqual(len(state.get('comparison.rooms.update')), 1)

        # No requests made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_depts_all_matched(self):
        """
        update_depts operation with default fixtures will have nothing to create
        or update as all departments will be matched (or possibly deleted)

        """
        self.assertIsNone(state.get('comparison.depts'))

        termtime.op_update_depts()

        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_depts_create(self):
        """
        update_depts operation will send request to create a department if a new
        one is added

        """
        self.assertIsNone(state.get('booker.depts'))
        self.assertIsNone(state.get('comparison.depts'))

        # Preload the Booker departments
        booker.op_get_depts()

        # Add a new department
        NEW_OPTIMEINDEX = 999
        NEW_ID = '2001'
        NEW_DESC = 'New Department'
        state.get('booker.depts').append({
            'OptimeIndex': NEW_OPTIMEINDEX,
            'Id': NEW_ID,
            'Description': NEW_DESC,
        })

        termtime.op_update_depts()

        # Single POST request made
        self.assertEqual(len(self.requests['POST']), 1)
        self.assertEqual(len(self.requests['PUT']), 0)

        # The single POST matches new department
        posted = self.requests['POST'][0]
        self.assertEqual(posted['code'], NEW_ID)
        self.assertEqual(posted['query'], {'n': NEW_DESC})

    def test_update_depts_update(self):
        """
        update_depts operation will send request to update a department if one is
        modified

        """
        self.assertIsNone(state.get('booker.depts'))
        self.assertIsNone(state.get('comparison.depts'))

        # Preload the Booker departments
        booker.op_get_depts()

        # Update "Department B" description
        ID = '1002'
        OLD_DESC = 'Department B'
        NEW_DESC = 'Department Blue'
        for d in state.get('booker.depts'):
            if d.get('Id') == ID:
                self.assertEqual(d['Description'], OLD_DESC)
                d['Description'] = NEW_DESC

        termtime.op_update_depts()

        # Single PUT request made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 1)

        # The single PUT matches change of department
        put = self.requests['PUT'][0]
        self.assertEqual(put['code'], ID)
        self.assertEqual(put['query'], {'n': NEW_DESC})

    def test_update_depts_read_only(self):
        """
        update_depts operation will *not* send requests to create or update
        departments if in read only mode

        """
        state.set('read_only', True)

        self.assertIsNone(state.get('booker.depts'))
        self.assertIsNone(state.get('comparison.depts'))

        # Preload the Booker departments
        booker.op_get_depts()

        # Add a new department
        NEW_OPTIMEINDEX = 999
        state.get('booker.depts').append({
            'OptimeIndex': NEW_OPTIMEINDEX,
            'Id': '2001',
            'Description': 'New Department',
        })

        # Add an update too
        for d in state.get('booker.depts'):
            if d.get('Id') == '1002':
                self.assertEqual(d['Description'], 'Department B')
                d['Description'] = 'Department Blue'

        termtime.op_update_depts()

        # Addition and update was needed
        self.assertEqual(len(state.get('comparison.depts.create')), 1)
        self.assertEqual(len(state.get('comparison.depts.update')), 1)

        # No requests made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_campuses_all_matched(self):
        """
        update_campuses operation with default fixtures will have nothing to create
        or update as all campuses will be matched (or possibly deleted)

        """
        self.assertIsNone(state.get('comparison.sites'))

        termtime.op_update_campuses()

        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_campuses_create(self):
        """
        update_campuses operation will send request to create a campus if a new
        one is added

        """
        self.assertIsNone(state.get('booker.sites'))
        self.assertIsNone(state.get('comparison.sites'))

        # Preload the Booker sites
        booker.op_get_sites()

        # Add a new site
        NEW_OPTIMEINDEX = 999
        NEW_ID = '21001001'
        NEW_DESC = 'New Site (gets url-parsed)'
        state.get('booker.sites').append({
            'OptimeIndex': NEW_OPTIMEINDEX,
            'Id': NEW_ID,
            'Description': NEW_DESC,
        })

        termtime.op_update_campuses()

        # Single POST request made
        self.assertEqual(len(self.requests['POST']), 1)
        self.assertEqual(len(self.requests['PUT']), 0)

        # The single POST matches new campus. POSTing to campus API requires name
        # in url path and 'code' in query string
        posted = self.requests['POST'][0]
        self.assertEqual(posted['path'][-1], NEW_DESC)
        self.assertEqual(posted['query'].get('code'), NEW_ID)

    def test_update_campuses_update(self):
        """
        update_campuses operation will send request to update a campus if one is
        modified

        """
        self.assertIsNone(state.get('booker.sites'))
        self.assertIsNone(state.get('comparison.sites'))

        # Preload the Booker sites
        booker.op_get_sites()

        # Update "Second Site" description
        ID = '21000002'
        OLD_DESC = 'Second Site'
        NEW_DESC = 'Renamed Site'
        for d in state.get('booker.sites'):
            if d.get('Id') == ID:
                self.assertEqual(d['Description'], OLD_DESC)
                d['Description'] = NEW_DESC

        termtime.op_update_campuses()

        # Single PUT request made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 1)

        # The single PUT matches change of campus
        put = self.requests['PUT'][0]
        self.assertEqual(put['code'], ID)
        self.assertEqual(put['query'].get('n'), NEW_DESC)

    def test_update_campuses_read_only(self):
        """
        update_campuses operation will *not* send requests to create or update
        campuses if in read only mode

        """
        state.set('read_only', True)

        self.assertIsNone(state.get('booker.sites'))
        self.assertIsNone(state.get('comparison.sites'))

        # Preload the Booker sites
        booker.op_get_sites()

        # Add a new site
        state.get('booker.sites').append({
            'OptimeIndex': 999,
            'Id': '21001001',
            'Description': 'New Site (gets url-parsed)',
        })
        # Add an update too
        for d in state.get('booker.sites'):
            if d.get('Id') == '21000002':
                self.assertEqual(d['Description'], 'Second Site')
                d['Description'] = 'Renamed Site'

        termtime.op_update_campuses()

        # Addition and update was needed
        self.assertEqual(len(state.get('comparison.sites.create')), 1)
        self.assertEqual(len(state.get('comparison.sites.update')), 1)

        # No requests made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_buildings_all_matched(self):
        """
        update_buildings operation with default fixtures will have nothing to create
        or update as all buildings will be matched (or possibly deleted)

        """
        self.assertIsNone(state.get('comparison.buildings'))

        termtime.op_update_buildings()

        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_buildings_create(self):
        """
        update_buildings operation will send request to create a building if a new
        one is added

        """
        self.assertIsNone(state.get('booker.buildings'))
        self.assertIsNone(state.get('comparison.buildings'))

        # Preload the Booker buildings
        booker.op_get_buildings()

        # Add a new building
        NEW_OPTIMEINDEX = 999
        NEW_ID = 'B501'
        NEW_DESC = 'New Building (gets url-parsed)'
        SITE_OI = 2
        SITE_ID = '21000002'
        SITE_DESC = 'Second Site'
        state.get('booker.buildings').append({
            'OptimeIndex': NEW_OPTIMEINDEX,
            'Id': NEW_ID,
            'Description': NEW_DESC,
            'SiteId': SITE_OI,
            'Site': {
                'OptimeIndex': SITE_OI,
                'Id': SITE_ID,
                'Description': SITE_DESC,
            }
        })

        termtime.op_update_buildings()

        # Single POST request made
        self.assertEqual(len(self.requests['POST']), 1)
        self.assertEqual(len(self.requests['PUT']), 0)

        # The single POST matches new building. POSTing to building API requires
        # building name and campus code in url path, and building code in query string.
        posted = self.requests['POST'][0]
        self.assertEqual(posted['path'][-2], NEW_DESC)
        self.assertEqual(posted['path'][-1], SITE_ID)
        self.assertEqual(posted['query'].get('code'), NEW_ID)

    def test_update_buildings_update(self):
        """
        update_buildings operation will send request to update a building if one is
        modified

        """
        self.assertIsNone(state.get('booker.buildings'))
        self.assertIsNone(state.get('comparison.buildings'))

        # Preload the Booker buildings
        booker.op_get_buildings()

        # Update "Building Two" description and site
        ID = 'B002'
        OLD_DESC = 'Building Two'
        NEW_DESC = 'Relocated Building'
        OLD_SITE_OI = 1
        NEW_SITE_OI = 2
        NEW_SITE_ID = '21000002'
        NEW_SITE_DESC = 'Second Site'
        for d in state.get('booker.buildings'):
            if d.get('Id') == ID:
                self.assertEqual(d['Description'], OLD_DESC)
                d['Description'] = NEW_DESC
                self.assertEqual(d['SiteId'], OLD_SITE_OI)
                d['SiteId'] = NEW_SITE_OI
                d['Site'] = {
                    'OptimeIndex': NEW_SITE_OI,
                    'Id': NEW_SITE_ID,
                    'Description': NEW_SITE_DESC,
                }

        termtime.op_update_buildings()

        # Single PUT request made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 1)

        # The single PUT matches change of building
        put = self.requests['PUT'][0]
        self.assertEqual(put['code'], ID)
        self.assertEqual(put['query'].get('n'), NEW_DESC)
        self.assertEqual(put['query'].get('camp'), NEW_SITE_ID)

    def test_update_buildings_read_only(self):
        """
        update_buildings operation will *not* send requests to create or update
        buildings if in read only mode

        """
        state.set('read_only', True)

        self.assertIsNone(state.get('booker.buildings'))
        self.assertIsNone(state.get('comparison.buildings'))

        # Preload the Booker buildings
        booker.op_get_buildings()

        # Add a new building
        state.get('booker.buildings').append({
            'OptimeIndex': 999,
            'Id': 'B501',
            'Description': 'New Building (gets url-parsed)',
            'SiteId': 2,
            'Site': {
                'OptimeIndex': 2,
                'Id': '21000002',
                'Description': 'Second Site',
            }
        })
        # Add an update too
        for d in state.get('booker.buildings'):
            if d.get('Id') == 'B201':
                self.assertEqual(d['Description'], 'Secondary Building')
                d['Description'] = 'Renamed Building'

        termtime.op_update_buildings()

        # Addition and update was needed
        self.assertEqual(len(state.get('comparison.buildings.create')), 1)
        self.assertEqual(len(state.get('comparison.buildings.update')), 1)

        # No requests made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_floors_all_matched(self):
        """
        update_floors operation with default fixtures will have nothing to create
        as all floors will be matched (or possibly deleted)

        """
        self.assertIsNone(state.get('comparison.floors'))

        termtime.op_update_floors()

        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_floors_create(self):
        """
        update_floors operation will send request to create a floor if a new
        one is needed

        """
        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('comparison.floors'))

        # Preload the Booker rooms
        booker.op_get_rooms()

        # Add a new floor by moving 'Teaching Room 2' to a new one
        ROOM_ID = 'B001-02-0001'
        OLD_FLOOR = 'Second Floor'
        NEW_FLOOR = 'Basement'
        BUILDING_ID = 'B001'
        SITE_ID = '21000001'
        for d in state.get('booker.rooms'):
            if d.get('Id') == ROOM_ID:
                self.assertEqual(d['Floor'], OLD_FLOOR)
                d['Floor'] = NEW_FLOOR

        termtime.op_update_floors()

        # Single POST request made
        self.assertEqual(len(self.requests['POST']), 1)
        self.assertEqual(len(self.requests['PUT']), 0)

        # The single POST matches new floor. POSTing to floor API requires
        # floor name, building code and campus code in url path.
        posted = self.requests['POST'][0]
        self.assertEqual(posted['path'][-3], NEW_FLOOR)
        self.assertEqual(posted['path'][-2], BUILDING_ID)
        self.assertEqual(posted['path'][-1], SITE_ID)
        self.assertIsNotNone(posted['query'].get('code'))

    def test_update_floors_read_only(self):
        """
        update_floors operation will *not* send requests to create floors if in
        read only mode

        """
        state.set('read_only', True)

        self.assertIsNone(state.get('booker.rooms'))
        self.assertIsNone(state.get('comparison.floors'))

        # Preload the Booker rooms_key
        booker.op_get_rooms()

        # Add a new floor by moving an existing room
        state.get('booker.rooms')[0]['Floor'] = 'Brand New Room'

        termtime.op_update_floors()

        # Addition was needed
        self.assertEqual(len(state.get('comparison.floors.create')), 1)

        # No requests made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)

    def test_update_room_bookings_all_matched(self):
        """
        update_room_bookings operation with default fixtures will have nothing
        to create, update or delete as all bookings will be matched

        """
        self.assertIsNone(state.get('comparison.roombookings'))

        termtime.op_update_room_bookings()

        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)
        self.assertEqual(len(self.requests['DELETE']), 0)

    def test_update_room_bookings_create(self):
        """
        update_room_bookings operation will send request to create a room
        booking if a new one is added

        """
        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('comparison.roombookings'))

        # Preload the Booker room bookings
        booker.op_get_room_bookings()

        # Add a new room bookings (no activity/instance reference)
        OPTIMEINDEX = 999
        TITLE = 'New Room Booking'
        ROOM_ID = 'B001-01-0001'
        START_TIME = "2021-01-13T09:00:00Z"
        END_TIME = "2021-01-13T11:00:00Z"
        # Dictionary of time, duration, day and ISO week used by TermTime for new booking
        TT_BOOKING_TIMINGS = utils.termtime_booking_format_datetime(
            utils.booker_parse_datetime(START_TIME),
            utils.booker_parse_datetime(END_TIME)
        )

        state.get('booker.roombookings').append({
            "OptimeIndex": OPTIMEINDEX,
            "BookedBy": {
                "Id": "some-user"
            },
            "StartTime": START_TIME,
            "EndTime": END_TIME,
            "Room": {
                "Id": ROOM_ID
            },
            "Title": TITLE
        })

        termtime.op_update_room_bookings()

        # Single POST request made
        self.assertEqual(len(self.requests['POST']), 1)
        self.assertEqual(len(self.requests['PUT']), 0)
        self.assertEqual(len(self.requests['DELETE']), 0)

        # The single POST matches new booking.
        posted = self.requests['POST'][0]
        self.assertEqual(posted['code'], f"BKR{OPTIMEINDEX}")
        self.assertEqual(posted['query'].get('des'), TITLE)
        self.assertEqual(posted['query'].get('rooms'), ROOM_ID)
        for param in TT_BOOKING_TIMINGS.keys():
            self.assertEqual(
                posted['query'].get(param), str(TT_BOOKING_TIMINGS[param])
            )

    def test_update_room_bookings_update(self):
        """
        update_room_bookings operation will send request to update a booking if
        one is modified

        """
        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('comparison.roombookings'))

        # Preload the Booker room bookings
        booker.op_get_room_bookings()

        # Update Lent booking's title/description, room and time
        OPTIMEINDEX = 1002
        OLD_TITLE = 'Booking made by a real person (Lent)'
        NEW_TITLE = 'Rescheduled booking'
        OLD_ROOM_ID = "B001-02-0001"
        NEW_ROOM_ID = "B002-00-0001"
        OLD_START_TIME = "2021-02-01T13:00:00Z"
        OLD_END_TIME = "2021-02-01T14:00:00Z"
        NEW_START_TIME = "2021-02-01T15:00:00Z"
        NEW_END_TIME = "2021-02-01T16:00:00Z"
        # Dictionary of time, duration, day and ISO week used by TermTime for updated booking
        TT_BOOKING_TIMINGS = utils.termtime_booking_format_datetime(
            utils.booker_parse_datetime(NEW_START_TIME),
            utils.booker_parse_datetime(NEW_END_TIME)
        )

        for d in state.get('booker.roombookings'):
            if d.get('OptimeIndex') == OPTIMEINDEX:
                self.assertEqual(d['Title'], OLD_TITLE)
                d['Title'] = NEW_TITLE
                self.assertEqual(d['Room']['Id'], OLD_ROOM_ID)
                d['Room']['Id'] = NEW_ROOM_ID
                self.assertEqual(d['StartTime'], OLD_START_TIME)
                self.assertEqual(d['EndTime'], OLD_END_TIME)
                d['StartTime'] = NEW_START_TIME
                d['EndTime'] = NEW_END_TIME

        termtime.op_update_room_bookings()

        # Single PUT request made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 1)
        self.assertEqual(len(self.requests['DELETE']), 0)

        # The single PUT matches change of booking
        put = self.requests['PUT'][0]
        self.assertEqual(put['code'], f"BKR{OPTIMEINDEX}")
        self.assertEqual(put['query'].get('des'), NEW_TITLE)
        self.assertEqual(put['query'].get('rooms'), NEW_ROOM_ID)
        for param in TT_BOOKING_TIMINGS.keys():
            self.assertEqual(
                put['query'].get(param), str(TT_BOOKING_TIMINGS[param])
            )

    def test_update_room_bookings_delete(self):
        """
        update_room_bookings operation will send request to delete a booking if
        one is removed

        """
        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('comparison.roombookings'))

        # Preload the Booker room bookings
        booker.op_get_room_bookings()

        # Remove second booking
        s = state.get('booker.roombookings')
        self.assertGreater(len(s), 1)
        REMOVED_BOOKING_OI = s.pop(1)['OptimeIndex']

        termtime.op_update_room_bookings()

        # Single PUT request made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)
        self.assertEqual(len(self.requests['DELETE']), 1)

        # The single DELETE matches change of booking
        self.assertEqual(self.requests['DELETE'][0]['code'], f"BKR{REMOVED_BOOKING_OI}")

    def test_update_room_bookings_read_only(self):
        """
        update_room_bookings operation will *not* send requests to create, update
        or delete bookings if in read only mode

        """
        state.set('read_only', True)

        self.assertIsNone(state.get('booker.roombookings'))
        self.assertIsNone(state.get('comparison.roombookings'))

        # Preload the Booker room bookings
        booker.op_get_room_bookings()

        # Add a new booking
        s = state.get('booker.roombookings')
        s.append({
            "OptimeIndex": 999,
            "BookedBy": {
                "Id": "some-user"
            },
            "StartTime": "2021-02-12T13:00:00Z",
            "EndTime": "2021-02-12T14:50:00Z",
            "Room": {
                "Id": "B002-00-0001"
            },
            "Title": "New Booking"
        })
        # Update one
        for d in s:
            if d.get('OptimeIndex') == 1003:
                self.assertEqual(d['Title'], 'Booking made by a real person (Easter) - BST')
                d['Title'] = 'Updated Booking Title'
        # And remove one
        s.pop(0)

        termtime.op_update_room_bookings()

        # Addition, update and removal was needed
        self.assertEqual(len(state.get('comparison.roombookings.create')), 1)
        self.assertEqual(len(state.get('comparison.roombookings.update')), 1)
        self.assertEqual(len(state.get('comparison.roombookings.delete')), 1)

        # No requests made
        self.assertEqual(len(self.requests['POST']), 0)
        self.assertEqual(len(self.requests['PUT']), 0)
        self.assertEqual(len(self.requests['DELETE']), 0)
