"""
Utilities for handling configuration files

"""
import logging
import os

import yaml
import dpath

from .exceptions import ConfigurationNotFound, ConfigurationInvalid


LOG = logging.getLogger(__name__)

# User agent for API requests
USER_AGENT = 'essmsync'

# Keys required in merged configuration
REQUIRED_CONFIG_KEYS = [
    'termtime.api.url',
    'termtime.api.key',
    'booker.api.url',
    'booker.api.email',
    'booker.api.password',
]

# dpath separator
DPATH_SEP = '.'

# Start with an empty state
state = dict()


def load_configs(config_files):
    """
    Load a list of configuration files and check required keys exist

    >>> load_configs([])
    Traceback (most recent call last):
    ...
    essmsync.exceptions.ConfigurationInvalid: Configuration is not valid
    >>> load_configs(['no_such_file.yaml'])
    Traceback (most recent call last):
    ...
    essmsync.exceptions.ConfigurationNotFound: Configuration file missing
    """
    LOG.info('Loading configurations')

    # Check for missing configuration files
    missing_files = [f for f in config_files if not os.path.isfile(f)]
    if len(missing_files) > 0:
        for f in missing_files:
            LOG.error(f'"{f}" configuration missing')
        raise ConfigurationNotFound()

    # Load each configuration file
    for f in config_files:
        load_config(f)

    # Check for missing required keys
    invalid = False
    for r in REQUIRED_CONFIG_KEYS:
        value = get(r, None)
        if value is None or str(value).strip() == "":
            LOG.error(f'{r}: required but not set')
            invalid = True
    if invalid:
        raise ConfigurationInvalid()

    # All configuration loaded
    return state


def load_config(file):
    """
    Load an individual configuration file, deep merging it with existing state
    """
    LOG.info(f'Loading "{file}"')
    with open(file) as f:
        c = yaml.safe_load(f)
        dpath.merge(state, c)


def get(dottedkey, default=None):
    """
    Get the value from a nested dict using a dotted key reference (e.g. key1.key2.key3).
    If value doesn't exist or is None then default will be return, unless default is a
    callable. In which case, default will be called (and assumed to set the key) then
    another attempt to get the key will be attempted (with no default).

    >>> set(None, {'a':{'b':'c','d':{'e':'f'}},'g':'h'})
    >>> get('g')
    'h'
    >>> get('a.b')
    'c'
    >>> get('a.d.e')
    'f'
    >>> get('x', 'missing')
    'missing'
    >>> get('a.x', 'missing')
    'missing'
    >>> get('a.d.x', 'missing')
    'missing'
    >>> get('a.x.y', 'missing')
    'missing'
    >>> get('a.x.y', lambda: set('a.x.y', 'setter_called'))
    'setter_called'
    >>> get('a.x.y')
    'setter_called'
    """
    # dpath isn't fast so for top level items just get them straight from the state dict
    if DPATH_SEP not in dottedkey:
        value = state.get(dottedkey)
    else:
        value = dpath.get(state, dottedkey, separator=DPATH_SEP, default=None)
    if value is None:
        if callable(default):
            # Call setting function then retry get but with no default
            default()
            return get(dottedkey, None)
        else:
            value = default
    return value


def set(dottedkey, value=None, overwrite=True):
    """
    Set the value in a nested dict using a dotted key reference (e.g. key1.key2.key3)
    By default will overwrite the key, otherwise lists and dicts are merged

    >>> set(None, {'a':{'b':'c','d':{'e':'f'}},'g':'h'})
    >>> set('g', 'update at root')
    >>> get('g')
    'update at root'
    >>> set('w', 'new at root')
    >>> get('w')
    'new at root'
    >>> set('a.d.e', 'update nested')
    >>> get('a.d.e')
    'update nested'
    >>> set('a.d.y', 'new nested')
    >>> get('a.d.y')
    'new nested'
    >>> get('a.d') == {'e': 'update nested', 'y': 'new nested'}
    True
    >>> set('a.d', {'z': 'overwrite'})
    >>> get('a.d')
    {'z': 'overwrite'}
    >>> set('a.d', {'m': 'merged'}, False)
    >>> get('a.d') == {'z': 'overwrite', 'm': 'merged'}
    True
    >>> set('l', ['new', 'list'])
    >>> get('l')
    ['new', 'list']
    >>> set('l', ['extended'], False)
    >>> get('l')
    ['new', 'list', 'extended']
    >>> set('l', ['overwrite'])
    >>> get('l')
    ['overwrite']
    >>> set('a.g', 'forced overwrite of strings', False)
    >>> get('a.g')
    'forced overwrite of strings'

    """
    if dottedkey is None:
        # Special case - merge directly on to state - for tests
        dpath.merge(state, value)
    elif not overwrite:
        # Append/merge with existing value if it is a list or dict
        current = get(dottedkey)
        if isinstance(current, list):
            current.extend(value)
        elif isinstance(current, dict):
            dpath.merge(current, value)
        else:
            # Just overwrite key
            set(dottedkey, value)
    else:
        # Attempt to set existing key
        if dpath.set(state, dottedkey, value, separator=DPATH_SEP) == 0:
            # doesn't exist so add new key
            dpath.new(state, dottedkey, value, separator=DPATH_SEP)
