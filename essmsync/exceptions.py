"""
All custom exceptions raised by tool

"""


class ConfigurationError(RuntimeError):
    """
    Base class for all configuration errors.

    """


class ConfigurationNotFound(ConfigurationError):
    """
    A configuration file could not be located.

    """
    def __init__(self):
        return super().__init__('Configuration file missing')


class ConfigurationInvalid(ConfigurationError):
    """
    Configuration is not valid

    """
    def __init__(self):
        return super().__init__('Configuration is not valid')


class AuthenticationError(RuntimeError):
    """
    Base class for all authentication errors.

    """


class APIError(RuntimeError):
    """
    Base class for all API errors.

    """


class TermTimeAPIError(APIError):
    """
    Base class for all TermTime API errors.

    """


class BookerAPIError(APIError):
    """
    Base class for all Booker API errors.

    """


class BookerAPIBookingConflict(BookerAPIError):
    """
    Special case for booking conflict failure

    """


class OperationInvalid(RuntimeError):
    """
    Operation specified is not valid

    """


class ComparisonError(RuntimeError):
    """
    Fatal condition during comparison

    """


class ConversionError(RuntimeError):
    """
    Fatal data conversion error

    """
