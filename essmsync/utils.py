"""
Common utility functions

"""
import requests
import logging
import json
import time
import re
import datetime
from functools import lru_cache
from dateutil import parser, tz, relativedelta

from . import state
from .exceptions import APIError, ConversionError

LOG = logging.getLogger(__name__)

# Regex that matches format of event id
ACTIVITY_MATCH = re.compile(r'Activity-(\d+)')


def api_get(url, **kwargs):
    return api_via_method(requests.get, url, **kwargs)


def api_post(url, **kwargs):
    return api_via_method(requests.post, url, **kwargs)


def api_put(url, **kwargs):
    return api_via_method(requests.put, url, **kwargs)


def api_delete(url, **kwargs):
    return api_via_method(requests.delete, url, **kwargs)


def api_via_method(method_func, url, exception=APIError, retries=2, sleep=5,
                   exception_handler=None, **kwargs):
    """
    Uses `method_func` (the requests.get/post/put function) to call the API
    endpoint.
    If an HTTP, Connection, Timeout or JSON decode error occurs then the
    `exception_handler` is called if provided. If not provided, or it doesn't
    return an APIError then the call will be retried up to `retries` times.
    If another error occurs or all retries have failed then an `exception` type
    exception is raised.

    """
    try:
        try:
            r = method_func(url, **kwargs)
            LOG.debug(f'Response [{r.status_code}]: {r.text[:500]}')
            r.raise_for_status()
            return (r.json(), r)
        # Exceptions that can be retried
        except (
                json.decoder.JSONDecodeError,
                requests.exceptions.ConnectionError,
                requests.exceptions.Timeout,
                requests.exceptions.HTTPError,
               ) as e:
            if exception_handler:
                # Use exception_handler if provided, and raise different exception if
                # it returns one. Exception handler takes url and response
                to_raise = exception_handler(url, r)
                if isinstance(to_raise, APIError):
                    raise to_raise
            if retries > 0:
                LOG.warning(f'Retrying: {e}')
                # Wait 5 seconds before retrying
                time.sleep(sleep)
                return api_via_method(
                    method_func, url, exception=exception, retries=retries-1, **kwargs
                )
            # Already retried enough times
            raise exception()
    except requests.exceptions.RequestException as e:
        LOG.error(f'Request {url} Failed: {e}')
        raise exception()


@lru_cache(maxsize=5000)
def booker_parse_datetime(s, source='Booker'):
    """
    Takes a ISO 8601 "zulu" string as returned by Booker and returns a
    datetime.datetime with UTC timezone

    """
    if re.fullmatch('[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z', s) is None:
        LOG.error(f"Invalid formatted {source} datetime '{s}'")
        raise ConversionError(f"Invalid {source} datetime")
    try:
        return parser.isoparse(s)
    except (ValueError, OverflowError) as e:
        LOG.error(f"Unable to parse {source} datetime '{s}': {e}")
        raise ConversionError(f"Unable to parse {source} datetime")


def booker_format_datetime(dt):
    """
    Takes a datetime.datetime object which must have a timezone and renders it
    as an ISO 8601 "zulu" string (i.e. 2001-02-03T04:05:06Z)

    """
    if dt.tzinfo is None:
        LOG.error(f"Booker datetime missing timezone '{dt}'")
        raise ConversionError("Booker datetime missing timezone")
    if dt.tzinfo is not tz.tzutc():
        dt = dt.astimezone(tz.tzutc())
    return dt.strftime('%Y-%m-%dT%H:%M:%S') + 'Z'


def termtime_parse_datetime(s):
    """
    Takes a datetime string as returned by TermTime for scheduled activities in
    local UK time (e.g. 2020-21-22 09:10:11) and returns a datetime.datetime with
    GB timezone.

    """
    if re.fullmatch('[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}', s) is None:
        LOG.error(f"Invalid formatted TermTime datetime '{s}'")
        raise ConversionError("Invalid TermTime datetime")
    try:
        return parser.parse(s).replace(tzinfo=tz.gettz('GB'))
    except (ValueError, OverflowError) as e:
        LOG.error(f"Unable to parse TermTime datetime '{s}': {e}")
        raise ConversionError("Unable to parse TermTime datetime")


def termtime_format_datetime(dt):
    """
    Takes a datetime.datetime object which must have a timezone and returns a
    string as used by TermTime scheduled events API, which formats the datetime
    in local UK time (yyyy-mm-dd hh:mm:ss)

    """
    if dt.tzinfo is None:
        LOG.error(f"TermTime datetime missing timezone '{dt}'")
        raise ConversionError("TermTime datetime missing timezone")
    if dt.tzinfo is not tz.gettz('GB'):
        dt = dt.astimezone(tz.gettz('GB'))
    return dt.strftime('%Y-%m-%d %H:%M:%S')


def termtime_booking_parse_datetime(d):
    """
    Takes a dict of containing time (hh:mm), du (hh:mm), day (1-7, 1=Mon),
    pat (ISO week, yyyy-Www) in local UK time, as used by TermTime room bookings
    API and returns a tuple of start and end datetime.datetime with GB timezones.

    Alternatively, the 'pat' key may be 'weeks' (but with only a single week),
    and the 'day' key may be 'days' with a single 'Mon' to 'Sun' value

    """
    # Fudge inconsistency with TermTime's use of:
    #   'day' (ISO 8601 decimal weekday) and 'days' (CSV list of weekday abbreviations)
    #   'pat' (ISO 8601 year and week) and 'weeks' (CSV list of the same)
    # This function only supports single 'days' and 'weeks'
    if 'days' in d:
        if ',' in d['days']:
            LOG.error(f"TermTime booking has multiple days '{d['days']} - not supported")
            raise ConversionError("Unsupported multiple TermTime booking days")
        days_of_week = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        if d['days'] not in days_of_week:
            LOG.error(f"TermTime booking has invalid days '{d['days']}")
            raise ConversionError("Invalid TermTime booking days")
        d['day'] = days_of_week.index(d['days']) + 1
    if 'weeks' in d:
        if ',' in d['weeks']:
            LOG.error(f"TermTime booking has multiple weeks '{d['weeks']} - not supported")
            raise ConversionError("Unsupported multiple TermTime booking weeks")
        d['pat'] = d['weeks']
    missing_fields = [f for f in ['time', 'day', 'du', 'pat'] if f not in d]
    if len(missing_fields) > 0:
        LOG.error(f"TermTime booking missing field(s) {missing_fields}")
        raise ConversionError("TermTime booking missing field(s)")
    if re.fullmatch('[0-9]{4}\\-W[0-9]{1,2}', d['pat']) is None:
        LOG.error(f"Invalid TermTime ISO week '{d['pat']}'")
        raise ConversionError("Invalid TermTime ISO week")
    if re.fullmatch('[0-9]{2}\\:[0-9]{2}', d['time']) is None:
        LOG.error(f"Invalid TermTime time '{d['time']}'")
        raise ConversionError("Invalid TermTime time")
    if re.fullmatch('[0-9]{2}\\:[0-9]{2}', d['du']) is None:
        LOG.error(f"Invalid TermTime duration '{d['du']}'")
        raise ConversionError("Invalid TermTime duration")
    # Python 3.8 has datatime.fromisocalendar() that may be simpler
    try:
        start_dt = datetime.datetime.strptime(
            f"{d['pat']}-{d['day']} {d['time']}", '%G-W%V-%u %H:%M'
        ).replace(tzinfo=tz.gettz('GB'))
    except ValueError as e:
        LOG.error(
            "Unable to parse TermTime booking start time '"
            f"{d['pat']}-{d['day']} {d['time']}"
            f"': {e}"
        )
        raise ConversionError("Unable to parse TermTime booking start time")

    delta = datetime.timedelta(hours=int(d['du'][:2]), minutes=int(d['du'][3:]))
    end_dt = start_dt + delta

    # return tuple of start and end datetimes
    return (start_dt, end_dt)


def termtime_booking_format_datetime(start_dt, end_dt):
    """
    Takes a start and end datetime.datetime objects which must have a timezone
    and be on the same day, returns a dict containing time (hh:mm), du (hh:mm),
    day (1-7, 1=Mon), pat (ISO week, yyyy-Www) in local UK time, as used by
    TermTime room bookings API

    """
    if start_dt.tzinfo is None:
        LOG.error(f"TermTime start date missing timezone '{start_dt}'")
        raise ConversionError("TermTime start datetime missing timezone")
    if end_dt.tzinfo is None:
        LOG.error(f"TermTime end date missing timezone '{end_dt}'")
        raise ConversionError("TermTime end datetime missing timezone")
    if start_dt.date() != end_dt.date():
        LOG.error(f"TermTime booking dates not on same day '{start_dt}' to '{end_dt}'")
        raise ConversionError("TermTime dates not on same day")

    diff = relativedelta.relativedelta(end_dt, start_dt)
    (year, week, dow) = start_dt.isocalendar()

    if start_dt.tzinfo is not tz.gettz('GB'):
        start_dt = start_dt.astimezone(tz.gettz('GB'))

    r = {
        'time': start_dt.strftime('%H:%M'),
        'du': f"{diff.hours:02}:{diff.minutes:02}",
        'day': dow,
        'pat': f"{year}-W{week:02}",
    }
    return r


def localtime_hhmm(dt):
    """
    Return the local time of a datetime.datetime object as a string in the
    format hh:mm

    """
    if dt.tzinfo is None:
        LOG.error(f"Cannot get localtime as missing timezone '{dt}'")
        raise ConversionError("Localtime missing timezone")
    if dt.tzinfo is not tz.gettz('GB'):
        dt = dt.astimezone(tz.gettz('GB'))
    return dt.strftime('%H:%M')


def now():
    """
    Return datetime.now() unless overridden by `now` state provided as ISO 8601
    "zulu" string.

    """
    override = state.get('now')
    if override is not None:
        # re-use booker time parse function
        return booker_parse_datetime(override, '"now"')
    return datetime.datetime.now(tz=tz.tzutc())


def act_code_from_event_id(id):
    """
    Return the activity code (a series of digits, X) from a TermTime event id
    of the form "Activity-X".

    Returns None if the id does not match this format.

    >>> act_code_from_event_id('Activity-12345')
    '12345'
    >>> act_code_from_event_id('Nonsense') is None
    True
    >>> act_code_from_event_id('Something-12345') is None
    True
    >>> act_code_from_event_id('Activity-abcde') is None
    True

    """
    match = ACTIVITY_MATCH.fullmatch(id)
    return None if match is None else match.group(1)
