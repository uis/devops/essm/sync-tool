"""
Handle authentication and usage of TermTime API

"""
import logging
import re

from . import state
from . import utils
from . import booker
from datetime import datetime, timezone
from secrets import token_hex
from .exceptions import TermTimeAPIError
from . import comparison

LOG = logging.getLogger(__name__)


def tt_get(resource, **kwargs):
    return utils.api_get(
        state.get('termtime.api.url') + resource,
        exception=TermTimeAPIError,
        headers=tt_headers(),
        **kwargs
    )


def tt_post(resource, **kwargs):
    if state.get('read_only', True):
        return None
    return utils.api_post(
        state.get('termtime.api.url') + resource,
        exception=TermTimeAPIError,
        headers=tt_headers(),
        **kwargs
    )


def tt_put(resource, **kwargs):
    if state.get('read_only', True):
        return None
    return utils.api_put(
        state.get('termtime.api.url') + resource,
        exception=TermTimeAPIError,
        headers=tt_headers(),
        **kwargs
    )


def tt_delete(resource, **kwargs):
    if state.get('read_only', True):
        return None
    return utils.api_delete(
        state.get('termtime.api.url') + resource,
        exception=TermTimeAPIError,
        headers=tt_headers(),
        **kwargs
    )


def tt_headers(form=False):
    return {
        'user-agent': state.USER_AGENT,
        'Authorization': state.get('termtime.api.key'),
    }


def auth():
    LOG.info('Authenticating TermTime API')

    # No separate authentication step just use api key
    (ping_resp, _) = tt_get('ping')
    if ping_resp.get('ping') != 1:
        LOG.error('TermTime API authentication failed')
        raise TermTimeAPIError()

    # If user and database provided check ping response matches
    user = state.get('termtime.api.user')
    db = state.get('termtime.api.database')
    if user and db:
        if ping_resp.get('user') == user and ping_resp.get('database') == db:
            LOG.info('TermTime API user/database validated')
        else:
            LOG.error('TermTime API user/database mismatch')
            raise TermTimeAPIError()


def _op_get_list(endpoint, target=None, overwrite=True, **kwargs):
    """
    Generic function for getting a list of from and endpoint and putting it in
    `termtime.<target>` (target = endpoint if not given)

    """
    if target is None:
        target = endpoint
    (l, _) = tt_get(endpoint, **kwargs)
    if not isinstance(l, list):
        LOG.error(f'TermTime {endpoint} result is not a list')
        raise TermTimeAPIError()
    LOG.info(f'TermTime {target}: {len(l)}')
    state.set(f'termtime.{target}', l, overwrite)
    return l


# TermTime operations


def op_get_rooms():
    """
    Get list of all rooms and put in `termtime.rooms`

    `capabilities` is a comma separated list of capability strings.
    Unfortunately, each list is not sorted which hampers comparison, so
    sort them when retrieved.

    """
    for r in _op_get_list('rooms'):
        caps = r.get('capabilities')
        if isinstance(caps, str) and caps != '':
            r['capabilities'] = ','.join(sorted(caps.split(',')))
        else:
            r['capabilities'] = ''


def op_get_capabilities():
    """
    Get list of all capabilities and put in `termtime.capabilities`

    Flatten single keyed dict entries, e.g.
    [{'n':'Capability Name'}, ...] to ['Capability Name', ...]

    """
    flattened = [c['n'] for c in _op_get_list('capabilities')]
    state.set('termtime.capabilities', flattened)


def op_get_depts():
    """
    Get list of all departments (aka institutions) and put in `termtime.depts`

    """
    _op_get_list('departments', 'depts')


def op_get_campuses():
    """
    Get list of all campuses and put in `termtime.campuses`

    """
    _op_get_list('campuses')


def op_get_buildings():
    """
    Get list of all buildings and put in `termtime.buildings`

    """
    _op_get_list('buildings')


def op_get_floors():
    """
    Get list of all floors and put in `termtime.floors`

    """
    _op_get_list('floors')


def op_get_all_room_bookings():
    """
    Get list of all roombookings and put in `termtime.all_roombookings`

    """
    all = _op_get_list('roombookings', 'all_roombookings')
    # Despite what the API spec says, TermTime sometimes provides the ISO week
    # without the leading zero on week number. i.e. 2020-W3 instead of 2020-W03
    # Fix on the fly else comparisons will break
    for b in all:
        weeks = b.get('weeks')
        if weeks and re.fullmatch("[0-9]{4}-W[0-9]", weeks):
            b['weeks'] = weeks[:6] + '0' + weeks[6:]


def op_get_room_bookings():
    """
    Get list of roombookings of type `termtime.room_booking_type` and within
    `termtime.timeframe` and put in `termtime.roombookings`. Need filtering
    ourselves as API provides no parameters to filter request. Also filters
    bookings before `utils.now()`.

    """
    # get timeframe dates (as datetimes)
    (start_dt, end_dt) = get_timeframe_dates(True)
    # bring forward start_dt to 'now'
    start_dt = max(start_dt, utils.now())
    # Filter all bookings by timeframe and room_booking_type
    tt_type = state.get('termtime.room_booking_type', 'bkr')
    room_bookings = [
        b for b in state.get('termtime.all_roombookings', op_get_all_room_bookings)
        if (
            b.get('type', '') == tt_type and
            start_dt < utils.termtime_booking_parse_datetime(b)[0] < end_dt
        )
    ]
    LOG.info(f'TermTime room bookings in timeframe: {len(room_bookings)}')
    state.set('termtime.roombookings', room_bookings)


def op_get_timeframes():
    """
    Get list of all timeframes and put in `termtime.timeframes`

    """
    _op_get_list('timeframes')


def op_get_events():
    """
    Get list of events within termtime.timeframe (excluding roombookings) and put
    in `termtime.events`. Filter roombookings made before `utils.now()`

    """
    # get start date for timeframe
    (start_dt, end_dt) = get_timeframe_dates(True)
    # bring forward start_dt to 'now'
    start_dt = max(start_dt, utils.now())
    # how many days to request
    days = (end_dt.date() - start_dt.date()).days
    # start in YYYY-MM-DD format
    start_ymd = start_dt.strftime('%Y-%m-%d')
    LOG.info(f'Scheduling Period: {start_ymd} for {days} days')
    (room_activities, _) = tt_get('schedule/rooms',
                                  params={
                                     'start': start_ymd,
                                     'days': days,
                                  })
    # TermTime returns {"code":0, "message":"No events"} if it cannot find any
    # instead of an empty list
    if isinstance(room_activities, dict) and room_activities.get('message') == 'No events':
        LOG.warning('TermTime found no events in period specified')
        room_activities = []
    elif not isinstance(room_activities, list):
        LOG.error('TermTime events result is not a list')
        raise TermTimeAPIError()
    # Only interested in activities in rooms we're syncing (i.e. those already
    # filtered in booker.rooms) so get a list of ids to match against
    room_ids = {r['Id'] for r in state.get('booker.rooms', booker.op_get_rooms) if r.get('Id')}
    events = []
    ignored_rooms = {}
    # compile events from each room's activities
    for r in room_activities:
        if r.get('roomcode') in room_ids:
            # room data added to each event (only roomcode actually needed)
            room_data = {k: r[k] for k in ['roomcode', 'roomname', 'buildingname']}
            # Remove events with "Booking" activity type (these are roombookings)
            # and not in past
            acts = [
                {**a, **room_data} for a in r['activities']
                if a.get('type') != 'Booking'
                and start_dt <= utils.termtime_parse_datetime(a.get('start'))
            ]
            events += acts
        else:
            ignored_rooms[r.get('roomcode')] = r.get('roomname', 'Unknown Name')

    if ignored_rooms:
        LOG.warning("Ignoring activities in TermTime rooms:")
        for code, name in ignored_rooms.items():
            LOG.warning(f'... {code}: {name}')

    LOG.info(f'TermTime events: {len(events)}')
    state.set('termtime.events', events)


def op_get_activities():
    """
    Get list of all termtime activities and put in the scheduled ones in
    `termtime.activities`. Unfortunately, there is no filtering other than
    'ready' in the API call.
    Also response will be large so filter which fields we keep, including the
    custom user field holding CRSid of requestor (defaults to 'user5').

    """
    all_activities = _op_get_list('activities')
    # Which field is the CRSid of requestor stored (default to 'user5')
    crsid_user_field = state.get('termtime.crsid_user_field', 'user5')
    # Filter which pieces of data to keep
    filtered_keys = ['code', 'departmentCode', crsid_user_field]

    # Filter to only scheduled activities and only keep filtered keys.
    # Also make sure CRSids are sanitised (lowercase without whitespace)
    filtered_activities = [
        {
            k: v if k != crsid_user_field or v is None else v.strip().lower()
            for k, v in a.items() if k in filtered_keys
        }
        for a in all_activities if a.get('scheduled', 0) == 1
    ]
    LOG.info(f'TermTime scheduled activities: {len(filtered_activities)}')
    state.set('termtime.activities', filtered_activities)


def op_get_activities_by_code():
    """
    Get dict of scheduled activities keyed on 'code' and put in `termtime.activities_by_code`

    """
    activities = state.get('termtime.activities', op_get_activities)
    activities_by_code = {a.get('code'): a for a in activities}
    LOG.info(f'TermTime activities by code: {len(activities_by_code)}')
    state.set('termtime.activities_by_code', activities_by_code)


def get_timeframe_dates(as_datetime=False):
    """
    Convenience function to get start and end dates of TermTime timeframe specified
    by `termtime.timeframe` (defaulting to 'All Year')

    """
    # which timeframe to use
    tf_code = state.get('termtime.timeframe', 'All Year')
    LOG.info(f'Restricting to timeframe: {tf_code}')
    # find timeframe matching tf_code
    timeframe = next(
        (tf for tf in state.get('termtime.timeframes', op_get_timeframes) if tf['c'] == tf_code),
        None
    )
    if timeframe is None:
        LOG.error(f"TermTime timeframe missing '{tf_code}'")
        raise TermTimeAPIError()
    # Convert to datetimes if required
    if as_datetime:
        # Convert to start and end datetimes
        start_dt = datetime.strptime(timeframe['fDate'], '%Y-%m-%d').replace(tzinfo=timezone.utc)
        end_dt = datetime.strptime(timeframe['lDate'], '%Y-%m-%d').replace(tzinfo=timezone.utc)
        return (start_dt, end_dt)
    else:
        return (timeframe['fDate'], timeframe['lDate'])


def op_update_rooms():
    """
    Use results of comparison operation to send POST and PUT requests to TermTime
    API to create and update rooms

    """
    # compare all rooms if haven't already
    comp = state.get('comparison.rooms', comparison.op_compare_rooms)

    for room in comp.get('create'):
        code = room['code']
        # Note: API POST uses `dc` for dept code instead of `dn`
        params = {(k if k != 'dn' else 'dc'): v for k, v in room.items() if k != 'code'}
        LOG.info(f"Creating room '{code}':")
        LOG.info(f"    Name    : '{params['n']}'")
        LOG.info(f"    Location: '{params['cam']}' '{params['b']}' '{params['f']}'")
        LOG.info(f"    Dept    : '{params['dc']}'")
        tt_post(f'room/{code}', params=params)

    for room in comp.get('update'):
        code = room['code']
        params = {k: v for k, v in room.items() if k != 'code'}
        LOG.info(f"Updating room '{code}':")
        LOG.info(f"    Name    : '{params['n']}'")
        LOG.info(f"    Location: '{params['cam']}' '{params['b']}' '{params['f']}'")
        LOG.info(f"    Dept    : '{params['dn']}'")
        tt_put(f'room/{code}', params=params)


def op_update_depts():
    """
    Use results of comparison operation to send POST and PUT requests to TermTime
    API to create and update departments

    """
    # compare all departments if haven't already
    comp = state.get('comparison.depts', comparison.op_compare_depts)

    for dept in comp.get('create'):
        code = dept['code']
        params = {k: v for k, v in dept.items() if k != 'code'}
        LOG.info(f"Creating department '{code}': '{params['n']}'")
        tt_post(f'department/{code}', params=params)

    for dept in comp.get('update'):
        code = dept['code']
        params = {k: v for k, v in dept.items() if k != 'code'}
        LOG.info(f"Updating department '{code}': '{params['n']}'")
        tt_put(f'department/{code}', params=params)


def op_update_campuses():
    """
    Use results of comparison operation to send POST and PUT requests to TermTime
    API to create and update campuses

    """
    # compare all campuses if haven't already
    comp = state.get('comparison.sites', comparison.op_compare_sites)

    for campus in comp.get('create'):
        # TermTime API for POSTing campuses uses campus name in url with 'code'
        # being in query string (confusingly)
        code = campus['code']
        new_name = campus['n']
        LOG.info(f"Creating campus '{code}': '{new_name}'")
        tt_post(f'campus/{new_name}', params={'code': code})

    for campus in comp.get('update'):
        # TermTime API for PUTting campuses can use either code or name in url
        # but need queryBy='Code' in query string to do former. New name goes in
        # 'n' in query string
        code = campus['code']
        new_name = campus['n']
        params = {
            'n': new_name,
            'queryBy': 'Code',
        }
        LOG.info(f"Updating campus '{code}': '{new_name}'")
        tt_put(f'campus/{code}', params=params)


def op_update_buildings():
    """
    Use results of comparison operation to send POST and PUT requests to TermTime
    API to create and update buildings

    """
    # compare all buildings if haven't already
    comp = state.get('comparison.buildings', comparison.op_compare_buildings)

    for b in comp.get('create'):
        # TermTime API for POSTing buildings uses buildings name in url along with
        # campus 'code' and building 'code' in the query string (+ queryBy='Code')
        # e.g. /building/{building_name}/{campus_code}?code={building_code}&queryBy=Code
        building_name = b['n']
        campus_code = b['camp']
        building_code = b['code']
        params = {
            'code': building_code,
            'queryBy': 'Code',
        }
        LOG.info(f"Creating building '{building_code}':")
        LOG.info(f"    Name       : '{building_name}'")
        LOG.info(f"    Campus Code: '{campus_code}'")
        tt_post(f'building/{building_name}/{campus_code}', params=params)

    # Need to be able to check TermTime building codes to names in hack below
    tt_b_by_code = {b['code']: b.get('n') for b in state.get('termtime.buildings')}

    for b in comp.get('update'):
        # TermTime API for PUTting buildings can be used without the campus code
        # in the url path if queryBy='Code' is put in the query string.
        # Name and campus code (allowing buildings to move) go in the query string.
        code = b['code']
        building_name = b['n']
        campus_code = b['camp']
        params = {
            'queryBy': 'Code',
            'camp': campus_code,
        }
        LOG.info(f"Updating building '{code}':")
        # Hack: TermTime PUT building endpoint complains with "Multiple objects
        # found - the Name is already used by another Building" if the update
        # doesn't change the building name but 'n' is in the request.
        # So we only PUT the 'n' in the params if the name has actually changed.
        if building_name != tt_b_by_code.get(code):
            params['n'] = building_name
            LOG.info(f"    Name       : '{building_name}'")
        else:
            LOG.info(f"    Name       : '{building_name}' (unchanged)")
        LOG.info(f"    Campus Code: '{campus_code}'")
        tt_put(f'building/{code}', params=params)


def op_update_floors():
    """
    Use results of comparison operation to send POST requests to TermTime API to
    create floors

    """
    # compare all floors if haven't already
    comp = state.get('comparison.floors', comparison.op_compare_floors)

    for floor in comp.get('create'):
        # TermTime API for POSTing floors requires floor name, buildings code,
        # and campus code in url path. Floor code goes in the query string
        # (and queryBy='Code'). As Booker has no concept of floor ids/codes we
        # will just have to create a sufficiently random one to be unique
        floor_name = floor['f']
        building_code = floor['b']
        campus_code = floor['c']
        new_code = token_hex(16)
        params = {
            'code': new_code,
            'queryBy': 'Code',
        }
        LOG.info(f"Creating floor '{new_code}':")
        LOG.info(f"    Name         : '{floor_name}'")
        LOG.info(f"    Building Code: '{building_code}'")
        LOG.info(f"    Campus Code  : '{campus_code}'")
        tt_post(f'floor/{floor_name}/{building_code}/{campus_code}', params=params)

    # As no one-to-one comparison of floors is possible we can never update
    # floors just create new ones as needed and potentially delete unused ones
    # (currently, the latter isn't implemented for fear of unexpected damage
    # with TermTime API)


def op_update_room_bookings():
    """
    Use results of comparison operation to send POST, PUT and DELETE requests to
    TermTime API to create, update and remove room bookings

    """
    # compare all room bookings if haven't already
    comp = state.get('comparison.roombookings', comparison.op_compare_room_bookings)

    for booking in comp.get('create'):
        code = booking['code']
        params = {k: v for k, v in booking.items() if k != 'code'}
        LOG.info(f"Creating room booking '{code}':")
        LOG.info(f"   Room         : '{params['rooms']}'")
        LOG.info(f"   Description  : '{params['des']}'")
        LOG.info(f"   Week/DoW     : '{params['pat']}' / '{params['day']}'")
        LOG.info(f"   Time/Duration: '{params['time']}' / '{params['du']}'")
        tt_post(f'roombooking/{code}', params=params)

    for booking in comp.get('update'):
        code = booking['code']
        params = {k: v for k, v in booking.items() if k != 'code'}
        # Note: roombooking endpoint only accepts 'code' not 'Code'
        params['queryBy'] = 'code'
        LOG.info(f"Updating room booking '{code}':")
        LOG.info(f"   Room         : '{params['rooms']}'")
        LOG.info(f"   Description  : '{params['des']}'")
        LOG.info(f"   Week/DoW     : '{params['pat']}' / '{params['day']}'")
        LOG.info(f"   Time/Duration: '{params['time']}' / '{params['du']}'")
        tt_put(f'roombooking/{code}', params=params)

    for booking in comp.get('delete'):
        code = booking['code']
        # Note: roombooking endpoint only accepts 'code' not 'Code'
        params = {'queryBy': 'code'}
        LOG.info(f"Removing room booking '{code}':")
        LOG.info(f"   Room         : '{booking['rooms']}'")
        LOG.info(f"   Description  : '{booking['des']}'")
        LOG.info(f"   Week/Day     : '{booking['pat']}' / '{booking['day']}'")
        LOG.info(f"   Time/Duration: '{booking['time']}' / '{booking['du']}'")
        tt_delete(f'roombooking/{code}', params=params)
