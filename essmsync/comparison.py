"""
Operations that compare the results of TermTime and Booker API get operations

"""
import logging

from . import state
from . import booker
from . import termtime
from . import utils
from .exceptions import ComparisonError

LOG = logging.getLogger(__name__)


def _compare_b_to_tt(data_type, booker_state, termtime_state, mapping, code='code'):
    """
    Generic comparison between Booker and TermTime, creating lists of necessary
    creations and updated in TermTime, and putting them in comparison state.

    :param data_type: type of data being compared as resulting state sub-key (e.g. sites)
    :type data_type: str
    :param booker_state: booker state (list of resources) to use in comparison
                         e.g. result of state.get('booker.sites')
    :type booker_state: list
    :param termtime_state: termtime state (list of resources) to use in comparison
                           e.g. result of state.get('termtime.campuses')
    :type termtime_state: list
    :param mapping: function called passed each booker entity that returns a dict containing
                    - 'expected' - expected TermTime keys and values
                    - 'update' - keys and values need to create or update TermTime
                    Returns None - if entry is invalid
    :type mapping: callable[[dict], dict]
    :param code: termtime field used for code (usually 'code', sometimes 'c'), defaults to 'code'
    :type code: str, optional

    """
    # TermTime entries by code (sometimes 'c'!)
    tt_by_code = {d[code]: d for d in termtime_state}
    # Results of comparison
    results = {
        'invalid': [],
        'match': [],
        'create': [],
        'update': [],
        'delete': [],  # in TermTime but not Booker
    }
    # Track matched TermTime entry codes
    matched_codes = []
    # Check each booker entry
    LOG.info(f'Comparing Booker {data_type} with TermTime')
    for d in booker_state:
        id = d.get('Id')
        # Booker data must have at least Id
        if id is None:
            desc = d.get('Description', 'Missing Description')
            LOG.warning(f"Booker {data_type} missing Id - '{desc}'")
            results['invalid'].append(d)
            continue
        # Get expected TermTime data and create/update data
        tt_mapping = mapping(d)
        if tt_mapping is None:
            # Booker entry invalid
            results['invalid'].append(d)
            continue
        # Set update mapping code
        tt_mapping['update']['code'] = id
        # Check existence in TermTime and match
        if id in tt_by_code:
            # flag as exists to aid finding extra TermTime entries later
            matched_codes.append(id)
            if _compare_expected(tt_mapping['expected'], tt_by_code[id], data_type, id):
                # comparison successful, add expected (with code) to 'match' results
                tt_mapping['expected'][code] = id
                results['match'].append(tt_mapping['expected'])
            else:
                # comparison failed, add needed update to 'update' results
                results['update'].append(tt_mapping['update'])
        else:
            # id not found so need to create
            results['create'].append(tt_mapping['update'])

    # Find those in TermTime but not Booker (ignoring many with code of None or 'None'!)
    results['delete'] = [
        d for c, d in tt_by_code.items()
        if c is not None and c != 'None' and c not in matched_codes
    ]
    # Log counts
    for k in results:
        LOG.info(f"...{k}: '{len(results[k])}'")
    # Put results of comparison in state
    state.set(f'comparison.{data_type}', results)


def _mapping_n(booker_entry):
    """
    Shared function that creates a mapping that only compares Booker 'Description'
    to TermTime 'n'

    >>> _mapping_n({}) is None
    True
    >>> _mapping_n({'Description':'Hypothetical Building'})
    {'expected': {'n': 'Hypothetical Building'}, 'update': {'n': 'Hypothetical Building'}}
    >>> _mapping_n({'Description':'  Unnecessarily Spaced Site  '})
    {'expected': {'n': 'Unnecessarily Spaced Site'}, 'update': {'n': 'Unnecessarily Spaced Site'}}
    """

    desc = booker_entry.get('Description')
    if desc is None:
        LOG.warning(f"Booker entry missing Description - '{booker_entry.get('Id')}'")
        return None

    # Booker Descriptions appear to frequently have trailing spaces!
    desc = desc.strip()

    return {
        'expected': {'n': desc},
        'update': {'n': desc},
    }


def op_compare_depts():
    """
    Compare departments between Booker and TermTime, and creating lists of necessary
    creations and updates and putting them in `comparison.depts`

    """
    # Get filtered depts list from Booker and full depts list from TermTime, if necessary.
    # Perform comparison - just Description to 'n' (depts use 'c' for code)
    _compare_b_to_tt(
        'depts',
        state.get('booker.depts', booker.op_get_depts),
        state.get('termtime.depts', termtime.op_get_depts),
        _mapping_n, 'c')


def op_compare_sites():
    """
    Compare sites/campuses between Booker and TermTime, and creating lists of necessary
    creations and updates and putting them in `comparison.sites`

    """
    # Get filtered sites list from Booker and full campuses list from TermTime, if necessary.
    # Perform comparison - just Description to 'n' (sites use 'code' for code)
    _compare_b_to_tt(
        'sites',
        state.get('booker.sites', booker.op_get_sites),
        state.get('termtime.campuses', termtime.op_get_campuses),
        _mapping_n)


def op_compare_buildings():
    """
    Compare buildings between Booker and TermTime, and creating lists of necessary
    creations and updates and putting them in `comparison.buildings`

    """
    # Need to be able to match site OptimeIndex to site Id
    sites_by_oi = {s['OptimeIndex']: s for s in state.get('booker.sites', booker.op_get_sites)}

    def mapping(booker_entry):
        """
        In addition to comparing 'Description' to 'n', also check that site's
        Description matches campus name ('cn').
        TermTime get buildings returns 'cn' for campus name but update uses 'camp'
        for campus name or code (better to use code)

        """
        # Common 'Description' to 'n' mapping
        m = _mapping_n(booker_entry)
        id = booker_entry.get('Id')
        # Validate SiteId (OptimeIndex) exists
        site_oi = booker_entry.get('SiteId')
        if site_oi is None:
            LOG.warning(f"Booker building missing SiteId - '{id}'")
            return None
        # Lookup site to get name and id
        site = sites_by_oi.get(site_oi)
        if site is None:
            LOG.warning(f"Booker building has missing SiteId '{site_oi}' - '{id}'")
            return None
        # Add Campuses Name (for comparison)
        m['expected']['cn'] = site.get('Description')
        # Add Campus Id (for update/create)
        m['update']['camp'] = site.get('Id')
        return m

    # Perform comparison
    _compare_b_to_tt(
        'buildings',
        state.get('booker.buildings', booker.op_get_buildings),
        state.get('termtime.buildings', termtime.op_get_buildings),
        mapping)


def op_compare_rooms():
    """
    Compare rooms between Booker and TermTime, and creating lists of necessary
    creations and updates and putting them in `comparison.rooms`

    """
    # Need to be able to match building.site OptimeIndex to site Id
    sites_by_oi = {s['OptimeIndex']: s for s in state.get('booker.sites', booker.op_get_sites)}

    # Need to be able to match Departments[0] OptimeIndex to department's Id
    depts_by_oi = {d['OptimeIndex']: d for d in state.get('booker.depts', booker.op_get_depts)}

    # Get TermTime capabilities
    tt_caps = state.get('termtime.capabilities', termtime.op_get_capabilities)

    # Need to be able to match Equipment OptimeIndex to it's Description but only
    # interested in those in TermTime capabilities
    eq_descs_by_oi = {
        eq['OptimeIndex']: eq['Description']
        for eq in state.get('booker.all_equipment', booker.op_get_all_equipment)
        if eq.get('Description') in tt_caps
    }
    # Warning if not all equipment match to capabilities
    s_eq = set(eq_descs_by_oi.values())
    s_caps = set(tt_caps)
    if s_caps - s_eq:
        missing_eq = ', '.join(sorted(s_caps - s_eq))
        LOG.warning(f"Booker missing equipment for TermTime capabilities - '{missing_eq}'")

    def mapping(booker_entry):
        """
        In addition to comparing 'Description' to 'n', also check that building
        code (f.bc), department code (d.c) and floor name (f.n) match.
        Create/update entry also needs floor, buildings and campus names (f, b, cam).

        """
        # Common 'Description' to 'n' mapping
        m = _mapping_n(booker_entry)
        id = booker_entry.get('Id')
        # Validate Building dict exists
        b = booker_entry.get('Building')
        if b is None or not isinstance(b, dict):
            LOG.warning(f"Booker room missing/invalid Building - '{id}'")
            return None
        # Validate Building.Id and Description exists
        b_id = b.get('Id')
        if b_id is None:
            LOG.warning(f"Booker room missing Building.Id - '{id}'")
            return None
        b_name = b.get('Description')
        if b_name is None:
            LOG.warning(f"Booker room missing Building.Description - '{id}'")
            return None
        # Validate Building.SiteId (OptimeIndex) exists
        site_oi = b.get('SiteId')
        if site_oi is None:
            LOG.warning(f"Booker room missing Building.SiteId - '{id}'")
            return None
        # Lookup site to get name and id
        site = sites_by_oi.get(site_oi)
        if site is None:
            LOG.warning(f"Booker room has missing SiteId '{site_oi}' - '{id}'")
            return None
        # Validate Departments[0] (OptimeIndex) exists
        try:
            dept_oi = booker_entry.get('Departments', [])[0]
        except IndexError:
            LOG.warning(f"Booker room missing Department - '{id}'")
            return None
        # Lookup department to get name and id
        dept = depts_by_oi.get(dept_oi)
        if dept is None:
            LOG.warning(f"Booker room has missing Department '{dept_oi}' - '{id}'")
            return None
        # Validate Floor name exists
        floor = booker_entry.get('Floor')
        if floor is None:
            LOG.warning(f"Booker room missing Floor - '{id}'")
            return None
        # Validate Equipment exists
        eq_ois = booker_entry.get('Equipment')
        if eq_ois is None:
            LOG.warning(f"Booker room missing Equipment - '{id}'")
            return None
        # Create list of equipment descriptions from OptimeIndexes (already filtered to only
        # those TermTime has as capabilities)
        eq_descs = [eq_descs_by_oi[oi] for oi in eq_ois if oi in eq_descs_by_oi]
        # As sorted comma-separated list to match pre-sorted TermTime capabilities
        expected_caps = ','.join(sorted(eq_descs))

        # Add building code, floor name, department code and capabilities (for comparison)
        m['expected']['f'] = {'bc': b_id, 'n': floor}
        m['expected']['d'] = {'c': dept.get('Id')}
        m['expected']['capabilities'] = expected_caps
        # Add floor, building, campus names and department code (for update)
        m['update']['f'] = floor
        m['update']['b'] = b_name
        m['update']['cam'] = site.get('Description')
        m['update']['dn'] = dept.get('Id')
        m['update']['capabilities'] = expected_caps
        return m

    # Perform comparison - rooms use 'c' for code
    _compare_b_to_tt(
        'rooms',
        state.get('booker.rooms', booker.op_get_rooms),
        state.get('termtime.rooms', termtime.op_get_rooms),
        mapping, 'c')


def op_compare_floors():
    """
    Compare floors between Booker and TermTime, and creating lists of necessary
    creations and updates and putting them in `comparison.floors`

    Booker floors are just strings on a room. The booker.get_floors operation
    constructs floors with 'Name', 'Building.Description' and 'Building.Site'
    (OptimeIndex that needs converting to a site Description). TermTime's floor
    'code's have no matching 'Id' in Booker, so a comparison by matching these
    is not possible. This comparison operation differs from by matching on multiple
    values.

    """
    # Need to be able to match site OptimeIndex to site Description
    sites_by_oi = {s['OptimeIndex']: s for s in state.get('booker.sites', booker.op_get_sites)}

    # TermTime entries as tuples (campus name, building name, floor name)
    tt_by_code = {f.get('code'): f for f in state.get('termtime.floors', termtime.op_get_floors)}

    # Results of comparison
    results = {
        'invalid': [],
        'match': [],
        'create': [],
        'update': [],  # always empty as one-to-one comparisons not possible
        'delete': [],  # in TermTime but not Booker
    }
    # Track matched TermTime entry codes
    matched_codes = []
    # Check each booker entry
    LOG.info('Comparing Booker floors with TermTime')
    for floor in state.get('booker.floors', booker.op_get_floors):
        # Validate floor name exists (and isn't blank)
        name = floor.get('Name')
        if name is None or name == '':
            LOG.warning(f"Booker floor missing/empty Name - '{floor.get('Id')}'")
            results['invalid'].append(floor)
            continue
        # Validate Building dict exists
        b = floor.get('Building')
        if b is None or not isinstance(b, dict):
            LOG.warning(f"Booker floor missing/invalid Building - '{floor.get('Id')}'")
            results['invalid'].append(floor)
            continue
        # Validate Building.SiteId (OptimeIndex) exists
        site_oi = b.get('SiteId')
        if site_oi is None:
            LOG.warning(f"Booker floor missing Building.SiteId - '{floor.get('Id')}'")
            results['invalid'].append(floor)
            continue
        # Lookup site to get name and id
        site = sites_by_oi.get(site_oi)
        if site is None:
            LOG.warning(f"Booker floor has missing SiteId '{site_oi}' - '{floor.get('Id')}'")
            results['invalid'].append(floor)
            continue

        # Create set of key/values to use to search for a match
        expected = {
            'cn': site.get('Description'),
            'bn': b.get('Description'),
            'n': name,
        }

        def compare_floors(expected, actual):
            for field in ['cn', 'bn', 'n']:
                if expected.get(field) != actual.get(field):
                    return False
            return True

        # Create list of matching TermTime floors
        matched = [c for c, f in tt_by_code.items()
                   if compare_floors(expected, f)]

        if len(matched) == 0:
            LOG.info(f"Floor {floor.get('Id')} not found")
            results['create'].append({
                'f': name,
                'b': b.get('Id'),
                'c': site.get('Id'),
            })
        elif len(matched) > 1:
            # Couldn't find a single match - not good
            LOG.warning(f"Booker floor matches {len(matched)} TermTime floors "
                        f"- '{floor.get('Id')}'")
            results['invalid'].append(floor)
            continue
        else:
            # Successfully matched
            tt_floor = tt_by_code.get(matched[0])
            results['match'].append(tt_floor)
            matched_codes.append(matched[0])

    # Find those in TermTime but not Booker
    results['delete'] = [d for c, d in tt_by_code.items() if c not in matched_codes]
    # Log counts
    for k in results:
        LOG.info(f"...{k}: '{len(results[k])}'")
    # Put results of comparison in state
    state.set('comparison.floors', results)


def op_compare_room_bookings():
    """
    Compare room bookings (not events) between Booker and TermTime, and creating
    lists of necessary creations and updates and putting them in
    `comparison.roombookings`.

    """
    # Room bookings are matched by TermTime code's being "BKR" + Booker's OptimeIndex.
    # We therefore need to construct an 'Id' for each Booker booking to compare.
    for b in state.get('booker.roombookings', booker.op_get_room_bookings):
        b['Id'] = f"BKR{b['OptimeIndex']}"

    # Owner and type of bookings to create/update in TermTime
    tt_owner = state.get('termtime.room_booking_owner', 'essm')
    tt_type = state.get('termtime.room_booking_type', 'bkr')

    def mapping(booker_entry):
        """
        Needs to check that Title ('des'), start and end times, and room id (rooms)
        match. Create/update entry also needs owner ("essm") and type ("bkr")

        """
        id = booker_entry.get('Id')
        title = booker_entry.get('Title')
        if title is None:
            LOG.warning(f"Booker room booking missing Title - '{id}'")
            return None
        title = title.strip()
        # Validate Room dict exists
        r = booker_entry.get('Room')
        if r is None or not isinstance(r, dict):
            LOG.warning(f"Booker room booking missing/invalid Room - '{id}'")
            return None
        # Validate Room.Id exists
        r_id = r.get('Id')
        if r_id is None:
            LOG.warning(f"Booker room booking missing Room.Id - '{id}'")
            return None
        # mapping so far
        m = {
            'expected': {
                'des': title,
                'rooms': r_id,
                'cap': '1',  # Returned as string
            },
            'update': {
                'des': title,
                'rooms': r_id,
                'owner': tt_owner,
                'type': tt_type,
                'cap': '1',  # Placeholder to have a valid value
            },
        }
        # Now for start, end times, convert Booker StartTime and EndTime to TermTime
        # time (hh:mm), du (hh:mm), day (1-7, 1=Mon), pat (ISO week, yyyy:Www)
        tt_booking_format = utils.termtime_booking_format_datetime(
            utils.booker_parse_datetime(booker_entry.get('StartTime')),
            utils.booker_parse_datetime(booker_entry.get('EndTime'))
        )

        # These are expected in comparison and needed for updates
        m['expected'].update(tt_booking_format)
        m['update'].update(tt_booking_format)
        return m

    # Perform comparison
    _compare_b_to_tt(
        'roombookings',
        state.get('booker.roombookings'),  # already retrieved to add Ids so no need for setter
        state.get('termtime.roombookings', termtime.op_get_room_bookings),
        mapping)


def op_compare_events():
    """
    Compare events between TermTime and Booker, and creating lists of necessary
    creations and updates and putting them in `comparison.events`.

    There is no shared id between Booker and TermTime events. We will therefore,
    use the `id` (actually Activity Id) combined with `instance` (individual booking
    of activity) from TermTime, and place this in the `Description` of the Booker
    booking. Not ideal but needed to link bookings together.

    This is the only TermTime to Booker comparison.
    """
    # Cache Booker events (bookings made by sync process)
    booker_events = state.get('booker.events', booker.op_get_events)

    # Cache type of bookings that are created in Booker
    booker_event_type = state.get('booker.booker_event_type', 'Teaching')

    # Cache Booker description (with embedded unique identifier) prefix
    booker_desc_prefix = state.get('booker.booker_event_prefix',
                                   'Booked by TermTime').strip()

    # Standard key/values needed to create new Booker booking
    new_event_template = {
      'Status': 'Approved',
      'BatchId': -1,
      'BatchPosition': -1,
      'BatchSize': -1,
      'Services_Layout': -1
    }

    # Need to map room codes/id to Booker OptimeIndexes
    room_oi_by_code = {
        b.get('Id'): b.get('OptimeIndex') for b in state.get('booker.rooms', booker.op_get_rooms)
    }

    # Creating a new booking needs the building (even though we provide the
    # room), so create a lookup list for room OptimeIndex to building OptimeIndex
    building_oi_by_room_oi = {
        b.get('OptimeIndex'): b.get('BuildingId')
        for b in state.get('booker.rooms', booker.op_get_rooms)
    }

    # TermTime Activities by code for getting extra activity data (crsid)
    acts_by_code = state.get('termtime.activities_by_code', termtime.op_get_activities_by_code)
    crsid_field = state.get('termtime.crsid_user_field', 'user5')
    bad_activities = set()

    # For quick lookup convert Booker events from list to dict keyed on tuple of activity code
    # (just the number) and instance with values of the list of events with that combination in
    # their Descriptions (ideally just one)
    booker_events_by_code = {}
    for b in booker_events:
        match = booker.EVENT_MATCH.search(b.get('Description', ''))
        if match:
            act_id_instance = match.group(1, 2)
            booker_events_by_code.setdefault(act_id_instance, []).append(b)

    # Booker staff (delay load until needed)
    booker_staff = None

    # Potential override of BookedBy
    override_booked_by_user = state.get('booker.override_booked_by_user')
    if override_booked_by_user is not None:
        override_booked_by_user = override_booked_by_user.strip().lower()

    # Results of comparison
    results = {
        'invalid': [],
        'match': [],
        'create': [],
        'update': [],
        'delete': [],
    }
    # Track matched Booker OptimeIndexes
    matched_ois = []
    # Check each termtime entry
    LOG.info('Comparing TermTime events with Booker')
    for event in state.get('termtime.events', termtime.op_get_events):
        # get required fields and validate they exist
        act_id = event.get('id', '')
        instance = event.get('instance')
        name = event.get('name', '').strip()
        roomcode = event.get('roomcode', '')
        if act_id == '':
            LOG.warning(f"TermTime event missing activity id - '{name}' in {roomcode}")
            results['invalid'].append(event)
            continue
        # Get the number part of event's (activity) id
        # i.e. the key of the activity in activities_by_code dict
        act_code = utils.act_code_from_event_id(act_id)
        if act_code is None:
            LOG.warning(f"TermTime event has bad activity id '{act_id}' - '{name}' in {roomcode}")
            results['invalid'].append(event)
            continue
        if instance is None:
            LOG.warning(f"TermTime event missing activity instance - '{name}' in {roomcode}")
            results['invalid'].append(event)
            continue
        # construct unique 'code' from activity id and instance
        code = f'{act_id}:{instance}'
        if name == '':
            LOG.warning(f"TermTime event missing/empty name - '{code}'")
            results['invalid'].append(event)
            continue
        if roomcode == '':
            LOG.warning(f"TermTime event missing/empty room code - '{code}'")
            results['invalid'].append(event)
            continue
        room_oi = room_oi_by_code.get(roomcode)
        if room_oi is None:
            LOG.error(f"TermTime event in room '{roomcode}' has no matching room "
                      f"in Booker - '{code}'")
            raise ComparisonError("TermTime event's room missing in Booker")

        # translate dates from TermTime to Booker format
        start = utils.booker_format_datetime(utils.termtime_parse_datetime(event.get('start')))
        end = utils.booker_format_datetime(utils.termtime_parse_datetime(event.get('end')))

        # Create set of expected key/values of the Booker booking
        expected = {
            'Title': name,
            'Room': {
                'Id': roomcode
            },
            'StartTime': start,
            'EndTime': end,
            'Type': booker_event_type,
            'ExpectedAttendees': event.get('size', 1),
        }

        # Search for a matching Booker booking (instance needs to be a string as it was found in
        # the Description via a regex)
        matched = booker_events_by_code.get((act_code, str(instance)), [])
        if len(matched) == 0:
            # Create new booking from template and expected values including
            # constructed unique Description, RoomId and BuildingId (last two needing
            # to be obtained from rooms list)
            new_event = {
                **new_event_template,
                **expected,
                'code': code,  # For logging when creating
                'RoomId': room_oi,  # Booker room OptimeIndex not actually Id
                'BuildingId': building_oi_by_room_oi.get(room_oi),
                # Only set Description for new events
                'Description': f'{booker_desc_prefix} [{code}]',
            }
            results['create'].append(new_event)
        elif len(matched) > 1:
            # Couldn't find a single match - not good
            LOG.error(f"TermTime event matches {len(matched)} Booker bookings "
                      f"- '{code}'")
            # Add them all to matched_ois so they don't get deleted
            matched_ois.extend([m.get('OptimeIndex') for m in matched])
            # But put the event in invalid result
            results['invalid'].append(event)
        else:
            # Successfully matched
            match = matched[0]
            matched_ois.append(match.get('OptimeIndex'))

            # If TermTime event has activity with CRSid user field then we expect
            # sufficient parts of BookedBy to match
            act = acts_by_code.get(act_code)
            if act is None:
                # Only warn once per activity
                if act_id not in bad_activities:
                    LOG.warning(f'{act_id} missing from TermTime Activities list')
                    bad_activities.add(act_id)
            elif act.get(crsid_field) is None:
                if act_id not in bad_activities:
                    LOG.warning(f'{act_id} missing crsid field ({crsid_field})')
                    bad_activities.add(act_id)
            else:
                # Allow overriding BookedBy user with `booker.override_booked_by_user` state key
                # - useful for testing without actually sending emails to others
                crsid = (
                    override_booked_by_user if override_booked_by_user is not None
                    else act.get(crsid_field).strip().lower()
                )
                # Delayed load of all staff until first needed (as there are many)
                if booker_staff is None:
                    booker_staff = state.get('booker.staff_by_id', booker.op_get_staff_by_id)
                staff = booker_staff.get(crsid)
                if staff is None:
                    if act_id not in bad_activities:
                        LOG.warning(f"{act_id} crsid '{crsid}' not found in Booker staff")
                        bad_activities.add(act_id)
                else:
                    # Have enough to check minimal pieces of BookedBy object (enough to update)
                    expected['BookedBy'] = {
                        'Id': crsid,
                        'UserType': 'Staff',
                        'UserOptimeIndex': staff['OptimeIndex'],
                    }

            # Compare expection with matched booking
            if _compare_expected(expected, match, 'event', code):
                # comparison successful, add expected (with code) to 'match' results
                results['match'].append({
                    **expected,
                    'code': code,
                })
            else:
                # comparison failed, add needed update to 'update' results
                # Use matched event combined with expected values including
                # RoomId and BuildingId (to be obtained from rooms list)
                updated_event = {
                    **match,
                    **expected,
                    'code': code,  # For logging when updating
                    'RoomId': room_oi,  # Booker room OptimeIndex not actually Id
                    'BuildingId': building_oi_by_room_oi.get(room_oi),
                }
                results['update'].append(updated_event)

    # Find those in Booker but not TermTime
    results['delete'] = [b for b in booker_events if b.get('OptimeIndex') not in matched_ois]
    # Log counts
    for k in results:
        LOG.info(f"...{k}: '{len(results[k])}'")
    # Put results of comparison in state
    state.set('comparison.events', results)


def _compare_expected(expected, actual, data_type, id):
    """
    Compares all the keys (and sub-keys) in 'expected' dict with the values in
    'actual' dict. Returns False if any don't match.

    >>> # single level matching returns True
    >>> _compare_expected({'a':1, 'b':'two'}, {'a':1, 'b':'two'}, 'test', 1)
    True
    >>> # single level mismatched returns False
    >>> _compare_expected({'a':1, 'b':'two'}, {'a':1, 'b':'three'}, 'test', 1)
    False
    >>> # nested matching returns True
    >>> _compare_expected({'a':{'b':'two'}}, {'a':{'b':'two'}}, 'test', 1)
    True
    >>> # nested mismatched returns False
    >>> _compare_expected({'a':{'b':'two'}}, {'a':{'b':'three'}}, 'test', 1)
    False
    >>> # missing key in actual raises ComparisonError
    >>> _compare_expected({'a':1, 'b':'two'}, {'a':1, 'c':'bad'}, 'test', 1)
    Traceback (most recent call last):
        ...
    essmsync.exceptions.ComparisonError: Missing expected key
    >>> # dict in expected but not in actual raises ComparisonError
    >>> _compare_expected({'a':{'b':'two'}}, {'a':1, 'b':'bad'}, 'test', 1)
    Traceback (most recent call last):
        ...
    essmsync.exceptions.ComparisonError: Expected dict
    >>> # dict in actual but not in expected raises ComparisonError
    >>> _compare_expected({'a':1}, {'a':{'b':'bad'}}, 'test', 1)
    Traceback (most recent call last):
        ...
    essmsync.exceptions.ComparisonError: Unexpected dict
    """
    success = True
    for k, v in expected.items():
        # Check for fatal missing and type mismatches before doing comparison
        if k not in actual:
            LOG.error(f"'{k}' missing from {data_type} '{id}'")
            raise ComparisonError('Missing expected key')
        elif isinstance(v, dict):
            if not isinstance(actual[k], dict):
                LOG.error(f"'{k}' expected to be dict in {data_type} '{id}'")
                raise ComparisonError('Expected dict')
            # Recursively, compare dicts
            if not _compare_expected(v, actual[k], data_type, id):
                success = False
        elif isinstance(actual[k], dict):
            LOG.error(f"'{k}' not expected to be dict in {data_type} '{id}'")
            raise ComparisonError('Unexpected dict')
        elif v != actual[k]:
            LOG.info(f"'{k}' mismatch for {data_type} '{id}' ('{v}' != '{actual[k]}')")
            success = False
    return success
