"""
Core operations not tied to a specific service's API

"""
import logging
import json
import yaml

from . import state

LOG = logging.getLogger(__name__)


# Argument helper functions

def bool_arg(value):
    """
    As args are always parsed to strings, need to convert to a boolean.
    Return a None if already or not convertable.

    >>> bool_arg(0) or bool_arg("0")
    False
    >>> bool_arg(1) and bool_arg("1")
    True
    >>> bool_arg("true") and bool_arg("True")
    True
    >>> bool_arg("false") or bool_arg("False")
    False
    >>> bool_arg(None) is None
    True
    >>> bool_arg(['unconvertable']) is None
    True
    """
    if value is None or isinstance(value, bool):
        return value
    if isinstance(value, int):
        return bool(value)
    if isinstance(value, str):
        if value.isnumeric():
            return bool(int(value))
        return value.lower() != "false"
    return None


def int_arg(value):
    """
    As args are always parsed to strings, need to convert to an integer.
    Return a None if already or not convertable.

    >>> int_arg(100) == int_arg("100") == 100
    True
    >>> int_arg(1) == int_arg("1") == int_arg(True) == 1
    True
    >>> int_arg(0) == int_arg("0") == int_arg(False) == 0
    True
    >>> int_arg('bad') is None
    True
    """
    if value is None or isinstance(value, int):
        return value
    if isinstance(value, bool) or value.isnumeric():
        return int(value)
    return None


# Core operations

def op_test(*args):
    """
    Test operation (for testing)

    >>> op_test()
    Test called without arguments
    >>> op_test('a', 'b')
    Test called with ('a', 'b')
    """
    if len(args) > 0:
        print(f"Test called with {args}")
    else:
        print("Test called without arguments")


def op_dump(key, keep_parents=True):
    """
    Shortcut to op_dump_yaml

    >>> state.set(None, {'d':{'a':'b','l':[{'id':1},{'id':2}]}})
    >>> op_dump('d.l')
    d:
      l:
      - id: 1
      - id: 2
    <BLANKLINE>
    >>> op_dump('d.l', False)
    - id: 1
    - id: 2
    <BLANKLINE>
    """
    op_dump_yaml(key, keep_parents)


def op_dump_yaml(key, keep_parents=True):
    """
    Dump key of configuration as yaml, optionally maintaining parents of key.
    By default, parents are maintained to help reusing output as extra
    configuration to be included on future call.

    >>> state.set(None, {'dy':{'a':'b','l':[{'id':1},{'id':2}]}})
    >>> op_dump_yaml('dy.l')
    dy:
      l:
      - id: 1
      - id: 2
    <BLANKLINE>
    >>> op_dump_yaml('dy.l', False)
    - id: 1
    - id: 2
    <BLANKLINE>
    """
    dump = state.get(key)
    if bool_arg(keep_parents):
        keys = key.split('.')
        keys.reverse()
        for k in keys:
            dump = {k: dump}
    print(yaml.dump(dump, sort_keys=True))


def op_dump_json(key, keep_parents=False, indent=None):
    """
    Dump key of configuration as json, optionally maintaining parents of key
    By default, parents are not maintained as a typical json dump is to get
    just the specific object.
    Indentation can be provided to 'prettify' the result using specified number
    of spaces.

    >>> state.set(None, {'dj':{'a':'b','l':[{'id':1},{'id':2}]}})
    >>> op_dump_json('dj.l')
    [{"id": 1}, {"id": 2}]
    >>> op_dump_json('dj.l', True)
    {"dj": {"l": [{"id": 1}, {"id": 2}]}}
    >>> op_dump_json('dj.l', False, 2)
    [
      {
        "id": 1
      },
      {
        "id": 2
      }
    ]
    """
    dump = state.get(key)
    if bool_arg(keep_parents):
        keys = key.split('.')
        keys.reverse()
        for k in keys:
            dump = {k: dump}
    print(json.dumps(dump, sort_keys=True, indent=int_arg(indent)))
