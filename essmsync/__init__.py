"""
Education Space Scheduling and Modelling synchronisation tool

Usage:
    essmsync (-h | --help)
    essmsync [--configuration=FILE]... [--quiet] [--debug] [--http-debug] [--really-do-this]
             [<operation>]...

Options:
    -h, --help                  Show a brief usage summary.

    --quiet                     Reduce logging verbosity.
    --debug                     Log debugging information
    --http-debug                Log debugging information from http calls

    -c, --configuration=FILE    Specify configuration file to load. Multiple
                                files will be merged (latter taking precedence).

    --really-do-this            Actually try to make the changes.

"""
import logging
import os
import sys
import http

import docopt

from . import state
from . import sync


LOG = logging.getLogger(os.path.basename(sys.argv[0]))


def main():
    opts = docopt.docopt(__doc__)

    # Configure logging
    logging.basicConfig(
        level=logging.DEBUG if opts['--debug'] else
        logging.WARN if opts['--quiet'] else logging.INFO
    )

    # Debug logging (don't use in production - secrets are displayed)
    if opts['--http-debug']:
        # Also enable requests debug logging
        http.client.HTTPConnection.debuglevel = 1
        req_log = logging.getLogger('requests.packages.urllib3')
        req_log.setLevel(logging.DEBUG)
        req_log.propagate = True

    config_files = expand_lists(opts['--configuration'])
    if len(config_files) == 0:
        # default to attempting to load configuration.yaml and secrets.yaml
        config_files = ['configuration.yaml', 'secrets.yaml']
    state.load_configs(config_files)

    # Add read only flag to configuration
    state.set('read_only', not opts['--really-do-this'])

    # Add any operations specified on command line
    state.set('operations', opts['<operation>'], False)

    # Perform sync actions
    sync.sync()


def expand_lists(list):
    """
    Iterate over list, if any comma-separated entries are found then split them
    inserting them in place.
    >>> expand_lists(['a', 'b'])
    ['a', 'b']
    >>> expand_lists(['a,b'])
    ['a', 'b']
    >>> expand_lists(['a,b', 'c', 'd'])
    ['a', 'b', 'c', 'd']
    >>> expand_lists(['a', 'b,c', 'd'])
    ['a', 'b', 'c', 'd']
    >>> expand_lists(['a', 'b', 'c,d'])
    ['a', 'b', 'c', 'd']
    >>> expand_lists(['a', 'b,c,d', 'e'])
    ['a', 'b', 'c', 'd', 'e']
    """
    return [y for x in list for y in x.split(',')]
