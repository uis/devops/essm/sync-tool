"""
Handle authentication and usage of Booker API

"""
import logging
import re

from . import state
from . import utils
from .exceptions import BookerAPIError, ConfigurationInvalid, BookerAPIBookingConflict
from . import termtime
from . import comparison

LOG = logging.getLogger(__name__)

# Regex for matching activity:instance references in booking descriptions
EVENT_MATCH = re.compile(r'\[Activity-(\d+)\:(\d+)\]')


def booker_get(resource, **kwargs):
    return utils.api_get(
        state.get('booker.api.url') + resource,
        exception=BookerAPIError,
        headers=booker_headers(),
        **kwargs
    )


def booker_post(resource, with_auth=True, is_update=True, **kwargs):
    if is_update and state.get('read_only', True):
        return None
    return utils.api_post(
        state.get('booker.api.url') + resource,
        exception=BookerAPIError,
        headers=booker_headers(with_auth),
        exception_handler=booker_api_handler,
        **kwargs
    )


def booker_put(resource, **kwargs):
    if state.get('read_only', True):
        return None
    return utils.api_put(
        state.get('booker.api.url') + resource,
        exception=BookerAPIError,
        headers=booker_headers(),
        exception_handler=booker_api_handler,
        **kwargs
    )


def booker_headers(with_auth=True):
    h = {'user-agent': state.USER_AGENT}
    if with_auth:
        h['Authorization'] = 'Bearer ' + state.get('booker.api.access_token')
    return h


def booker_api_handler(url, response):
    if 'booking' in url and response.status_code == 409:
        # A 409 to booking endpoint should raise a conflict exception when making/updating booking
        return BookerAPIBookingConflict()
    LOG.error(f"Booker API error: {response.status_code} {response.reason} {response.text}")
    return None


def auth():
    LOG.info('Authenticating Booker API')

    # Only request an access token if we don't already have one
    if not state.get('booker.api.access_token'):
        (token_resp, _) = booker_post(
            # v1.21 moves endpoint to 'api/Token' so no need for 'api/' in all
            # other endpoint usage, just append it to `booker.api.url` in configuration
            'Token',
            # Don't pass auth token as that's what we're trying to get
            with_auth=False,
            is_update=False,
            # Token endpoint only takes form-data not json!
            data={
                # v1.21 requires 'email' instead of 'username'
                'email': state.get('booker.api.email'),
                'password': state.get('booker.api.password')
            }
        )
        access_token = token_resp.get('access_token')
        if not access_token:
            LOG.error('Booker API authentication failed')
            raise BookerAPIError()
        LOG.info('Booker API access token retrieved')
        state.set('booker.api.access_token', access_token)
    else:
        LOG.info('Booker API access token already provided')

    # No ping to validate access_token (so use roomtypes as a low impact)
    (roomtypes, _) = booker_get('roomtypes')
    # Should have a non-empty list back
    if not isinstance(roomtypes, list) or len(roomtypes) == 0:
        LOG.error('Booker API authentication failed')
        raise BookerAPIError()
    LOG.info('Booker API authentication succeeded')


def _op_get_list(endpoint, target, overwrite=True, **kwargs):
    """
    Generic function for getting a list of from and endpoint and putting it in
    `booker.<target>`

    """
    (l, _) = booker_get(f"{endpoint}", **kwargs)
    if not isinstance(l, list):
        LOG.error(f'Booker {endpoint} result is not a list')
        raise BookerAPIError()
    LOG.info(f'Booker {target}: {len(l)}')
    state.set(f'booker.{target}', l, overwrite)
    return l


# Booker operations

# Full list operations

def op_get_all_equipment():
    """
    Get list of all equipment types and put in `booker.all_equipment`

    """
    _op_get_list('equipment', 'all_equipment')


def op_get_all_depts():
    """
    Get list of all departments (aka institutions) and put in `booker.all_depts`

    """
    _op_get_list('departments', 'all_depts')


def op_get_all_floors():
    """
    Get list of all floors and put in `booker.all_floors`. Booker doesn't store
    floors as separate entities, just a string on the room entity, so objects
    with building, floor name and compiled id are created.

    """
    # Get a compiled list of floor 'objects'
    floors = _compile_floors(state.get('booker.all_rooms', op_get_all_rooms))
    if len(floors) == 0:
        LOG.error('No floors in Booker')
        raise BookerAPIError()
    LOG.info(f'Booker floors: {len(floors)}')
    state.set('booker.all_floors', floors)


def op_get_all_sites():
    """
    Get list of all sites and put in `booker.all_sites`

    """
    _op_get_list('sites', 'all_sites')


def op_get_all_buildings():
    """
    Get list of all buildings and put in `booker.all_buildings`

    """
    _op_get_list('buildings', 'all_buildings')


def op_get_all_rooms():
    """
    Get list of all rooms and put in `booker.all_rooms`

    """
    _op_get_list('rooms', 'all_rooms')


def op_get_all_staff():
    """
    Get list of all staff, filter fields to keep and put in `booker.all_staff`

    """
    # Filter which fields to keep
    filtered_keys = ['Id', 'OptimeIndex', 'email', 'forename', 'surname']
    all_staff = [
        {k: v for k, v in s.items() if k in filtered_keys}
        for s in _op_get_list('staff', 'all_staff')
    ]
    state.set('booker.all_staff', all_staff)


def op_get_staff_by_id():
    """
    Get dict of all staff keyed on Booker.Id with filtered dict of data and
    put in `booker.staff_by_id`

    """
    all_staff = state.get('booker.all_staff', op_get_all_staff)
    # Ignore staff without an Id (hopefully none) and those without a crsid-like Id
    staff_by_id = {
        _sanitised_staff_id(s['Id']): s
        for s in all_staff if _valid_staff_id(s.get('Id'))
    }
    LOG.info(f'Booker staff by id: {len(staff_by_id)}')
    state.set('booker.staff_by_id', staff_by_id)


def _valid_staff_id(id):
    """ Smells like a crsid? """
    if id is None:
        return False
    id = _sanitised_staff_id(id)
    return re.fullmatch('[a-z][a-z0-9]{2,7}', id)


def _sanitised_staff_id(id):
    """ Clean up Ids that have whitespace and are uppercase """
    return id.strip().lower()


# List operations filtered by rooms with TermTime equipment

def op_get_rooms():
    """
    Get list of rooms with 'TermTime' equipment and put in `booker.rooms`

    """
    # Need the equipment used to indicate sync with TermTime
    tt_equip = state.get('booker.termtime_equipment', _get_termtime_equipment)

    # Now get all rooms (no way to filter API get) and filter them
    oi = tt_equip['OptimeIndex']
    LOG.info(f"Booker 'TermTime' equipment OptimeIndex: {oi}")
    filtered_rooms = [
        r for r in state.get('booker.all_rooms', op_get_all_rooms)
        if 'Equipment' in r and oi in r['Equipment']
    ]
    only_buildings = state.get('booker.only_buildings', [])
    if only_buildings:
        if not isinstance(only_buildings, list):
            only_buildings = [only_buildings]
        LOG.info(f"Filtering rooms to those only in buildings: {', '.join(only_buildings)}")
        filtered_rooms = [
            r for r in filtered_rooms
            if ('Building' in r and 'Id' in r['Building'] and
                r['Building']['Id'] in only_buildings)
        ]
    exclude_buildings = state.get('booker.exclude_buildings', [])
    if exclude_buildings:
        if not isinstance(exclude_buildings, list):
            exclude_buildings = [exclude_buildings]
        LOG.info(f"Filtering rooms to exclude those in buildings: {', '.join(exclude_buildings)}")
        filtered_rooms = [
            r for r in filtered_rooms
            if ('Building' in r and 'Id' in r['Building'] and
                r['Building']['Id'] not in exclude_buildings)
        ]
    if len(filtered_rooms) == 0:
        LOG.error('No rooms in Booker set to be used by TermTime')
        raise BookerAPIError()
    LOG.info(f'Booker filtered rooms: {len(filtered_rooms)}')
    state.set('booker.rooms', filtered_rooms)


def op_get_depts():
    """
    Get list of departments related to rooms with 'TermTime' equipment and put
    in `booker.depts`

    """
    # Since v1.21 the room response has no 'DepartmentId' just the list of departments
    # under 'Departments'. We'll assume that the first in the list is the room's department
    # we want (as TermTime only has one per room) but warn if we see more than one or none.

    # Get all (non -1) department OptimeIndexes for filtered rooms
    dept_ois = set()
    for d in state.get('booker.rooms', op_get_rooms):
        dl = d.get('Departments', [])
        if len(dl) == 0:
            LOG.warning(f"Room '{d['Id']}' has no department")
            continue
        if len(dl) > 1:
            LOG.warning(f"Room '{d['Id']}' has {len(dl)} departments")
        dept_ois.add(dl[0])

    if len(dept_ois) == 0:
        LOG.error('No departments in Booker related to TermTime rooms')
        raise BookerAPIError()
    LOG.info(f'Booker filtered department OptimeIndexes: {len(dept_ois)}')

    # Now get all departments (no way to filter API get) and filter to those in the id list
    depts = [d for d in state.get('booker.all_depts', op_get_all_depts)
             if d['OptimeIndex'] in dept_ois]
    LOG.info(f'Booker filtered departments: {len(depts)}')
    if len(depts) != len(dept_ois):
        LOG.warning(f'Booker missing {len(dept_ois) - len(depts)} departments')

    state.set('booker.depts', depts)


def op_get_buildings():
    """
    Get list of buildings related to rooms with 'TermTime' equipment and put
    in `booker.buildings`

    """
    # Building property of room response contains building entity
    # Convert dict keyed on BuildingId to list to remove duplicates
    buildings = list({
        r['BuildingId']: r['Building'] for r in state.get('booker.rooms', op_get_rooms)
        if r.get('BuildingId', -1) != -1 and r.get('Building')
    }.values())
    if len(buildings) == 0:
        LOG.error('No buildings in Booker related to TermTime rooms')
        raise BookerAPIError()
    LOG.info(f'Booker filtered buildings: {len(buildings)}')
    state.set('booker.buildings', buildings)


def op_get_sites():
    """
    Get list of sites related to rooms with 'TermTime' equipment and put
    in `booker.sites`

    """
    # In v1.21 the 'Site' property of the 'Building' property of a room response changed from
    # the OptimeIndex to the site object and 'SiteId' became the OptimeIndex
    # Use this Site property of the buildings list (itself the Buildings property of the rooms
    # list) to build the list of sites.
    # Convert dict keyed on SiteId to list to remove duplicates
    sites = list({
        b['SiteId']: b['Site'] for b in state.get('booker.buildings', op_get_buildings)
        if b.get('SiteId', -1) != -1 and b.get('Site')
    }.values())
    if len(sites) == 0:
        LOG.error('No sites in Booker related to TermTime rooms')
        raise BookerAPIError()
    LOG.info(f'Booker filtered sites: {len(sites)}')
    state.set('booker.sites', sites)


def op_get_floors():
    """
    Get a list of floors related to rooms with 'TermTime' equipment and put in
    `booker.floors`. Booker doesn't store floors as separate entities, just a
    string on the room entity, so objects with building, floor name and compiled
    id are created.

    """
    # Get a compiled list of floor 'objects' from filtered rooms
    floors = _compile_floors(state.get('booker.rooms', op_get_rooms))
    if len(floors) == 0:
        LOG.error('No floors in Booker related to TermTime rooms')
        raise BookerAPIError()
    LOG.info(f'Booker filtered floors: {len(floors)}')
    state.set('booker.floors', floors)


def _compile_floors(rooms):
    """
    Compiles a list of floor 'objects' from the specified rooms list.

    """
    floors_by_id = {}
    for r in rooms:
        if r['BuildingId'] != -1:
            id = f"{r['Building']['Id']}-{r['Floor']}"
            floors_by_id[id] = {
                'Id': id,
                'BuildingId': r['BuildingId'],
                'Building': r['Building'],
                'Name': r['Floor'],
            }
    # Return as a list
    return list(floors_by_id.values())


def op_get_room_bookings(start=None, end=None):
    """
    Get a list of Booker bookings (defaulting to within termtime.timeframe),
    for TermTime managed rooms, that were not synced from TermTime events
    (i.e. don't have an [activity:instance] reference in their description)
    and put in booker.roombookings

    """
    # get all bookings keeping only those that are *not* event bookings
    bookings = _get_bookings(start, end, lambda b: not is_event_booking(b))
    LOG.info(f'Bookings not previously synced: {len(bookings)}')
    state.set('booker.roombookings', bookings)


def op_get_events(start=None, end=None):
    """
    Get a list of Booker bookings (defaulting to within termtime.timeframe),
    for TermTime managed rooms, that were previously synced from TermTime events
    (i.e. have an [activity:instance] reference in their description) and put
    them in booker.events

    """
    # get all bookings keeping only those that are event bookings
    bookings = _get_bookings(start, end, is_event_booking)
    LOG.info(f'Bookings previously synced: {len(bookings)}')
    state.set('booker.events', bookings)


def is_event_booking(b):
    """
    Use the description of a Booker booking to determine if it is an event
    (activity instance already synced) or a 3rd party booking.

    """
    return EVENT_MATCH.search(b.get('Description', ''))


def op_update_events():
    """
    Use results of comparison operation to send POST and PUT requests to
    Booker API to create and update events

    """
    # compare all room bookings if haven't already
    comp = state.get('comparison.events', comparison.op_compare_events)

    counter = 0
    for booking in comp.get('update')[:state.get('booker.update_limit', 250)]:
        code = booking['code']
        room_id = booking['Room']['Id']
        params = {k: v for k, v in booking.items() if k not in ['code', 'Room']}
        counter += 1
        LOG.info(f"Updating event '{code}'/'{params['OptimeIndex']}': [{counter}]")
        LOG.info(f"   Start Time : '{params['StartTime']}'")
        LOG.info(f"   End Time   : '{params['EndTime']}'")
        LOG.info(f"   Room Id/OI : '{room_id}'/'{params['RoomId']}'")
        LOG.info(f"   Building OI: '{params['BuildingId']}'")
        LOG.info(f"   Title      : '{params['Title']}'")
        LOG.info(f"   Booked By  : '{params['BookedBy']['Id']}'")
        try:
            booker_put(f"booker/booking/{booking['OptimeIndex']}", json=params)
        except BookerAPIBookingConflict:
            # hopefully another update will remove the conflict and this will succeed next time
            LOG.warning(f"Failed to update event '{code}' due to conflict")

    counter = 0
    for booking in comp.get('create')[:state.get('booker.create_limit', 250)]:
        code = booking['code']
        room_id = booking['Room']['Id']
        params = {k: v for k, v in booking.items() if k not in ['code', 'Room']}
        counter += 1
        LOG.info(f"Creating event '{code}': [{counter}]")
        LOG.info(f"   Start Time : '{params['StartTime']}'")
        LOG.info(f"   End Time   : '{params['EndTime']}'")
        LOG.info(f"   Room Id/OI : '{room_id}'/'{params['RoomId']}'")
        LOG.info(f"   Building OI: '{params['BuildingId']}'")
        LOG.info(f"   Title      : '{params['Title']}'")
        try:
            booker_post('booker/booking', json=params)
        except BookerAPIBookingConflict:
            # hopefully an update will remove the conflict and this will succeed next time
            LOG.warning(f"Failed to create event '{code}' due to conflict")


def op_cancel_events():
    """
    Use results of comparison operation to send PUT requests to Booker API to cancel deleted events

    """
    # compare all room bookings if haven't already
    comp = state.get('comparison.events', comparison.op_compare_events)

    # regex for finding code in description
    code_re = re.compile(r'Activity\-\d+:\d+')

    for booking in comp.get('delete')[:state.get('booker.cancel_limit', 50)]:
        # look for code in description, e.g. "Activity-X:Y"
        code_match = code_re.search(booking['Description'])
        if code_match is None:
            # If the description doesn't have the expected code in it, let's
            # not cancel it as it may not be one of ours
            LOG.warning(f"Failed to cancel event '{booking['OptimeIndex']}' - missing code in "
                        f"Description: '{booking['Description']}'")
            continue
        code = code_match[0]

        room_id = booking['Room']['Id']
        # sending entire booking with just status changed is possibly unnecessary but easiest
        params = {k: v if k != 'Status' else 'Cancelled' for k, v in booking.items()}

        LOG.info(f"Cancelling event '{code}'/'{params['OptimeIndex']}':")
        LOG.info(f"   Start Time : '{params['StartTime']}'")
        LOG.info(f"   End Time   : '{params['EndTime']}'")
        LOG.info(f"   Room Id/OI : '{room_id}'/'{params['RoomId']}'")
        LOG.info(f"   Building OI: '{params['BuildingId']}'")
        LOG.info(f"   Title      : '{params['Title']}'")

        booker_put(f"booker/booking/{booking['OptimeIndex']}", json=params)


def op_find_conflicts():
    """
    Use results of comparison operation and booker.roombookings to attempt to
    find conflicts between events to be created and existing bookings.
    Rather simple check that doesn't cope with sub-rooms, multiple creates
    overlapping, or updates at all.

    """
    all_conflicts = []
    comp = state.get('comparison.events', comparison.op_compare_events)
    creations = comp.get('create', [])
    title_match = 0
    if len(creations) == 0:
        LOG.info('No event creations to look for conflicts for')
    else:
        existing_bookings = state.get('booker.roombookings', op_get_room_bookings)
        if len(existing_bookings) == 0:
            LOG.info('No existing bookings to look for conflicts against')
        else:
            LOG.info(
                f'Comparing {len(creations)} event creations against '
                f'{len(existing_bookings)} existing bookings'
            )
            for event in creations:
                for booking in existing_bookings:
                    if _conflict(event, booking):
                        all_conflicts.append((event, booking))
                        if booking['Title'].strip() == event['Title'].strip():
                            title_match += 1
                        other_room = (
                            '' if event['Room']['Id'] == booking['Room']['Id']
                            else f"/{booking['Room']['Id']}"
                        )
                        LOG.info(
                            f"Conflict in room '{event['Room']['Id']}{other_room}' "
                            f"on {event['StartTime'][:10]}:"
                        )
                        LOG.info(f"...{event['code']}: {event['Title']}")
                        LOG.info(f"...{event['StartTime']} to {event['EndTime']}")
                        LOG.info(f"...{booking['OptimeIndex']}: {booking['Title']}")
                        LOG.info(f"...{booking['StartTime']} to {booking['EndTime']}")

    LOG.info(f'Conflicts found: {len(all_conflicts)}')
    LOG.info(f'    Title match: {title_match}')
    state.set('booker.conflicts', all_conflicts)


def op_missing_staff():
    """
    Use results of termtime.get_events operation to filter termtime.activities
    to only those being used. Get the requestors from these and check if a
    matching Staff member exists in Booker. Logs and store the missing ones as
    dict of id to list of activities codes they are requestors for.

    """
    # Get all termtime events
    events = state.get('termtime.events', termtime.op_get_events)
    # Get a set of activity codes that created these events
    act_ids = {utils.act_code_from_event_id(e.get('id')) for e in events}

    # Get all activities (by code)
    acts_by_code = state.get('termtime.activities_by_code', termtime.op_get_activities_by_code)
    # Which field is the CRSid of requestor stored (default to 'user5')
    crsid_user_field = state.get('termtime.crsid_user_field', 'user5')
    # Filter activities to those we had events for and that have a requestor
    filtered_acts = {
        k: v.get(crsid_user_field)
        for k, v in acts_by_code.items()
        if k in act_ids and v.get(crsid_user_field, '') != ''
    }

    # Create dict of requestors and their activity codes
    act_codes_by_requestor = {}
    for code, requestor in filtered_acts.items():
        act_codes_by_requestor.setdefault(requestor, []).append(code)
    LOG.info(f"Requestors from events: {len(act_codes_by_requestor)}")

    # Get all Booker staff ids
    staff_ids = state.get('booker.staff_by_id', op_get_staff_by_id).keys()

    # Finally find missing requestors
    missing = {
        k: v for k, v in act_codes_by_requestor.items()
        if k not in staff_ids
    }
    LOG.info(f"Requestors but not staff: {len(missing)}")
    for id, acts in missing.items():
        LOG.info(f"... {id}: {', '.join(acts)}")
    state.set('booker.missing_staff', missing)


def _conflict(event, booking):
    """
    Returns whether the `event` room and period overlaps with the `booking` room and period

    """
    # HACK: STS00015 is the whole exam hall, other STS rooms are subsections
    if event['Room']['Id'] == 'STS00015' or booking['Room']['Id'] == 'STS00015':
        if event['Room']['Id'][:3] != 'STS' or booking['Room']['Id'][:3] != 'STS':
            return False
    elif event['Room']['Id'] != booking['Room']['Id']:
        return False
    event_start = utils.booker_parse_datetime(event['StartTime'])
    event_end = utils.booker_parse_datetime(event['EndTime'])
    booking_start = utils.booker_parse_datetime(booking['StartTime'])
    booking_end = utils.booker_parse_datetime(booking['EndTime'])

    latest_start = max(event_start, booking_start)
    earliest_end = min(event_end, booking_end)
    delta = earliest_end - latest_start
    if delta.days < 0:
        return False
    return delta.seconds > 0


def op_invalid_events():
    """
    Use results of comparison operation and booker room details to find potential
    invalid events (those that may not be created/updated due to timing and
    capacity rules) and log them.

    """
    # compare all room bookings if haven't already
    comp = state.get('comparison.events', comparison.op_compare_events)

    # for each room get the capacity and opening hours
    for room in state.get('booker.rooms'):
        # check room details (OpenTime, CloseTime, AllowOutsideHours, MinOccupancyThreshold)
        has_min_occupancy = (
            room.get('MinOccupancyThreshold') is not None and room['MinOccupancyThreshold'] > 0
        )
        allowed_outside_hours = room.get('AllowOutsideHours', False)
        # Booker OpenTime and CloseTime despite containing a zulu datetime actually only use
        # the time component and as a localtime value, so just grab HH:MM from string
        open_time = room['OpenTime'][11:16]
        close_time = room['CloseTime'][11:16]
        LOG.info(f"Room {room['Id']} {room['Description']!r}:")
        LOG.info(
            f"...Open period: {open_time}-{close_time}" +
            (" (allowed outside hours)" if allowed_outside_hours else "")
        )
        if has_min_occupancy:
            LOG.info(f"...Min Occupancy: {room['MinOccupancyThreshold']} %")

        # get all events for this room
        room_create_events = [e for e in comp.get('create', []) if e['Room']['Id'] == room['Id']]
        LOG.info(f"...Creations: {len(room_create_events)}")
        room_update_events = [e for e in comp.get('update', []) if e['Room']['Id'] == room['Id']]
        LOG.info(f"...Updates: {len(room_update_events)}")
        room_events = room_create_events + room_update_events
        if len(room_events) == 0:
            continue

        if has_min_occupancy:
            # check new/updated event meets min occupancy
            required_num = int(room['Capacity'] * room['MinOccupancyThreshold'] / 100)
            LOG.info(f"...Required attendees: {required_num}")
            for event in room_events:
                if event['ExpectedAttendees'] < required_num:
                    LOG.info(
                        f"****** {event['code']} has {event['ExpectedAttendees']} attendees"
                    )

        if not allowed_outside_hours:
            # check new/updated event is within opening hours
            for event in room_events:
                if not _within_opening_hours(event, open_time, close_time):
                    LOG.info(
                        f"****** {event['code']} ({event['StartTime']}-{event['EndTime']}) "
                        "outside opening hours"
                    )


def _within_opening_hours(event, open_time, close_time):
    """
    Returns whether the `event` is within the opening hours (localtime)

    """
    event_start = utils.booker_parse_datetime(event['StartTime'])
    event_end = utils.booker_parse_datetime(event['EndTime'])
    event_start_time = utils.localtime_hhmm(event_start)
    event_end_time = utils.localtime_hhmm(event_end)
    return open_time <= event_start_time < close_time and open_time < event_end_time <= close_time


def _get_bookings(start=None, end=None, filter=None):
    """
    Get all bookings from Booker between start and end dates (defaulting to
    termtime.timeframe dates) using filter function

    """
    # If either or both start and end are None use TermTime timeframe
    if start is None or end is None:
        (tf_start, tf_end) = termtime.get_timeframe_dates()
        start = start if start is not None else tf_start
        end = end if end is not None else tf_end
    # bring forward start (just date) to 'now'
    now_in_booker_format = utils.booker_format_datetime(utils.now())
    start = max(start, now_in_booker_format[:10])
    # get a function that will match bookings against the `booker.ignore_bookings` list
    # returning True if the booking should be ignored
    should_ignore = should_ignore_bookings_factory(state.get('booker.ignore_bookings'))
    # calendar/events endpoint needs a list of RoomIds (actually OptimeIndexes)
    rooms_by_ois = {r['OptimeIndex']: r for r in state.get('booker.rooms', op_get_rooms)}
    # Booker API appear to respond with 500 error if there are a large number of events.
    # This hack requests events one room at a time until a better fix can be found.
    bookings = []
    for room_oi, room in rooms_by_ois.items():
        LOG.info(f"Getting bookings for room: {room_oi} "
                 f"{room.get('Id', '?')} {room.get('Description', 'Unknown')}")
        (room_bookings, _) = booker_post(
            'booker/calendars/events',
            is_update=False,
            # As of v1.21 this endpoint only accepts the params in form data
            data={
                'start': start,
                'end': end,
                'StatusList': ['Approved'],
                'roomIds': [room_oi],
            }
        )
        bookings.extend(room_bookings)
    # Bookings actually in 'booking' property of response
    bookings = [b['booking'] for b in bookings if 'booking' in b]
    LOG.info(f'All bookings between {start} to {end}: {len(bookings)}')
    # all bookings need a ['BookedBy']['Id']
    # also we assume ['Description'] is a string but appears to sometimes be None, so fix here
    for b in bookings:
        if b.get('BookedBy', {}).get('Id') is None:
            LOG.error('At least one booking missing BookedBy.Id property')
            raise BookerAPIError()
        if b.get('Description') is None:
            b['Description'] = ''
    # return filtered bookings, only including:
    # - those starting on or after current time (or 'now')
    # - those passing `filter` (if specified)
    # - those not matching `booker.ignore_bookings` state (if specified)
    return [
        b for b in bookings
        if now_in_booker_format <= b.get('StartTime', '')
        and (filter is None or filter(b))
        and not should_ignore(b)
    ]


def should_ignore_bookings_factory(filter_list):
    """
    Create a function that matches a booking against any of the list of filters.
    Each filter is a dict of 'by' and 'values', where 'by' can be one of 'booked_by',
    'room', 'building' or 'type', and 'values' is a list of ids to compare against.

    If `filter_list` is None or empty then a function that returns False will be
    provided.
    """
    # Always False function
    def always_false(b):
        return False
    # No filters then nothing to do
    if filter_list is None:
        return always_false
    if not isinstance(filter_list, list):
        LOG.error("booker.ignore_bookings must be a list of filters")
        raise ConfigurationInvalid()
    if len(filter_list) == 0:
        # No filters in list
        return always_false

    # checks for each 'by'
    filter_checks = {
        'booked_by': lambda b, id: b.get('BookedBy', {}).get('Id', '') == id,
        'room': lambda b, id: b.get('Room', {}).get('Id', '') == id,
        'building': lambda b, id: b.get('Room', {}).get('Building', {}).get('Id', '') == id,
        'type': lambda b, id: b.get('Type', '') == id,
    }

    # validate filter_list
    for f in filter_list:
        by = f.get('by')
        if not by:
            LOG.error("booker.ignore_bookings filter missing 'by'")
            raise ConfigurationInvalid()
        if not filter_checks.get(by):
            by_options = "'" + "', '".join(filter_checks.keys()) + "'"
            LOG.error(
                f"booker.ignore_bookings filter has invalid 'by': '{by}' must "
                f"be one of {by_options}"
            )
            raise ConfigurationInvalid()
        if not isinstance(f.get('values'), list):
            LOG.error("booker.ignore_bookings filter has missing/invalid 'values' list")
            raise ConfigurationInvalid()

    def match_func(booking):
        for f in filter_list:
            for v in f['values']:
                if filter_checks[f['by']](booking, v):
                    return True
        return False

    return match_func


def _get_termtime_equipment():
    """
    Get the Booker equipment that identifies which rooms should be sync'ed with
    TermTime

    """
    # Find equipment with "TermTime" Id
    tt_id = state.get('booker.termtime_id', 'TermTime')
    tt_equip = next(
        (
            e for e in state.get('booker.all_equipment', op_get_all_equipment)
            if e['Id'] == tt_id
        ),
        None
    )
    if tt_equip is None:
        LOG.error(f"Booker equipment types missing '{tt_id}' entry")
        raise BookerAPIError()
    state.set('booker.termtime_equipment', tt_equip)
