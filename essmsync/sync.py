"""
Synchronisation actions

"""
import logging
import re

from . import state
from . import core  # noqa: F401
from . import termtime
from . import booker
from . import comparison  # noqa: F401
from .exceptions import OperationInvalid

LOG = logging.getLogger(__name__)

# modules that contain valid operations
VALID_MODULES = ['core', 'termtime', 'booker', 'comparison']


def sync():
    """
    Perform sync actions

    """
    if state.get('read_only', True):
        LOG.info('Performing synchronisation in READ ONLY mode.')
    else:
        LOG.info('Performing synchronisation in WRITE mode.')

    ops = state.get('operations')
    if len(ops) == 0:
        LOG.warning('No operations to perform')
        return

    # Get all possible operations and their functions
    op_map = get_operations()
    LOG.debug(f'Possible operations: {op_map}')
    # Parse and validate operations before calling any
    valid_ops = []
    invalid = False
    for o in ops:
        try:
            valid_ops.append(validate_op(o, op_map))
        except OperationInvalid:
            invalid = True
    if invalid:
        raise OperationInvalid

    # Authenticate to each API
    termtime.auth()
    booker.auth()

    # Perform operations
    for (func, args, name) in valid_ops:
        LOG.info(f'Performing: {name}')
        func(*args)


def get_operations():
    """
    Searches valid modules for `op_*` functions building a map of
    (operation name, module name) tuples to function

    """
    modules = [(m, globals()[m]) for m in VALID_MODULES]
    return {
        (o[3:], name): getattr(module, o)
        for (name, module) in modules
        for o in dir(module) if o[:3] == 'op_'
    }


def validate_op(op, op_map):
    """
    Parse then validate operations returning tuple of function, arguments and full name.
    Automatically find the module if not specified from operations map.

    >>> op_map = {('o1','core'):'f1', ('o2','core'): 'f2', ('o2','booker'): 'f3'}
    >>> validate_op('core.o1', op_map)
    ('f1', [], 'core:o1:[]')
    >>> validate_op('core.o2', op_map)
    ('f2', [], 'core:o2:[]')
    >>> validate_op('booker.o2', op_map)
    ('f3', [], 'booker:o2:[]')
    >>> validate_op('o1', op_map)  # only op and in one module
    ('f1', [], 'core:o1:[]')
    >>> validate_op('badmodule.o1', op_map)  # no such module
    Traceback (most recent call last):
        ...
    essmsync.exceptions.OperationInvalid
    >>> validate_op('core.o3', op_map)  # no such op in module
    Traceback (most recent call last):
        ...
    essmsync.exceptions.OperationInvalid
    >>> validate_op('o2', op_map)  # ambiguous op in more than one module
    Traceback (most recent call last):
        ...
    essmsync.exceptions.OperationInvalid
    >>> validate_op('o3', op_map)  # no such op in any module
    Traceback (most recent call last):
        ...
    essmsync.exceptions.OperationInvalid
    >>> validate_op('', op_map)  # completely blank
    Traceback (most recent call last):
        ...
    essmsync.exceptions.OperationInvalid
    >>> validate_op('core.', op_map)  # blank op
    Traceback (most recent call last):
        ...
    essmsync.exceptions.OperationInvalid

    """
    (module_name, op_name, args) = parse_op_string(op)
    if not op_name:
        LOG.error(f'Invalid operation {op_name} ({op})')
        raise OperationInvalid
    if module_name and module_name not in VALID_MODULES:
        LOG.error(f'Invalid operation module {module_name} ({op})')
        raise OperationInvalid

    if module_name:
        # module specified to look for exact match
        func = next(
            (f for (o, m), f in op_map.items() if o == op_name and m == module_name),
            None
        )
    else:
        # find all operations just by op_name and check for just one
        method_funcs = [(m, f) for (o, m), f in op_map.items() if o == op_name]
        if len(method_funcs) > 1:
            LOG.error(f'Ambiguous operation {op_name} ({op})')
            raise OperationInvalid
        module_name, func = next(iter(method_funcs), (None, None))

    if not func:
        LOG.error(f'Missing operation ({op})')
        raise OperationInvalid

    return (func, args, f'{module_name}:{op_name}:{args}')


def parse_op_string(op):
    """
    Parses operation string in to components
    Format: [module:]name[([arg[,arg]])]
            alternatively '.' instead of ':' for module separator

    >>> parse_op_string('action')
    (None, 'action', [])
    >>> parse_op_string('module:action')
    ('module', 'action', [])
    >>> parse_op_string('module.action')
    ('module', 'action', [])
    >>> parse_op_string('action()')
    (None, 'action', [])
    >>> parse_op_string('action(arg1)')
    (None, 'action', ['arg1'])
    >>> parse_op_string('module:action(arg1)')
    ('module', 'action', ['arg1'])
    >>> parse_op_string('module.action(arg1)')
    ('module', 'action', ['arg1'])
    >>> parse_op_string('action(arg1,arg2)')
    (None, 'action', ['arg1', 'arg2'])
    >>> parse_op_string('module:action(arg1,arg2)')
    ('module', 'action', ['arg1', 'arg2'])
    >>> parse_op_string('module.action(arg1,arg2)')
    ('module', 'action', ['arg1', 'arg2'])
    """
    # defaults
    module = None
    op_name = op
    args = []
    # match if arguments given
    match = re.fullmatch(r'(.*)\((.*)\)', op)
    if match is not None:
        op_name = match.group(1)
        args = [a.strip() for a in match.group(2).split(',')]
        if len(args) == 1 and args[0] == '':
            args = []
    # match if module given
    if ':' in op_name:
        module, op_name = [a.strip() for a in op_name.split(':', 2)]
    elif '.' in op_name:
        module, op_name = [a.strip() for a in op_name.split('.', 2)]

    return (module, op_name, args)
