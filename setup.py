import os

from setuptools import setup, find_packages


def load_requirements(file):
    """
    Load requirements file and return non-empty, non-comment lines with leading and trailing
    whitespace stripped.
    """
    with open(os.path.join(os.path.dirname(__file__), file)) as f:
        return [
            line.strip() for line in f
            if line.strip() != '' and not line.strip().startswith('#')
        ]


setup(
    name='essmsync',
    version='0.3.0',
    packages=find_packages(),
    install_requires=load_requirements('requirements.txt'),
    tests_require=load_requirements('requirements-tests.txt'),
    entry_points={
        'console_scripts': [
            'essmsync=essmsync:main',
        ]
    },
    extras_require={
        ':python_version < "3.7"': [
            'dataclasses',
        ],
    }
)
