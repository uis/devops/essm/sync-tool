# Operations

Operations are categorised by module. Arguments are optional if defaults are listed below.

## Core

#### dump / dump_yaml (key, parents)
- `key` - key in to state
- `parents` - whether to keep parents of key in output (default `true`)

Prints out the contents of the specified key in yaml format. Key should be in
'dotted' format for nested keys, e.g. `rootkey.subkey.subsubkey`.

#### dump_json (key, parents, indent)
 - `key` - key in to state
 - `parents` - whether to keep parents of key in output (default `false`)
 - `indent` - number of spaces to indent output (default no indentation)

Prints out the contents of the specified key in json format, optionally with
indentation. Key should be in 'dotted' format for nested keys, e.g.
`rootkey.subkey.subsubkey`.


## Booker

In order to filter the entities in Booker to only those relating to the rooms
that we are interested in (those with the TermTime equipment), there are two
sets of booker "get" operations. Those that get all the entities from the API
(`get_all_*`) and those that filter the lists (`get_*`).

#### get_all_buildings
Get list of all buildings and put them in the `booker.all_buildings`
state key.

#### get_all_depts
Get list of all departments (aka institutions) and put them in the
`booker.all_depts` state key.

#### get_all_equipment
Get list of all equipment types and put them in the `booker.all_equipment`
state key.

#### get_all_floors
Get list of all floors and put them in the `booker.all_floors` state key.
Booker doesn't store floors as separate entities, just a string on the room
entity, so objects with building, floor name and compiled id are created.

#### get_all_rooms
Get list of all rooms and put them in the `booker.all_rooms` state key.

#### get_all_sites
Get list of all sites and put them in the `booker.all_sites` state key.

#### get_all_staff
Get list of all staff and put them in the `booker.all_staff` state key with
limited data due to the quantity of them.

#### get_staff_by_id
Get all staff, filter to those with "crsid-looking" Ids, and create a dict keyed
on a sanitised Id (lowercased and whitespace removed). Put this in the
`booker.staff_by_id` state key.

#### get_buildings
Get a list of buildings related to the filtered rooms list (see get_rooms)
and put them in the `booker.buildings` state key.

The `get_rooms` operation is called, if needed, to obtain the filtered list of
rooms. A list of unique buildings is compiled from these rooms.

#### get_depts
Get a list of departments related to the filtered rooms list (see get_rooms)
and put them in the `booker.depts` state key.

The `get_rooms` operation is called, if needed, to obtain the filtered list of
rooms. A list of department OptimeIndexes is compiled from the first member of
the room's `Departments` list.

The `get_all_depts` operation is then called, if needed, then filtered to only
those departments with OptimeIndexes in the above compiled list.

#### get_floors
Get list of floors related to the filtered rooms list (see get_rooms) and put
them in the `booker.all_floors` state key.
Booker doesn't store floors as separate entities, just a string on the room
entity, so objects with building, floor name and compiled id are created.

#### get_rooms
Get a filtered list of rooms and put them in the `booker.rooms` state key.

The `get_all_equipment` operation is called, if needed, so that the 'TermTime'
equipment can be found (and stored in `booker.termtime_equipment`). The `Id` of
this equipment can be specified by `booker.termtime_id`, otherwise "TermTime" is
used.

The `get_all_rooms` operation is then called, if needed, and the list of all rooms
filtered down to only those with the 'TermTime' equipment.

Additionally, the state keys `booker.only_buildings` and `booker.exclude_buildings`
can be use to give a single or list of building Ids. The rooms list will be filtered
to only include and/or exclude (respectively) rooms in these buildings.

The `booker.rooms` state key is updated with this filtered list.

#### get_sites
Get a list of sites related to the filtered rooms list (see get_rooms)
and put them in the `booker.sites` state key.

The `get_buildings` operation is called, if needed, to obtain the filtered list
of buildings. A list of the `Site` values of each buildings is compiled.

#### get_room_bookings(start=None, end=None)
- `start` - date at start of period in YYYY-MM-DD format (inclusive)
- `end` - date at end of period in YYYY-MM-DD format (exclusive)

Get a list of bookings within the given period, for TermTime managed rooms, that
were **not** synced from TermTime events (i.e. don't have an activity/instance
reference in their description) and put them in the `booker.roombookings`
configuration key.

If either `start` or `end` dates are not specified then the matching dates from
the TermTime timeframe specified in `termtime.timeframe` ("All Year" by default)
are used.

Bookings starting before the current time (or state `now`) and those matching any
of the filters in `booker.ignore_bookings` (see `configuration-example.yaml`) are
also excluded.

#### get_events(start=None, end=None)
- `start` - date at start of period in YYYY-MM-DD format (inclusive)
- `end` - date at end of period in YYYY-MM-DD format (exclusive)

Get a list of bookings within the given period, for TermTime managed rooms, that
were previously synced from TermTime events (i.e. have an activity/instance
reference in their description) and put them in the `booker.events` state key.

If either `start` or `end` dates are not specified then the matching dates from
the TermTime timeframe specified in `termtime.timeframe` ("All Year" by default)
are used.

Events starting before the current time (or state `now`) and those matching any
of the filters in `booker.ignore_bookings` (see `configuration-example.yaml`) are
also excluded.

#### get_activities
Get list of all activities within termtime.timeframe and put in the scheduled
ones in `termtime.activities`. Due to the potential size of this list only
limited fields are kept, one of them being the custom user field containing the
crsid of the requestor (defaults to 'user5' but can be specified by setting
`termtime.crsid_user_field`; also gets lowercased and whitespace removed).

#### get_activities_by_code
Get activities (as above) and create a dict keyed on 'code'. Put this in
the `termtime.activities_by_code` state key.

#### update_events
Use the results of the appropriate comparison operation (see below) to send
requests to create and update events.
> Requests only sent if using `--really-do-this` command line argument

#### cancel_events
Use the results of the appropriate comparison operation (see below) to send
requests to cancel deleted events.
> By default limits to only cancelling 10 events at a time unless overridden by
> `booker.cancel_limit` configuration key.
> Requests only sent if using `--really-do-this` command line argument

#### find_conflicts
Attempts to find conflicts between event creations and existing bookings, log
them and store them in `booker.conflicts` as tuple (event, booking)

#### missing_staff
Attempts to find people assigned as requestors of TermTime activities that
don't have a matching staff account in Booker, log them and store them in
`booker.missing_staff` as dict of id to list of their activities

### invalid_events
Attempts to find potential invalid events. i.e. those that may not be
created/updated due to open period and capacity rules set it Booker.


## TermTime

#### get_buildings
Get list of all buildings and put them in the `termtime.buildings` state
key.

#### get_campuses
Get list of all campuses and put them in the `termtime.campuses` state
key.

#### get_capabilities
Get list of all capabilities as simple list of strings and put them in the
`termtime.capabilities` state key.

#### get_depts
Get list of all departments (aka institutions) and put them in the `termtime.depts` state key.

#### get_events
Within a specified timeframe, get all scheduled events and put them in the
`termtime.events` state key.

The timeframe to use can be specified by the `termtime.timeframe` state
key, otherwise "All Year" will be used.

The timeframe will be looked up in `termtime.timeframes` (calling the
`get_timeframes` operation, if necessary) so that the start date and number of
days can be determined.

Events starting before the current time (or state `now`) are also excluded.

#### get_floors
Get list of all floors and put them in the `termtime.floors` state key.

#### get_all_room_bookings
Get list of all room bookings and put them in the `termtime.all_roombookings`
state key.

#### get_room_bookings
Within a specified timeframe, get list of room bookings of a specified type and
put them in the `termtime.roombookings` state key.

The timeframe to use can be specified by the `termtime.timeframe` state key,
otherwise "All Year" will be used.

The type to use can be specified by the `termtime.room_booking_type` state key,
otherwise 'bkr' will be used.

Bookings starting before the current time (or state `now`) are also excluded.

#### get_rooms
Get list of all rooms and put them in the `termtime.rooms` state key.

#### get_timeframes
Get list of all timeframes and put them in the `termtime.timeframes`
state key.

#### update_buildings
Use the results of the appropriate comparison operation (see below) to send
requests to create and update buildings.
> Requests only sent if using `--really-do-this` command line argument

#### update_campuses
As `update_buildings` but for campuses

#### update_depts
As `update_buildings` but for departments

#### update_depts
As `update_buildings` but for departments

#### update_floors
Similar `update_buildings` but for floors, except only creations are performed
not updates.

#### update_rooms
As `update_buildings` but for rooms

#### update_room_bookings
Similar to `update_buildings` but for room bookings, and in addition removes
room bookings that are not present in Booker.


## Comparison

These operations compare the results of the get_* operations of Booker and TermTime.
They produce new state keys under `comparison.<data type>` with subkeys:
 - `create` - TermTime/Booker entities to create
 - `update` - TermTime/Booker entities to update
 - `delete` - TermTime/Booker entities not existing in the other service, could
possibly be deleted, if necessary
 - `invalid` - Booker/TermTime entities that cannot be synced as their content is
considered invalid

#### compare_depts
Compares Booker departments with TermTime's, creating lists of necessary creations,
updates and deletes needed to TermTime, putting them in `comparison.depts`. Only
description is compared.

#### compare_sites
Compares Booker sites with TermTime campuses, creating lists of necessary creations,
updates and deletes needed to TermTime, putting them in `comparison.sites`. Only
description is compared.

#### compare_buildings
Compares Booker buildings with TermTime's, creating lists of necessary creations,
updates and deletes needed to TermTime, putting them in `comparison.buildings`.
Description and site are compared.

#### compare_floors
Compares Booker floors with TermTime's, creating lists of necessary creations,
updates and deletes needed to TermTime, putting them in `comparison.floors`. As
Booker has no ids for floors to match to TermTime code so floor, building and
site descriptions are matched collectively.

#### compare_rooms
Compares Booker rooms with TermTime's, creating lists of necessary creations,
updates and deletes needed to TermTime, putting them in `comparison.rooms`.
Description, building, floor and department are compared.

#### compare_room_bookings
Compares Booker room bookings with TermTime's, creating lists of necessary
creations, updates and deletes needed to TermTime, putting them in
`comparison.roombookings`. Title, start and end/duration, and room are compared.

#### compare_events
Compares TermTime events with Booker's, creating lists of necessary
creations, updates and deletes needed to Booker, putting them in
`comparison.events`. Title, start and end times, room and size are compared.
Also, Booker booking type is expected to be `booker.booker_event_type` (defaults
to "Teaching").

There is no common 'Id' property to match TermTime events to Booker bookings, so an
identifier "{id}:{instance}" is created for each TermTime event from activity id
and instance number. Then a search is made in Booker for a booking with a
description containing [{id}:{instance}]".

Descriptions are not changed with updates but new bookings are created with a
'prefix' (from `booker.booker_event_prefix`, defaults to "Booked by TermTime")
and the [{id}:{instance}] reference.

If TermTime's activity requestor doesn't match the Booker's BookedBy for the
event then an update will be created to update this. This is not set by the
initial creation of the booking as the Booker API fails to set this correctly
(forcing it to be the API authenticated user). The CRSid used in this update
can be overridden by specifying the `booker.override_booked_by_user` state key
(useful for testing without notifying the actual requestor).
